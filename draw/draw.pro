#-------------------------------------------------
#
# Project created by QtCreator 2014-12-18T11:02:55
#
#-------------------------------------------------

QT       += widgets
TARGET = draw
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++11
INCLUDEPATH += ../base/

SOURCES += \
    drawer.cpp

HEADERS += \
    drawer.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
