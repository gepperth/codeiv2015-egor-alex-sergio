
#ifndef DRAWER_H_
#define DRAWER_H_

#include "state.h"
#include "detection.h"
#include "target.h"
#include "particle.h"
#include "tracklets.h"
#include "floatfann.h"
#include <QPainter>
#include <QPen>
#include <QRect>
using namespace std;

namespace drawing
{
	Target drawPrediction(const Target& target, QPainter& painter, QPen& pen, struct fann* ann);
    void drawTarget(const Target& target, QPainter& painter, QPen& pen, double width, double height, bool showSpeed);
    void drawDetections(const vector<Detection>& detections, QPainter& painter, QPen& pen, double width, double height);
    void drawCar(const CarData &cardata, int nFrame, QPainter& painter, QPen& pen);
    void drawParticles(const vector<vector<Particle> >& particles, QPainter& painter, QPen& pen, int type);
    void drawTargets(vector<Target>& targets, QPainter& painter, QPen& pen,double width,double height, bool useDimensions,bool showSpeed);

}


#endif /* DRAWER_H_ */
