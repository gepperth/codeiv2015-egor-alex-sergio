//#ifndef DRAWER_H_
//#define DRAWER_H_



#include "drawer.h"



namespace drawing
{

    Target drawPrediction(const Target& target, QPainter& painter, QPen& pen, fann* ann)
	{
		/*
    	list<State> history=target.getHistory();
    	list<State>::reverse_iterator itrev=history.rbegin();
    	fann_type *calc_out;
    	fann_type input[12];
    	double new0=dynamic_cast<Cartesian2D*>((*itrev)[0])->mX;
    	double new1=dynamic_cast<Cartesian2D*>((*itrev)[0])->mY;
    	double new2=dynamic_cast<Cartesian2D*>((*itrev)[0])->mVx;
    	double new3=dynamic_cast<Cartesian2D*>((*itrev)[0])->mVy;
    	double new4=dynamic_cast<Dimension2D*>((*itrev)[1])->mDx;
    	double new5=dynamic_cast<Dimension2D*>((*itrev)[1])->mDy;

    	itrev++;

    	input[0]=new0-dynamic_cast<Cartesian2D*>((*itrev)[0])->mX;
    	input[1]=new1-dynamic_cast<Cartesian2D*>((*itrev)[0])->mY;
    	input[2]=new2-dynamic_cast<Cartesian2D*>((*itrev)[0])->mVx;
    	input[3]=new3-dynamic_cast<Cartesian2D*>((*itrev)[0])->mVy;
    	input[4]=new4-dynamic_cast<Dimension2D*>((*itrev)[1])->mDx;
    	input[5]=new5-dynamic_cast<Dimension2D*>((*itrev)[1])->mDy;

    	new0=dynamic_cast<Cartesian2D*>((*itrev)[0])->mX;
    	new1=dynamic_cast<Cartesian2D*>((*itrev)[0])->mY;
    	new2=dynamic_cast<Cartesian2D*>((*itrev)[0])->mVx;
    	new2=dynamic_cast<Cartesian2D*>((*itrev)[0])->mVy;
    	new3=dynamic_cast<Dimension2D*>((*itrev)[1])->mDx;
    	new4=dynamic_cast<Dimension2D*>((*itrev)[1])->mDy;

    	itrev++;
    	input[6]=new0-dynamic_cast<Cartesian2D*>((*itrev)[0])->mX;
    	input[7]=new1-dynamic_cast<Cartesian2D*>((*itrev)[0])->mY;
    	input[8]=new2-dynamic_cast<Cartesian2D*>((*itrev)[0])->mVx;
    	input[9]=new3-dynamic_cast<Cartesian2D*>((*itrev)[0])->mVy;
    	input[10]=new4-dynamic_cast<Dimension2D*>((*itrev)[1])->mDx;
    	input[11]=new5-dynamic_cast<Dimension2D*>((*itrev)[1])->mDy;


    	cout<<"input ";
    	for (int k=0; k<12; k++)
    		cout<<input[k]<<" ";
    	cout<<endl;


    	calc_out = fann_run(ann, input);

    	for (int k=0; k<6; k++)
    		cout<<calc_out[k]<<" ";
    	cout<<endl;

    	State s=target.getState();
    	dynamic_cast<Cartesian2D*>(s[0])->mX+=calc_out[0];// *10;
    	dynamic_cast<Cartesian2D*>(s[0])->mY+=calc_out[1];// *10;
    	dynamic_cast<Cartesian2D*>(s[0])->mVx+=calc_out[2];
    	dynamic_cast<Cartesian2D*>(s[0])->mVy+=calc_out[3];
    	dynamic_cast<Dimension2D*>(s[1])->mDx+=calc_out[4];
    	dynamic_cast<Dimension2D*>(s[1])->mDy+=calc_out[5];


    	pen.setColor(Qt::blue);
    	painter.setPen(pen);
    	drawLine(target.getState(), s, painter, pen);
    	//drawRectangle(s, painter, pen, 0, 0);
*/
    	Target t=target;
    	//t.setState(s);

    	return t;

	}

    void drawTarget(const Target& target, QPainter& painter, QPen& pen, double width, double height, bool showSpeed)
    {
    	double validity=target.getValidity();
    	validity=min(validity,1.0);
    	validity=max(validity,0.0);

        list<shared_ptr<State>> history=target.getHistory();
    	list<double> historyValidity=target.getHistoryValidity();
    	list<double>::iterator valit = historyValidity.begin();

    	painter.setPen(pen);
        target.getState()->draw(painter,width);

    	QFont font;


        font.setPointSizeF(max(abs(width),0.0001));

    	painter.setFont(font);
        target.getState()->drawText(painter,QString::number(target.getID()));

        //fann_type *calc_out;
        //fann_type input[12];

    	//setlocale(LC_ALL, "C");

        //struct fann *ann = fann_create_from_file("/home/egor/gitall/workspace/egor_xor_float.net");
        /*
    	Target t=target;
    	if (history.size()>3)
    	{

    		pen.setColor(Qt::blue);
    		painter.setPen(pen);
    		for (int i=0; i<10; i++)
    		{
    			//t=drawPrediction(t, painter, pen, ann);
    		}

        }*/
        //fann_destroy(ann);

    	//printf("xor test (%f,%f) -> %f\n", input[0], input[1], calc_out[0]);



    	pen.setColor(Qt::green);
        for (std::list<shared_ptr<State>>::iterator it = history.begin(); (it != history.end())&& (next(it,1)!= history.end()); it++)
    	{
            shared_ptr<State> olds=*it, news=*next(it,1);

    	    validity=*valit;
    	    validity=min(validity,1.0);
    	    validity=max(validity,0.0);

    	    //pen.setColor(QColor( (1.0-validity)*255, 255*validity, 0));
    	    painter.setPen(pen);

            olds->drawConnection(painter,news);

			//if (!useDimensions)
			//{
				//calibrate(xold,yold,zold);
				//calibrate(xnew,ynew,znew);
			//}
    	    valit++;
    	}
    	if (showSpeed)
    	{
            /*
            pen.setColor(Qt::magenta);
    	    painter.setPen(pen);
    	    shared_ptr<Coordinate> prediction=target.getState().getCoordinate()->copy();
            target.getState().getCoordinate()->drawConnection(painter,prediction->predict()->predict()); state class changed
            */
    	    //delete prediction;
    	}
    }
    void drawDetections(const vector<Detection>& detections, QPainter& painter, QPen& pen, double width, double height)
    {
    	pen.setColor(Qt::red);
    	painter.setPen(pen);
        for (unsigned int i=0; i<detections.size(); i++)
            detections[i].getState()->draw(painter,width);
    };
    void drawCar(const CarData &cardata, int nFrame, QPainter& painter, QPen& pen)
    {
    	pen.setColor(Qt::blue);
		painter.setPen(pen);


    	double carx=cardata.positions[nFrame].e;
		double cary=-cardata.positions[nFrame].n;
		double alpha=-cardata.positions[nFrame].yaw;

		double carw=4.0/2.0;
		double carh=1.7/2.0;

		//painterImage.drawRect ( QRectF (carx-carw, cary-carh, 2*carw, 2*carh));
		double lbx=carx-carw*cos(alpha)+carh*sin(alpha);
		double lby=cary-carw*sin(alpha)-carh*cos(alpha);
		double lfx=carx-carw*cos(alpha)-carh*sin(alpha);
		double lfy=cary-carw*sin(alpha)+carh*cos(alpha);

		double rbx=carx+carw*cos(alpha)+carh*sin(alpha);
		double rby=cary+carw*sin(alpha)-carh*cos(alpha);
		double rfx=carx+carw*cos(alpha)-carh*sin(alpha);
		double rfy=cary+carw*sin(alpha)+carh*cos(alpha);


		painter.drawLine ( QLineF(lbx,lby,lfx,lfy));
		painter.drawLine ( QLineF(lfx,lfy,rfx,rfy));
		painter.drawLine ( QLineF(rfx,rfy,rbx,rby));
		painter.drawLine ( QLineF(rbx,rby,lbx,lby));
    };
    void drawParticles(const vector<vector<Particle> >& particles, QPainter& painter, QPen& pen, int type)
    {
		double maxWeight;
    	for (int i=0; i<(int)particles.size(); i++)
		{
			maxWeight=0;
			for (int j=0; j<(int)particles[i].size(); j++)
			{
				maxWeight=max(particles[i][j].getWeight(),maxWeight);
			}
			for (int j=0; j<(int)particles[i].size(); j++)
			{

                shared_ptr<State> state=particles[i][j].getState();
				double weight=particles[i][j].getWeight();

				if (maxWeight!=0)
					weight=weight/maxWeight;
				pen.setColor(QColor( weight*255, 0, 255*(1.0-weight)));
				painter.setPen(pen);

				if (type==0)
				{
                    state->drawPoint(painter);
				}
				else if (type==1)
				{
					//drawing::drawArrow(state,painter,pen);
                    //shared_ptr<Coordinate> prediction=state.getCoordinate()->copy();
                    //state.getCoordinate()->drawConnection(painter,prediction->predict()->predict());

				}
			}

		}
    };
    void drawTargets(vector<Target>& targets, QPainter& painter, QPen& pen,double width,double height, bool useDimensions,bool showSpeed)
    {
    	for (unsigned int i=0; i<targets.size(); i++)
    	{
    		//if (phdInterface->targets[i].getProbability()>0.06)
    		pen.setColor(Qt::green);
    		painter.setPen(pen);

    		if (useDimensions)
    		{
    			width=0;
    			height=0;
    		}

    		drawing::drawTarget(targets[i],painter,pen,width,height,showSpeed);
    	}
    }
}

