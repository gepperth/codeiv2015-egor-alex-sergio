#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTextStream>
#include <qtimer.h>
#include "paintwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();


    PaintWidget *cameraWidget;
    PaintWidget *laserWidget;
    PaintWidget *rectSizeWidget;
    QWidget 	*centralWidget;
    QGridLayout *layout;
    QLineEdit *repeatEdit;
    QPushButton *learnButton;
    QPushButton *visualTestButton;
    QPushButton *saveImagesButton;
    QTimer		*timer;
    fusionSOM *fusion;

signals:
    void timeout();
public slots:
    void associateLearn();
    void saveImages();
    void visualTest();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
