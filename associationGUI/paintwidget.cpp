#include "paintwidget.h"

PaintWidget::PaintWidget(QWidget* parent)
    : QWidget(parent)
{
    drawState=UNCHOSEN;
    node=NULL;
    saveFileName="";
    testNum=0;
}
PaintWidget::~PaintWidget()
{
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);


    double radius1, radius2;

    QPen pen;
    painter.setWindow(windowRect);
    pen.setWidth(0.5);
    painter.setPen(pen);
    if (identifier==0)
    {
        radius1=5;//3.4;
        radius2=3;//1.2;
    }
    else if (identifier==1)
    {
        double val;
        if (fusion->isTracklets)
            val=0.2;
        else
            val=50;//reel=50, tracklets=0.2

        radius1=4*val;
        radius2=4*val;
    }
    else if (identifier==2)
    {
        radius1=7;
        radius2=2;
    }
    if (drawState==CHOSEN || drawState==UNCHOSEN)
    {
        if (identifier==0)
            fusion->somCam.draw(painter,radius1,radius2);
        else if (identifier==1)
            fusion->somLas.draw(painter,radius1,radius2);
        else if (identifier==2)
            fusion->somRectSize.draw(painter,radius1,radius2);
    }

    if (drawState==CHOSEN)
    {
        if (identifier==0)
            fusion->somCam.drawChosen(painter,I,J,radius1,radius2);
        else if (identifier==1)
            fusion->somLas.drawChosen(painter,I,J,radius1,radius2);
        else if (identifier==2)
            fusion->somRectSize.drawChosen(painter,I,J,radius1,radius2);
    }

    if (drawState==CHOICE)
    {
        int k=testNum;

        vector<int> nodesCam;
        vector<int> nodesLas;
        vector<vector<vector<double> > > weights;

        fusion->getFrameDetects(k,nodesCam,nodesLas,weights);
/*
        for (unsigned int i=0; i<nodesCam.size(); i++)
        {
            cout<<"CAM "<<nodesCam[i]<<endl;
        }

        for (unsigned int i=0; i<nodesLas.size(); i++)
        {
            cout<<"LAS "<<nodesLas[i]<<endl;
        }*/

        if (identifier==0)
        {
            if (nodesCam.size()>0)
            {
                int nNode=nodesLas[0];
                int nNodeSelf=0;

                fusion->somCam.drawWeights(painter,0,fusion->somLas.mNodes[nNode],radius1,radius2);
                            fusion->somCam.drawChoice(painter, nNodeSelf, nodesCam,radius1*2,radius2*2);
            }
        }

        else if (identifier==1)
        {
            vector<vector<vector<double> > > weightsT=weights;
            for (unsigned int i=0; i<weights.size(); i++)
            {
                for (unsigned int j=0; j<weights[i].size(); j++)
                {
                    weightsT[i][j]=weights[j][i];
                }
            }
            if (nodesLas.size()>0)
            {
                int nNode=nodesCam[0];
                int nNodeSelf=0;

                fusion->somLas.drawWeights(painter,1,fusion->somCam.mNodes[nNode],radius1,radius2);

                fusion->somLas.drawChoice(painter, nNodeSelf, nodesLas,radius1*2,radius2*2);
            }
        }

    }

    if (drawState==PROJECTED)
    {
        if (identifier==0)
        {
            fusion->somCam.drawWeights(painter,0,*node,radius1,radius2);
            double x,y,w=100,h=200;
            filereader::lidar2img(node->coords[0], node->coords[1],node->coords[2], x, y);
            pen.setColor("blue");
            pen.setWidthF(5);
            cout<<"coords "<<node->coords[0]<<" "<<node->coords[1]<<" "<<node->coords[2]<<endl;
            cout<<"xy "<<x<<" "<<y<<endl;
            /*
            fusion->somCam.findTheNearest({x,y}, I, J);
            int Icam=I*fusion->somCam.mH+J;
            int Isize=fusion->somCam.getMaxIndex(Icam, 2);
            */

            int Il,Jl;
            fusion->somLas.findTheNearest({node->coords[0],node->coords[1]}, Il, Jl);
            int Ilas=Il*fusion->somLas.mH+Jl;
            int Isize=fusion->somLas.getMaxIndex(Ilas, 2);

            w=fusion->somRectSize.mNodes[Isize].coords[0];
            h=fusion->somRectSize.mNodes[Isize].coords[1];
            cout<<"wh "<<w<<" "<<h<<endl;
            QColor color;
            color.setAlphaF(0);
            QBrush brush(color);
            painter.setPen(pen);
            painter.setBrush(brush);
            painter.drawRect(QRect(x-w,y-h,2*w,2*h));
            painter.drawEllipse(QPoint(x,y),5,5);
        }
        else if (identifier==1)
            fusion->somLas.drawWeights(painter,1,*node,radius1,radius2);
        else if (identifier==2)
            fusion->somRectSize.drawWeights(painter,2,*node,radius1,radius2);
    }

    painter.end();

}
void PaintWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons() == Qt::LeftButton)
	{
        double x=event->localPos().x();
        double y=event->localPos().y();
        x=windowRect.width()*x/(qreal)width()+windowRect.left();
        y=windowRect.height()*y/(qreal)height()+windowRect.top();

        if (identifier==0)
            fusion->somCam.findTheNearest({x,y},I,J);
        else if (identifier==1)
            fusion->somLas.findTheNearest({x,y},I,J);
        else if (identifier==2)
            fusion->somRectSize.findTheNearest({x,y},I,J);
        drawState=CHOSEN;
        update();
        if (identifier==0)
        {
            clickout(fusion->somCam.mNodes[I*fusion->somCam.mH+J]);
            cout<<"CHOSEN "<<I*fusion->somCam.mH+J<<endl;
        }
        else if (identifier==1)
        {
            clickout(fusion->somLas.mNodes[I*fusion->somLas.mH+J]);
            cout<<"CHOSEN "<<I*fusion->somLas.mH+J<<endl;
        }
        else if (identifier==2)
        {
            clickout(fusion->somRectSize.mNodes[I*fusion->somLas.mH+J]);
            cout<<"CHOSEN "<<I*fusion->somLas.mH+J<<endl;
        }
	}
}

void PaintWidget::clickin(Node &nodein)
{
    node=&nodein;
    drawState=PROJECTED;
    update();
}


