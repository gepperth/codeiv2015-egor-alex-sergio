#-------------------------------------------------
#
# Project created by QtCreator 2015-01-13T16:56:44
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11
INCLUDEPATH += ../base/

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

LIBS += \
       -lboost_serialization\

TARGET = associationGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintwidget.cpp

HEADERS  += mainwindow.h \
    paintwidget.h

FORMS    +=

unix: LIBS += -L$$PWD/../build-base-Desktop-Debug/ -lbase

INCLUDEPATH += $$PWD/../build-base-Desktop-Debug
DEPENDPATH += $$PWD/../build-base-Desktop-Debug

unix: PRE_TARGETDEPS += $$PWD/../build-base-Desktop-Debug/libbase.a
