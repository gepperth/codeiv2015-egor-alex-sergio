#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    cameraWidget=new PaintWidget();
    cameraWidget->setFixedSize(650,650);
    cameraWidget->identifier=0;
    laserWidget=new PaintWidget();
    laserWidget->setFixedSize(650,650);
    laserWidget->identifier=1;
    rectSizeWidget=new PaintWidget();
    rectSizeWidget->setFixedSize(100,650);
    rectSizeWidget->identifier=2;

    fusion = new fusionSOM();
    cameraWidget->fusion=fusion;
    laserWidget->fusion=fusion;
    rectSizeWidget->fusion=fusion;



    //========Tracklets=================================================
    if (fusion->isTracklets)
    {
        cameraWidget->windowRect=QRect(-100,-20,1400,400);
        laserWidget->windowRect=QRect(-20,50,120,-100);//QRect(-20,50,120,-100);
    }
    //========Tracklets==end============================================

    //========Real======================================================
    else
    {
        cameraWidget->windowRect=QRect(0,0,1600,800);//QRect(-100,-20,1400,400);
        laserWidget->windowRect=QRect(-5000,-20000,80000,40000);//QRect(-5,-20,80,40);//QRect(-20,50,120,-100);//QRect(-20,50,120,-100);
    }
    //========Real==end=================================================

    rectSizeWidget->windowRect=QRect(0,0,200,200);
    fusion->learn();


    //reduceData(4);
    centralWidget=new QWidget(this);
    repeatEdit = new QLineEdit("10");
    learnButton = new QPushButton("Learn associations");
    visualTestButton = new QPushButton("Visual test");
    saveImagesButton = new QPushButton("Save images");

    timer = new QTimer();
    timer->setSingleShot ( true );
    timer->setInterval (80);

    layout = new QGridLayout();
    layout->addWidget(cameraWidget,0,0);
    layout->addWidget(laserWidget,0,1);
    layout->addWidget(rectSizeWidget,0,2);
    layout->addWidget(repeatEdit,1,0);
    layout->addWidget(learnButton,1,1);
    layout->addWidget(saveImagesButton,1,2);
    layout->addWidget(visualTestButton,2,1);
    centralWidget->setLayout(layout);
    QObject::connect(cameraWidget, SIGNAL(clickout(Node&)), laserWidget, SLOT(clickin(Node&)));
    QObject::connect(laserWidget, SIGNAL(clickout(Node&)), cameraWidget, SLOT(clickin(Node&)));
    QObject::connect(cameraWidget, SIGNAL(clickout(Node&)), rectSizeWidget, SLOT(clickin(Node&)));
    QObject::connect(laserWidget, SIGNAL(clickout(Node&)), rectSizeWidget, SLOT(clickin(Node&)));
    QObject::connect(rectSizeWidget, SIGNAL(clickout(Node&)), cameraWidget, SLOT(clickin(Node&)));
    QObject::connect(rectSizeWidget, SIGNAL(clickout(Node&)), laserWidget, SLOT(clickin(Node&)));
    QObject::connect(learnButton, SIGNAL(clicked()), this, SLOT(associateLearn()));
    QObject::connect(visualTestButton, SIGNAL(clicked()), this, SLOT(visualTest()));
    QObject::connect(saveImagesButton, SIGNAL(clicked()), this, SLOT(saveImages()));

    QObject::connect(this, SIGNAL(timeout()),timer,SLOT(start()));
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(visualTest()));

    this->setCentralWidget(centralWidget);
}

MainWindow::~MainWindow()
{
    delete cameraWidget;
    delete laserWidget;
    delete rectSizeWidget;
    delete learnButton;
    delete repeatEdit;
    delete layout;
    delete centralWidget;
    delete fusion;
    delete ui;
}
void MainWindow::associateLearn()
{
    fusion->associateEval(repeatEdit->text().toInt());
}


void MainWindow::saveImages()
{
    laserWidget->grab().save("/home/egor/laser.png");
    cameraWidget->grab().save("/home/egor/camera.png");
}
void MainWindow::visualTest()
{
    cameraWidget->drawState=PaintWidget::CHOICE;
    cameraWidget->testNum++;
    cameraWidget->update();

    laserWidget->drawState=PaintWidget::CHOICE;
    laserWidget->testNum++;
    laserWidget->update();
    cout<<cameraWidget->testNum<<" "<<fusion->data.size()-2<<endl;
    if (cameraWidget->testNum<(int)fusion->data.size()-2)
        timeout();
}
