
#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <iostream>
#include <QApplication>
#include <QStyle>
#include <QWidget>
#include <QPainter>
#include <QMouseEvent>
#include "som.h"
#include "filereader.h"
#include "fusionsom.h"


using namespace std;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	explicit PaintWidget(QWidget* parent=0);
	~PaintWidget();
    int identifier;
    QString saveFileName;
    QRect windowRect;
    fusionSOM *fusion;
    enum state {CHOSEN=1,UNCHOSEN=2,PROJECTED=3,CHOICE=4};
    state drawState;
    int I;
    int J;
    int testNum;
    Node *node;

signals:
    void clickout(Node &node);
public slots:
    void clickin(Node &node);
	void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
};

#endif /* PAINTWIDGET_H_ */
