#ifndef PHDFILTER_H
#define PHDFILTER_H

#include <vector>
#include <random>
#include <chrono>
#include <cfloat>
#include <fstream>
#include <algorithm>
#include "newstate.h"
#include "detection.h"
#include "particle.h"
//#include "field.h"

using namespace std;

class PHDFilter
{
private:
public:
	vector<vector<Particle> > mParticles;
	vector<Detection> mObservations;
	vector<Target*> mTargets;
	int targetHistoryLength;
	int nParticlesPerObject;
	vector<Direction> mDirections;
	double modelForce;

	vector<bool> mUsed;
	default_random_engine mGenerator;
	double mPfn;
	double mPb;
	double mPd;
        double mPdeltaPlus,mPdeltaMinus ;
	int idNum;

public:

	double mAssociationCoef;
	double mResampleCoef;
	double mInfluenceCoef;
	vector<double> mSigmas;
        vector<bool> usedObservations ;
public:
	PHDFilter();
	~PHDFilter();
	double getTargetProbability(int i) ;
	void resample();
	void initialize(int num_particles,
					 const vector<double>& sigmas,					 
					 double pfn,
					 int historyLength,
					 const string& fieldName);
	void prediction();
	void association(const vector<Detection>& detections);
	void merge();
	void observation();
	void correction();
	void evaluate(const vector<Detection>& state,vector<vector<Particle> >& weightedParticles);
	int getTargetsNumber();
	int getParticlesNumberInTarget(int numTarget);
	Particle getParticle(int numTarget, int numParticle);
	vector<vector<Particle> > getParticles();
	void oneResample(const vector<Particle>& oldParticles, int numTarget, const vector<double>& cumulatedProbabilities, double sum);
	void getCumulatedProbability(int numTarget, vector<double>& cumulatedProbabilities, double& sum);
	void setValidity(int i, int numDirections);
	int randomFieldFluctuation(int i);
	void printParticles(ostream& out);
};






#endif /* PHDFILTER_H_ */
