#ifndef DETECTION_H_
#define DETECTION_H_

#include "newstate.h"

using namespace std;

class Detection
{
private:
	State * mState;
	double mScore;
	int mID;
 public:

    Detection ();
    Detection(const State * state, double score);
    ~Detection();
    Detection operator=(const Detection i);
    void setState(State * state);
    State * getState() const;
    void setID(int id);
    int getID() const;
    void setScore(double score);
    double getScore() const;
    friend std::ostream& operator<< (std::ostream& out, const Detection& detection);
};


#endif /* DETECTION_H_ */
