
#ifndef STATE_H
#define STATE_H

#include <math.h>
#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include "coordinate.h"

using namespace std;
class Direction
{
public:
	vector<double> mD;
	double mWeight;
};


class visualRect
{
  public:
  double x,y,width,height ;
} ;


class State
{
private:
	
public:
//	State()=0;
//	State( State *  i)=0
//	~State()=0;

	virtual void set(const vector<double> & data)=0;
	virtual void get(vector <double> & data) const=0;
	virtual void set( const State * i)=0 ;
	virtual void add( const State * i)=0 ;
	virtual void sub( const State * i)=0 ;
	virtual void divBy(double d)=0 ;

	virtual void setDynamics( const State * begin,  const State * end)=0;

	virtual double at (int i) const=0;
	virtual void predict ()=0;
	virtual double intersectionRate( State * i) const=0;
	virtual void merge ( State * i)=0 ;
	virtual double getGaussian ( State * i,const vector<double>& sigmas) const=0;
	virtual void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator)=0;
	virtual State * copy() =0;
	virtual void printState ()=0 ;
	virtual double getSimilarityToObs( const State * i, const vector <double> & sigmas) =0;
	virtual void setSpeedsZero()=0;
};

class visualState: public State
{
public:
        double xc,yc,xs,ys,xv,yv ;
        double targetRatio;
        bool compareSize, compareSpeed ;
public:
	visualState();
	visualState(const State *  i);
	visualState(double xc, double xs, double xv, double yc, double ys, double yv);
	~visualState(){};
	virtual State * copy() ;

	virtual void set(const vector<double> & data);
	virtual void get(vector <double> & data) const;
	virtual void set( const State * i) ;
	virtual void add( const State * i) ;
	virtual void sub( const State * i) ;
	virtual void divBy(double d) ;


	virtual void setDynamics( const State * begin,  const State * end);

	virtual double at (int i) const;
	virtual void predict ();
	virtual double intersectionRate( State * i) const;
	virtual void merge ( State * i);
	virtual double getGaussian ( State * i,const vector<double>& sigmas) const;
	virtual void setConsiderSpeed() ;
	virtual void setDisregardSpeed () ;
	virtual void setConsiderSize() ; 
	virtual void setDisregardSize() ;
	virtual void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator);
	virtual void printState () ;
	virtual double getSimilarityToObs( const State * i, const vector <double> & sigmas)  ;
	//friend std::ostream& operator<< (std::ostream& out, const State& state);
	virtual void setSpeedsZero();
} ;

class visualPlusLaserState: public visualState
{
public:
        //laserPlusVisstate has laser coordinates x (left-right) and y (near-far) plus speeds
        double lxc, lyc, lzc ;
        double lxv, lyv, lzv ;
public:
	visualPlusLaserState(): visualState(), lxc(0), lyc(0), lzc(0), lxv(0),lyv(0), lzv(0){};
	visualPlusLaserState(const  State *  i);
	visualPlusLaserState(double xc, double xs, double xv, double yc, double ys, double yv, double _lxc, double _lyc, double _lzc, double _lxv, double _lyv, double _lzv): visualState(xc,xs,xv,yc,ys,yv), lxc(_lxc),lyc(_lyc),lzc(_lzc), lxv(_lxv),lyv(_lyv), lzv(_lzv) {};
	~visualPlusLaserState(){};
	
	
	virtual State * copy() ;
	virtual void set(const vector<double> & data);
	virtual void get(vector <double> & data) const;
	virtual void set( const State * i) ;
	virtual void add( const State * i) ;
	virtual void sub( const State * i) ;
	virtual void divBy(double d) ;


	virtual void setDynamics( const State * begin,  const State * end);
	virtual void predict ();
	virtual double getGaussian ( State * i,const vector<double>& sigmas) const;
	virtual void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator);
	virtual void printState () ;
	virtual double getSimilarityToObs( const State * i, const vector <double> & sigmas) ;
	virtual void setSpeedsZero();

	
	//friend std::ostream& operator<< (std::ostream& out, const State& state);
} ;


#endif
