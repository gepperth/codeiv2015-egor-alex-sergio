#ifndef PARTICLE_H
#define PARTICLE_H


#include "newstate.h"
#include "target.h"

class Particle
{
private:
    State * mState;
    double mWeight;
    Target* mTarget;
 public:
    Particle ();
    ~Particle();
    void setState(State * state);
    State * getState() const;
    const State * getStateConst() const;
    void setWeight(double weight);
    double getWeight() const;
    void setTarget(Target *target);
    Target* getTarget() const;
    void operator= (Particle in);
    bool operator() ( Particle i, Particle j);
    bool operator< ( Particle i);
    void predict() {mState->predict() ;}
    friend std::ostream& operator<< (std::ostream& out, Particle& particle);
    void applyRandomFluctuation(const vector <double> & sigmas, default_random_engine &generator) {this->mState->applyRandomFluctuation(sigmas,generator);}
};



#endif /* PARTICLE_H */
