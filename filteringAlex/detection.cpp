#include "detection.h"


Detection::Detection()
{
	mScore=0;
}
Detection::Detection(const State * state, double score)
{
	mState=state->copy();
	mScore=score;

}
Detection::~Detection()
{

}
void Detection::setID(int id)
{
	mID=id;
}
int Detection::getID() const
{
	return mID;
}
void Detection::setScore(double score)
{
	mScore=score;
}
double Detection::getScore() const
{
	return mScore;
}

Detection Detection::operator=(const Detection i)
{
	this->mState=i.mState->copy();
	this->mScore=i.mScore;
	return *this;
}
void Detection::setState(State * state)
{
	mState=state->copy();
}
State * Detection::getState() const
{
	return this->mState->copy();
}


std::ostream& operator<< (std::ostream& out, const Detection& detection)
{ /*
	State * state=detection.getState();
	out<<state<<detection.getScore()<<" ";
	return out; */
}




