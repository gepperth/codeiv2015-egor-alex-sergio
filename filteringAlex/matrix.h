#ifndef MATRIX_H_
#define MATRIX_H_
#include "state.h"


class Vector
{

	vector<double> V;
public:

	Vector() {};
	Vector(vector<double> v)
	{
		V=v;
	}
	Vector(int size)
	{
		V.assign(size,0);
	}
	Vector(const double v[], int length)
	{
		vector<double> vtmp (v, v + length);
		V=vtmp;
	}
	~Vector() {};
	void set(vector<double> v)
	{
		V=v;
	}
	void setSize(int size)
	{
		V.assign(size,0);
	}
	int size()
	{
		return V.size();
	}
	Vector operator+ (Vector v)
	{
		if (this->V.size()!=v.V.size())
			return Vector(vector<double>());
		Vector result(V);
		for (unsigned int i=0; i<V.size(); i++)
			result.V[i]=this->V[i]+v.V[i];
		return result;
	}
	Vector operator- (Vector v)
	{
		if (this->V.size()!=v.V.size())
			return Vector(vector<double>());
		Vector result(V);
		for (unsigned int i=0; i<V.size(); i++)
			result.V[i]=this->V[i]-v.V[i];
		return result;
	}
	double& operator[] (int i)
	{
		return V[i];
	}
};

class Matrix
{
public:
	vector<double> M;
	int Nrow;
	int Ncol;
public:

	//Matrix();
	Matrix(int nrow, int ncol);
	Matrix(vector<double> m, int nrow, int ncol);
	Matrix(const double m[], int nrow, int ncol);
	~Matrix();
	void set(vector<double> m, int nrow, int ncol);

	Vector operator* (Vector v);
	Matrix operator* (Matrix matrix);
	Matrix operator+ (Matrix matrix);
	Matrix operator- (Matrix matrix);
	Matrix invert ();
	Matrix transpose ();
	double determinant();
	Matrix minorM(int i, int j);
	void show();
};


#endif /* MATRIX_H_ */
