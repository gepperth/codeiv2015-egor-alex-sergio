#ifndef PHDINTERFACE_H_
#define PHDINTERFACE_H_

#include "configuration.h"
#include "tracklets.h"
#include "phdFilter.h"
#include "matrix.h"

class PHDInterface
{
public:
	vector<Detection> detections;
	vector<Target> targets;
	PHDFilter phdfilter;
	vector<vector<Particle> > oldWeightedParticles;
	ifstream data;
	ofstream unions, filtered, particles, associations;
	Tracklets *tracklets;
	Configuration config;
	CarData cardata;
	int maxFrame;
	int nFrame;
	string line;
public:
	PHDInterface();
	~PHDInterface();
	int initialize(char *configName);
	int execute();
	void mergeDetections();
};




#endif /* PHDINTERFACE_H_ */
