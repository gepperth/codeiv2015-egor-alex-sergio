#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
using namespace std;

class Configuration
{
public:
	class Window
	{
	public:
		bool present;
		string framesIn;
		string framesOut;
		vector<int> sizes;
		int firstCoordinate;
		int secondCoordinate;
		double drawWidth;
		bool useDimensions;

		template<typename Archive> void serialize(Archive &ar, const unsigned int version){
			string strSizes;
		      ar & boost::serialization::make_nvp("present", present)
		      	 & boost::serialization::make_nvp("sizes", strSizes)
		         & boost::serialization::make_nvp("first", firstCoordinate)
		         & boost::serialization::make_nvp("second", secondCoordinate)
		      	 & boost::serialization::make_nvp("framesIn", framesIn)
			     & boost::serialization::make_nvp("framesOut", framesOut)
		      	 & boost::serialization::make_nvp("drawWidth", drawWidth)
		      	 & boost::serialization::make_nvp("useDimensions", useDimensions);
		      sizes.clear();
		      stringstream in(strSizes);
		      int i;
		      while (in >> i) sizes.push_back(i);
		      cout<<"	present: "<<present<<endl;
		      cout<<"	Size: "<<sizes[0]<<" "<<sizes[1]<<" "<<sizes[2]<<" "<<sizes[3]<<endl;
		      cout<<"	Coordinates: "<<firstCoordinate<<" "<<secondCoordinate<<endl;
		      cout<<"	framesIn "<<framesIn<<endl;
		      cout<<"	framesOut "<<framesOut<<endl;
		      cout<<"	useDimensions "<<useDimensions<<endl;

		    }
	};
	string mergedFile;
	string associatedFile;
	string filteredFile;
	string particleFile;
	string dataFile;
	string fieldFile;
	vector<double> sigmas;
	vector<bool> coordinatesToUse;
	double Pfn;
	double Pfb;
	double Pfd;
	int particlesPerTarget;
	int historyLength;
	bool mergeDetections;
	bool useTracklets;
	Window window1;
	Window window2;

	  // load tracklets from xml file
	  bool loadFromFile (std::string filename) {
	    try {
	      std::ifstream ifs(filename.c_str());
	      boost::archive::xml_iarchive ia(ifs,boost::archive::no_header);
	      ia >> boost::serialization::make_nvp("mergedFile", mergedFile);
	      cout<<"mergedFile "<<mergedFile<<endl;
	      ia >> boost::serialization::make_nvp("associatedFile", associatedFile);
	      cout<<"associatedFile "<<associatedFile<<endl;
	      ia >> boost::serialization::make_nvp("filteredFile", filteredFile);
	      cout<<"filteredFile "<<filteredFile<<endl;
	      ia >> boost::serialization::make_nvp("particleFile", particleFile);
	      cout<<"particleFile "<<particleFile<<endl;
	      ia >> boost::serialization::make_nvp("dataFile", dataFile);
	      cout<<"dataFile "<<dataFile<<endl;
	      ia >> boost::serialization::make_nvp("fieldFile", fieldFile);
	      cout<<"fieldFile "<<fieldFile<<endl;
	      string strSigmas;
	      ia >> boost::serialization::make_nvp("sigmas", strSigmas);
	      {
	    	  sigmas.clear();
	    	  stringstream in(strSigmas);
	    	  double i;
	    	  cout<<"Sigmas: ";
	    	  while (in >> i)
	    	  {
	    		  sigmas.push_back(i);
	    		  cout<<i<<" ";
	    	  }
	    	  cout<<endl;
	      }
	      string strCoordinatesToUse;
	      ia >> boost::serialization::make_nvp("coordinatesToUse", strCoordinatesToUse);
	      {
	    	  coordinatesToUse.clear();
	    	  stringstream in(strCoordinatesToUse);
	    	  bool i;
	    	  cout<<"coordinatesToUse: ";
	    	  while (in >> i)
	    	  {
	    		  coordinatesToUse.push_back(i);
	    		  cout<<i<<" ";
	    	  }
	    	  cout<<endl;
	      }

	      ia >> boost::serialization::make_nvp("Pfn", Pfn);
	      cout<<"Pfn "<<Pfn<<endl;
	      ia >> boost::serialization::make_nvp("Pfb", Pfb);
	      cout<<"Pfb "<<Pfb<<endl;
	      ia >> boost::serialization::make_nvp("Pfd", Pfd);
	      cout<<"Pfd "<<Pfd<<endl;
	      ia >> boost::serialization::make_nvp("particlesPerTarget", particlesPerTarget);
	      cout<<"particlesPerTarget "<<particlesPerTarget<<endl;
	      ia >> boost::serialization::make_nvp("historyLength", historyLength);
	      cout<<"historyLength "<<historyLength<<endl;
	      ia >> boost::serialization::make_nvp("mergeDetections", mergeDetections);
	      cout<<"mergeDetections "<<mergeDetections<<endl;
	      cout<<"Window1:"<<endl;
	      ia >> boost::serialization::make_nvp("window1", window1);
	      cout<<"Window2:"<<endl;
	      ia >> boost::serialization::make_nvp("window2", window2);

	      stringstream dataFileStream(dataFile);
	      string ext;
	      while (getline(dataFileStream, ext, '.'));
	      if (ext=="xml")
	    	  useTracklets=true;
	      else
	    	  useTracklets=false;
	      cout<<"useTracklets "<<useTracklets<<endl;


	    } catch (...) {
	      return false;
	    }
	    return true;
	  }
};



#endif /* CONFIGURATION_H_ */
