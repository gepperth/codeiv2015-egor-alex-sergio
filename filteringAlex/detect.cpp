/*
Syntax: ./detect cpu|gpu outputTextFile inputTextFile svTextFile [blo on jcksizeXY blockstrideXY cellsizeXY imgX imgY winsizeX winsizeY svmthreshold]
Assumption! assumes that SVM model was trained using a normalization by the nr of blocks in a window. As this normalization of feature vecs is not performed by detectMultiScale, the svm params (rho) and results are appropriately rescaled
*/

/* Observations:
- the resampling variances for dimension and speed are hard-set in state.cpp :: predict -- changed!!

- the vectors used ans sigmas are totalyly dcorrelated: used indexes coordinates, sigmas components of coordinates (pos, size, dir)

- likewise the scale dependency of prediction is hardcoded there

- teh resamplingCoeff is basically used to increase or decrease the sigmas during reqsampling. The sigmas thus do n,ot necessarily mean 
  anything by themselves
  
- no noise is added during prediction

- seems that opencv hogs vectors are not normalized to 1.0. So any linear SVM trained on normalized vecgtors needs to be adapted.

- new birth a	nd death model: unassigned dets create a track with prob mPDeltaPlus. Each assoc increases the prob by mPDeltaPlus, each missed assoc
  decreases it my mPdeltaMinus. When prob reqches mPb it is official, when track reaches mPd it gets deleted.

- pfn increases weight of each particle at ech step by probTrack*mPfn/nrParticles so that parts do not die in case of no assignments 
*/

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "filtering/detection.h"
#include "filtering/phdFilter.h"

using namespace std ;
using namespace cv ;

int area(CvRect r)
{
  return r.width * r.height ;
}


CvRect det2rect(const Detection & d)
{
  CvRect r;
  visualPlusLaserState * st =   dynamic_cast<visualPlusLaserState *> (d.getState()) ;
  r.x = st->xc - st->xs ; 
  r.y = st->yc - st->ys ; 
  r.width = 2*st->xs ;
  r.height = 2*st->ys ;
  return r;
}


int areaDet(const Detection & d)
{  
  
  return area(det2rect(d));
  
}


int intersect(CvRect & r1, CvRect & r2)
{
  CvRect intersection;

// find overlapping region
  intersection.x = (r1.x < r2.x) ? r2.x : r1.x;
  intersection.y = (r1.y < r2.y) ? r2.y : r1.y;
  intersection.width = (r1.x + r1.width < r2.x + r2.width) ? r1.x + r1.width : r2.x + r2.width;
  intersection.width -= intersection.x;
  intersection.height = (r1.y + r1.height < r2.y + r2.height) ? r1.y + r1.height : r2.y + r2.height;
  intersection.height -= intersection.y;
  
  if ((intersection.width <= 0) || (intersection.height <= 0)) 
    return 0 ;
  else
    return area (intersection) ;
}

float overlap(CvRect r1, CvRect r2)
{
  int inter = intersect(r1,r2) ;
  int join = area(r1) + area(r2) - inter ;
  
  return float (inter)/ float (join) ;
}


float overlapDets(Detection & d1, Detection & d2)
{
  return overlap(det2rect(d1),det2rect(d2)) ;
}

bool sortFunction (const Detection & d1, const Detection & d2) 
{
  return d1.getScore() > d2.getScore() ;
}



void maxSuppress (vector <Detection> & detects, float thrOver, float areaFactor ) 
{
  
  vector <bool> deleted(detects.size()) ;
  std::fill (deleted.begin(), deleted.end(), false ) ;
  
  std::sort(detects.begin(), detects.end(), sortFunction) ;
  
  if (detects.size()==0)
    return ;
  
  for(int killer=0; killer<detects.size(); killer++)
  {
    for (int victim=killer+1; victim < detects.size(); victim++)
    {
      if (overlapDets(detects[killer], detects[victim]) >= thrOver )
        deleted[victim] = true ;
    }
    
  }
  
  int k=0; int i=0 ;
  while (k<detects.size())
  {
    if (deleted[i])
    {
      // delete from detects
      detects.erase(detects.begin()+k) ;
      k-=1;
    }
    k++ ;
    i++ ;
  }
  

}

void maxSuppressGeneric (vector <Detection> & detects, float thrOver, float areaFactor, vector <bool> & deleted ) 
{
  
  deleted.resize(detects.size()) ;
  std::fill (deleted.begin(), deleted.end(), false ) ;
  
  std::sort(detects.begin(), detects.end(), sortFunction) ;
  
  if (detects.size()==0)
    return ;
  
  for(int killer=0; killer<detects.size(); killer++)
  {
    for (int victim=killer+1; victim < detects.size(); victim++)
    {
      if (overlapDets(detects[killer], detects[victim]) >= thrOver )
        deleted[victim] = true ;
    }
    
  }
/*  
  int k=0; int i=0 ;
  while (k<detects.size())
  {
    if (deleted[i])
    {
      // delete from detects
      detects.erase(detects.begin()+k) ;
      k-=1;
    }
    k++ ;
    i++ ;
  }
  */

}


// Used to sort two associated vectors
struct CmpWeight
{
	CmpWeight(vector<double>& vec) : values(vec){}
	bool operator() (const int& a, const int& b) const
	{
		return values[a] < values[b];
	}
	vector<double>& values;
};


double gauss(double x, double y, double x0, double y0, double sigma)
{
  return exp(-1.0/(2.0*sigma*sigma) * ((x-x0)*(x-x0)+(y-y0)*(y-y0))) ;
}



void modifyScoreImage (int scale, double scaleFactor, cv::Mat scores, vector <vector <Particle> > & particles, vector <double> & targetProbs, cv::Size win_size, cv::Size block_stride, double sigma, double targetProbThr, double coeff)
{
  ///return ;
  double realScale = pow(scaleFactor,scale) ;
  double widthScore = (float(win_size.width)*realScale*0.5);
  double heightScore =(float(win_size.height)*realScale*0.5);
  int index=0 ;
  for (int t=0; t<particles.size(); t++)
  if(targetProbs[t] >=targetProbThr)
    for(int p=0; p<particles[t].size(); p++)
    {
      const visualPlusLaserState * s= dynamic_cast<visualPlusLaserState *> (particles[t][p].getState()) ;
      //double w0=c0->mDimension ; // ****
      double w0=s->xs ;
//      double x0=coords[0].mCenter - w0; // WHY ?????
//      double y0=coords[1].mCenter - 2.*w0; // WHY ????
      double x0=s->xs ; // WHY ?????
      double y0=s->ys ; // WHY ????
      float particleScaleIndex = log(2.*w0/double(win_size.width) )/ log(sqrt(2.0)) ;
      //double h0=c1->mDimension ;
      double h0=s->ys ; // ****
      int deltaY = 10 ; 
      int deltaX = 10 ;
    //  cout << "Particle " << t << "/" << p << ": " << x0 << "/" << y0 << "/" << w0 << endl ;
      
      double tildey0 = int ((y0-2.*w0) / realScale / double(block_stride.height) ) ;
      double tildex0 = int ((x0-w0) / realScale / double(block_stride.width) ) ;
  //    cout << "Particle " << t << "/" << p << ": " << tildex0 << "/" << tildey0 << "/" << w0 << endl ;
      
      for(int y=int(tildey0)-deltaY;y< int(tildey0)+deltaY ; y++)
      {
        for(int x=int(tildex0)-deltaX; x<int(tildex0)+deltaX ; x++)
        {
//	    cout << "...." << endl ;
            if((y<0) || (y>=scores.rows))
              continue ;
            if((x<0) || (x>=scores.cols))
              continue ;
	    double & sc = scores.at<double>(y,x) ;
//	    cout << "...." << endl ;
	    
	    // determine CENTER coordinates of image coord bounding box
	    double xDet = double(x)*realScale*double(block_stride.width)+widthScore ;
	    double yDet = double(y)*realScale*double(block_stride.height)+heightScore ;
	  
	    float size=s->ys ;
	    //float size=c1->mDimension ;
	    float w= particles[t][p].getWeight();	    
	    //cout << w << endl ;
  	    //double coeffScale=gauss(0,double(scale), 0, particleScaleIndex,1.) ;
  	    double coeffSpace = gauss (xDet,yDet,x0,y0,double(widthScore)) ;
  	    
  	    double coeffScale=gauss(0,scale, 0, particleScaleIndex, 1.0) ;
  	//    if (coeffSpace >= 0.5)
    	//      cout << "!!" << w0/float(win_size.width) << endl ;
  	    //if (coeffSpace >= 0.0 &&coeffScale >= 0.9)
  	    //  cout << xDet << " " << yDet << " " << x0 << " " << y0 << " " << scale << " " << particleScaleIndex <<  " " << realScale << endl ;  	    
/*   if (fabs(scale - particleScaleIndex) < 0.5)
  	      cout << scale - particleScaleIndex << " " << scale << " " << coeffScale << endl;
  	    if (coeffSpace >= 0.5 && coeffScale >= 0.9)
  	    {
  	      cout << x << " " << y << " " << x0 << " " << y0 << " " << scale << " " << particleScaleIndex <<  endl ;  	    
  	      cout << coeffSpace << " " << coeffScale << " " << w << endl ;
  	    } */
  	    
  	    sc += coeff*w*coeffSpace*coeffScale ;
	    
	  }
    }
  }
}


void drawParticles(cv::Mat & img, vector <Target *> & targets, vector <vector <Particle > > & particles, vector <double> & targetProbs, double targetProbThr, int shade, bool drawTargets, bool drawParticles, bool drawSpeed)
{
    cout << "Draxing " << targets.size() << "targets" << " " << targetProbThr << endl ;
    for (int i=0; i<targets.size(); i++)
    {
      cout << "probab of target "<< i << " " << targetProbs[i] << endl ;
    if(targetProbs[i] >=targetProbThr)
    {
        
        visualPlusLaserState * st2 = dynamic_cast<visualPlusLaserState*>(targets[i]->getState()) ;
        
        int xLeft=st2->xc - st2->xs ;
        int xRight=st2->xc + st2->xs ;
        int yTop=st2->yc - st2->ys ;
        int yBottom=st2->yc + st2->ys ;

        drawTargets=true;
        if(drawTargets)
        {
          CvRect r;
          r.x = xLeft; r.y = yTop ;
          r.width = xRight-xLeft;
          r.height = yBottom-yTop ;
          rectangle(img, r, CV_RGB(255,0,255), 3) ;
	}
	
	for (int p=0; p<particles[i].size(); p++)
	{
	  const visualPlusLaserState * s= dynamic_cast<visualPlusLaserState*>(particles[i][p].getState()) ;
  	  float xSpeed=s->xv ;
  	  float ySpeed=s->yv ;
	  float x=s->xc ;
	  float y=s->yc ;
	  
	  float size=s->ys * 0.1 ;
	  float w= particles[i][p].getWeight();
	  //cout << x << " " << y << " " << size << endl ;
	  if( drawParticles)
	  {
  	    line(img,cv::Point(x+0.5*size,y+0.5*size), cv::Point(x-0.5*size,y-0.5*size),CV_RGB(shade,shade,shade),1,CV_AA,0);
	    line(img,cv::Point(x+0.5*size,y-0.5*size), cv::Point(x-0.5*size,y+0.5*size),CV_RGB(shade,shade,shade),1,CV_AA,0);
	  }
	  if(drawSpeed)
          { //cout << "speed draw!" << xSpeed << " " << ySpeed << endl ;
  	    line(img,cv::Point(x,y), cv::Point(x+size*xSpeed,y+size*ySpeed),CV_RGB(50,50,50),1,CV_AA,0);
          }
	}
	
    }
    }

}


void getTargetProbsArray(PHDFilter & filter, vector<double> & targetProbs)
{
  targetProbs.resize(0) ;
  for (int z=0; z<filter.getTargetsNumber(); z++)
    targetProbs.push_back( filter.getTargetProbability(z) )  ;
}

class point2d3d
{
  public:
  float x,y,z ;
  float x2d,y2d ;
  const float dist3d(const point2d3d & py) {return sqrt(  (x-py.x)*(x-py.x)+(y-py.y)*(y-py.y)+(z-py.z)*(z-py.z) ) ;} 
} ;




void readLaserData(std::string& laser2dFile, std::string&laserPointsFile, vector <point2d3d> & points )
{
  std::vector <float> laser2dX; 
  std::vector <float> laser2dY;
  std::vector <float> laser3dX; 
  std::vector <float> laser3dY;
  std::vector <float> laser3dZ ;
  ifstream p3d (laserPointsFile.c_str()) ;
  ifstream p2d (laser2dFile.c_str()) ;
  laser2dX.resize(0) ;
  laser2dY.resize(0) ;
  laser3dX.resize(0) ;
  laser3dY.resize(0) ;
  laser3dZ.resize(0) ;
  
  while (!p3d.eof())
  {
    float v1,v2,v3,v4,v5 ;
    p3d >> v1 >> v2 >> v3 >> v4 >> v5 ;
    
    laser3dX.push_back(v3) ;
    laser3dY.push_back(v4) ;
    laser3dZ.push_back(v5) ;
    p2d >> v1 >> v2  ;
//    cout << v1 << v2 << endl ;
    laser2dX.push_back(v1) ;
    laser2dY.push_back(v2) ;        
  }
  
  p3d.close() ;
  p2d.close() ;
  
  points.resize(0) ;
  point2d3d pt;
  for (int i=0; i<laser2dX.size(); i++)
  {
    pt.x = laser3dX[i] ;
    pt.y = laser3dY[i] ;
    pt.z = laser3dZ[i] ;
    pt.x2d = laser2dX[i] ;
    pt.y2d = laser2dY[i] ;
    points.push_back(pt) ;
  }
}                   


void laserPointsInDetection(CvRect r, vector<point2d3d> & points, vector <point2d3d> & result )
{
  result.resize(0) ;
  
  float xMin;float yMin; float xMax;float yMax;
  xMin = r.x ; yMin = r.y ; xMax = r.x+r.width ; yMax = r.y+r.height ;
  
  for (vector<point2d3d>::iterator el = points.begin() ; el != points.end(); el++)
  {
    if ((xMin <= (*el).x2d) && (xMax >= (*el).x2d) && (yMin <= (*el).y2d) && (yMax >= (*el).y2d))
      result.push_back(*el);
  }  
}

bool predicate(const point2d3d & a, const point2d3d & b)
{
  if (a.dist3d(b) >= 0.5)
    return false ;
  else
    return true ;
}

void findClosestSubCloud (vector <point2d3d> & cloud, vector <point2d3d> & result)
{
  vector <int> labels ;
  cv::partition<point2d3d>(cloud, labels, predicate) ;
   
  point2d3d zero ;
  
  // how many clusters?
  int maxLabel = -1 ;
  for (int l=0; l<labels.size(); l++)
    if (labels[l] > maxLabel) maxLabel = l;
    
  //cout << "nrClusters " << maxLabel << endl ;
    
  // min disdtances in each cluster
  vector <float> minDistances (maxLabel+1);
  for (vector<float>::iterator f=minDistances.begin() ; f != minDistances.end(); f++)
    (*f)=10000000. ;
    
  zero.x=0 ; zero.y = 0 ;zero.z = 0 ; 
  for (int el = 0 ; el < cloud.size(); el++)
  { 
    int label = labels[el] ;
    if (zero.dist3d(cloud[el]) < minDistances[label])
      minDistances[label] =zero.dist3d(cloud[el]) ;
  }
  //cout << "min didstances" << endl ;
  //for (vector<float>::iterator f=minDistances.begin() ; f != minDistances.end(); f++)
  //  cout << (*f) << endl ;
  
  
  
  int bestLabel = -1 ;
  float bestDist = 1000000000;
  for (int l=0; l <= maxLabel; l++)
    if (minDistances[l] < bestDist)
    {
      bestLabel = l ;
      bestDist = minDistances[l] ;
    }
//  cout << "best Label "<< bestLabel << endl ;
      
  result.resize(0) ;
  for (int el = 0 ; el < cloud.size(); el++)
    if(labels[el]==bestLabel)
      result.push_back(cloud[el]) ;
  
  
}


void calcCOG(vector <point2d3d> & cloud, float & x, float & y, float & z)
{
  x = y = 0. ;
  int sz = cloud.size() ;
  
  for (int el=0; el<sz; el++)
  {
    x+=cloud[el].x ; // left/right
    y+=cloud[el].y ; // far/near
  }
    
  x /= float(sz) ;
  y /= float(sz) ;
  z = 1.0 ;
        
  
}



int main(int argc, char ** argv)
{
  int diffX=1;
  int diffY = 1;
  if(argc<5)
  {
    printf("%s\n",argv[1]);
  }

  bool useCpu = (strcmp(argv[1],"cpu")==0) ;
  bool useGpu = (strcmp(argv[1],"gpu")==0) ;
  bool useBoth = (strcmp(argv[1],"both")==0) ;
  if( useBoth )
    useGpu=true, useCpu=true;

  ofstream outFile; outFile.open(argv[2]);
  ifstream listfile; listfile.open (argv[3]) ;
  ifstream svFile; svFile.open (argv[4]) ;
  CvSize block_size = Size(8, 8) ;
  if (argc > 5) 
  {
    CvSize & s = block_size; int val=atoi(argv[5]);
    s.width=val;  s.height=val;
  }

  CvSize block_stride = Size(4, 4) ;
  if (argc > 6) 
  {
    CvSize & s = block_stride; int val=atoi(argv[6]);
    s.width=val;  s.height=val;
  }

  CvSize cell_size = Size(4, 4) ;
  if (argc > 7) 
  {
    CvSize & s = cell_size; int val=atoi(argv[7]);
    s.width=val;  s.height=val;
  }

  CvSize img_size = Size(800, 600) ;
  if (argc > 8) 
  {
    int val1=atoi(argv[8]);
    int val2=atoi(argv[9]);
    img_size.width=val1;  img_size.height=val2;
  }

  CvSize win_size = Size(32, 64) ;
  if (argc > 10) 
  {
    printf("set winsize to %s %s\n", argv[10],argv[11]) ;
    int valx=atoi(argv[10]);
    int valy=atoi(argv[11]);
    win_size.width=valx;  win_size.height=valy;
  }

  float svmThr = 0.0 ;
  if (argc > 12) 
  {
    svmThr=atof(argv[12]);
  }
  //svmThr=0.5 ;
  
  
  float overlapThr = 0.0 ;
  if (argc > 13) 
  {
    overlapThr=atof(argv[13]);
  }
  
  float areaFactor = 0.0 ;
  if (argc > 14) 
  {
    areaFactor=atof(argv[14]);
  }

  int enableFeedback = 0 ;
  if (argc > 15) 
  {
    enableFeedback=atoi(argv[15]);
  }

  float trackThr = svmThr ;
  if (argc > 16) 
  {
    trackThr=atof(argv[16]);
  }

  

  int nBins = 9;


  float nrBlocksInWindowX = (float(win_size.width) - float(block_size.width)) / float (block_stride.width) +1.0;
  float nrBlocksInWindowY = (float(win_size.height) - float(block_size.height)) / float (block_stride.height) +1.0;
  float nrBlocksInWin = nrBlocksInWindowY * nrBlocksInWindowX ;
  float norm = float(nrBlocksInWin) ;
  //float norm = 1.0;
  CvSize window_stride = block_stride;
  printf("Params are: winSize=%d/%d, blockSize=%d, blockStride=%d, cellSize=%d, imgsize=%d %d\n", win_size.width, win_size.height, 
    block_size.width, block_stride.width, cell_size.width, img_size.width, img_size.height) ;
  printf("svmThreshold=%f\n",svmThr) ;

  HOGDescriptor hog (win_size, // win_size
                     block_size, // block_size, 
                     block_stride, // block_stride
	     	     	 cell_size, // cell_size
                     nBins,          // nbins , 
                     1,   // derivAperture
                     -1, // win_sigma , 
                     HOGDescriptor::L2Hys, 
                     0.2,        // threshold_L2hys 
                     false,   // gamma_correction
                     1 // nLevels
                      ); 



  //printf("descr size is %d\n", (int) hog.getDescriptorSize()) ;
  // ********************
  int startImg = 11450 ;
  startImg = 0 ;
  int startScale=0;
  int lastScale = 5;
  bool display = true ;
  //display=false ;

  bool drawParts=true;
  bool drawTargets=true;
  bool drawSpeed=true;

  vector<double> sigmas;
  vector<bool> used;
  sigmas.push_back(5);
  sigmas.push_back(5);
  sigmas.push_back(2);
  sigmas.push_back(0.5);
  sigmas.push_back(0.5);
  float resampleCoef = 0.3 ;
  float gaussWidth = 1 ;
  float mInfluenceCoeff = 0.5 ;
  float mAssociationCoeff = 0 ;
  float mPd = 0.5, mPb = 0.5, mPdeltaPlus=0.4, mPdeltaMinus=0.1 ;
  float mPfn = 0.2 ;
  int nrParticles = 500 ;
  float influenceStrength = 1.1 ;
  float targetProbThr=mPd;



  // ********************

  CvSize img_sizes[10] ;
  img_sizes[0] = img_size;
  int nbr_windowsX[10] ;
  int nbr_windowsY[10] ;
  int nbr_windows[10] ;
  int total_nbr_windows=0;
  for(int i=startScale+1;i<=lastScale;i++) 
  {
        img_sizes[i].width=cvRound(((float) img_sizes[i-1].width)/1.41);
        img_sizes[i].height=cvRound(((float) img_sizes[i-1].height)/1.41);
  }
  for(int i=startScale;i<=lastScale;i++)
  {
        nbr_windowsX[i] = (img_sizes[i].width - win_size.width)/window_stride.width + 1 ;
        nbr_windowsY[i] = (img_sizes[i].height - win_size.height)/window_stride.height + 1 ;
        nbr_windows[i] = nbr_windowsX[i]*nbr_windowsY[i];
        total_nbr_windows+=nbr_windows[i];
        //printf("NxFenetres[%d] = %d, NyFenetres[%d] = %d, NFenetres[%d] = %d\n",i,nbr_windowsX[i],i,nbr_windowsY[i],i,nbr_windows[i]);
  }
  printf("NbrMaxDetections = %d\n",total_nbr_windows);


  // read sv into matrix
  // how can  i correct for svm that has been trained on normalized vectors, ie where the whole hog vector has L2 norm 1?
  if (strcmp(argv[4],"__default__")!=0)
  {
    cout << "Reading " << 1+hog.getDescriptorSize() << "entries from " << argv[4] << endl ;
    cv::Mat sv ;
    sv.create(1,1+hog.getDescriptorSize(),CV_32F) ;
    sv = cvScalar(0) ;
    float entry;
    int index=0 ;
    //norm = sqrt(float(nrBlocksInWin)) ;
    norm = -1. ;
    while (svFile >> entry)
      sv.at<float>(0,index++)=entry ;
    svFile.close() ;
    printf("Read %d values from sv file\n", index) ;
    sv.at<float>(0,hog.getDescriptorSize()) *= -1.*norm;
    printf("rho value is %f\n",  sv.at<float>(0,hog.getDescriptorSize())) ;
    hog.setSVMDetector(sv) ;
  }
  else
  {
   // This det expects 16 8 8 0 params and a 48x96 win size, be sure to give this on the cmd line
   cout << "Loading daimler 48x96 people detector" << endl ;
   hog.setSVMDetector(HOGDescriptor::getDaimlerPeopleDetector());
  }



  // initialize tracking  
  PHDFilter filter;
  vector<vector<Particle> > particles;
  vector<Target> targets(0);
  filter.initialize(nrParticles,sigmas,used,mPfn,15,"");
  filter.mInfluenceCoef=mInfluenceCoeff ;
  filter.mResampleCoef=resampleCoef ;
  filter.mAssociationCoef=mAssociationCoeff ;
  filter.mPd = mPd ;
  filter.mPb = mPb ;
  filter.mPdeltaPlus = mPdeltaPlus ;
  filter.mPdeltaMinus = mPdeltaMinus ;

  
  ofstream logFileTracking("./logFileTracking.txt") ;
  
  int imgIndex=0 ;

  if(display)
  {
    for (int sc=startScale; sc <=lastScale; sc++)
    {
      
      cvNamedWindow( ("Scale "+std::to_string(sc)).c_str(), CV_WINDOW_AUTOSIZE );
    }
  }

  

  while(!listfile.eof())
  {
    int ts;
    string imgFile,laserPointsFile, laser2dFile ;
    listfile >> ts;
    listfile >> imgFile >> laser2dFile >> laserPointsFile ;
    cv::Mat img[10];
    if (++imgIndex < startImg) continue ;
    
    img[0] = imread(imgFile.c_str(), 0);

    vector<point2d3d> points ;
    readLaserData(laser2dFile, laserPointsFile, points) ;
    
    //cout << "uimg file is " << imgFile << endl ;
    cout << "uimg file is " << imgFile << endl ;
    
    for(int sc = (startScale==0)?1:startScale ; sc <= lastScale ; sc++)
    {
      resize(img[ 0 ], img[ sc ], img_sizes[ sc ]);
//      cout << sc << endl ;
    }
                
    vector <double> weights[10] ;    
    vector<Point> locations[10] ;

    for(int sc = startScale ; sc <= lastScale ; sc++)
      fill(weights[sc].begin(), weights[sc].end(), 0.0) ;
               
    
    // svmThr replaced by newThr in order to have all detections
    double newThr = -300; //

    int i=0 ;
    for(int sc = startScale ; sc <= lastScale ; sc++)
    {
      hog.detect(img[sc],locations[sc],weights[sc],newThr, window_stride,Size(0,0));
      /*
       // debug code
      std::vector<float> descr ;
      hog.compute(img[sc],descr);
      cout << "vec siez is " << descr.size()/nbr_windows[sc] << endl ;
      float acc=0 ;
      for(int k=0;k<descr.size()/nbr_windows[sc];k++) acc+= descr[k]*descr[k] ;
      cout << "vec norm is " << acc << endl ;
      */
      //cout << "detected at " << i++  << endl ;
    }


    
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // draw old particles then predicted ones
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // if not first iteration: draw old particles!    
    
    filter.prediction() ;
    
    vector <double> targetProbs (0) ;
    particles=filter.getParticles();
    getTargetProbsArray(filter, targetProbs) ;
    drawParticles(img[0], filter.mTargets, particles, targetProbs, targetProbThr, 250, drawTargets, drawParts, drawSpeed) ;


    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // transform linear score arrays into 2D matrices + count detections
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    cv::Mat scaledWeights[10];
    int nbDetections = 0;
    for(int i=startScale; i<=lastScale; i++)
    {
      scaledWeights[i] = cv::Mat(weights[i]).reshape(1,nbr_windowsY[i]);
      if( enableFeedback != 0 )
        modifyScoreImage(i, 1.41, scaledWeights[i], particles, targetProbs, win_size, window_stride, 
                         1.0, targetProbThr, influenceStrength)  ;
      for(int y=0;y< scaledWeights[i].rows ; y++)
      {
        for(int x=0;x<scaledWeights[i].cols ; x++)
        { 
          if(scaledWeights[i].at<double>(y,x) > svmThr)
            nbDetections++  ;
        } 
      }
    }
    cout << "transformation complete" << endl ;
    
    vector<Detection> detects(0), detectsLaser(0);     
    stringstream detections;
    // hack to exclude vehicle detections on bonnet of car
    int lowerBorder = 5800 ;
    
    outFile << imgFile << " " << ts << " " << nbDetections << endl ; 

    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // create a vector of detections for NMS and fe"eding to tracker  
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    for(int sc=startScale;sc<=lastScale;sc++)
    {
      double scale = pow(sqrt(2),sc) ;
      int width = round(float(win_size.width)*scale);
      int height = round(float(win_size.height)*scale);
	
	
      int index=0 ;
      for(int y=0;y< scaledWeights[sc].rows ; y++)
      {
        for(int x=0;x<scaledWeights[sc].cols ; x++)
        {
          int xLeft = round(x*scale*window_stride.width);
          int yTop = round(y*scale*window_stride.height);
          int yBottom = yTop + height -1;
          int xRight = xLeft + width -1;
          if(scaledWeights[sc].at<double>(y,x) > svmThr)
          {
		    //std::cout << scaledWeights[sc].at<double>(y,x) << endl ;
		    //std::cout << x << y << sc << endl;
            vector <point2d3d> cloudTmp,cloud ; 
            CvRect r =  cvRect(xLeft,yTop,width,height) ;
            laserPointsInDetection  (r,points, cloudTmp) ;
            findClosestSubCloud(cloudTmp,cloud) ;
            float x3d,y3d,z3d ;
            calcCOG(cloud,x3d,y3d,z3d) ;
            cout << "3D COGS are " << x3d << " " << y3d << endl ;
            nbDetections++; index++ ;
       	            
            outFile <<   scaledWeights[sc].at<double>(y,x) << " " << xLeft << " " << yTop  << " " << xRight << " " << yBottom << " " 
              << endl ;
            visualPlusLaserState * state = new visualPlusLaserState ((xLeft+xRight)/2.0, abs(xLeft-xRight)/2.0, 0, (yTop+yBottom)/2.0, abs(yBottom-yTop)/2.0, 0, x3d, y3d, 0.,0.) ;
            
            Detection detection(state,scaledWeights[sc].at<double>(y,x));
            delete state ;
           
            if (cloud.size()  >=2)
              detects.push_back(detection);                      
			    
          }
   	            
        }
      }
   	        
    }
    cout << "det feeding complete" << detects.size() << " " << detectsLaser.size() << endl ;


    vector <bool> deleted ;
    vector <Detection> detectsTracking (0);
    
    maxSuppressGeneric( detects, 0.05, 1.0,deleted) ;    
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    // draw detections with laser points
    // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    
    for (int d=0; d<detects.size(); d++)
      if (deleted[d] == false)
      {
        
        detectsTracking.push_back(detects[d]) ;         
      }
      
    
    

    cout << "maxSup complete" << detects.size() << " " << detectsTracking.size() << endl ;

    
    filter.association(detectsTracking, logFileTracking);
    cout << "assoc complete" << detects.size() << " " << detectsTracking.size() << endl ;
    filter.observation();
    filter.resample();
    //filter.merge(logFileTracking);
    cout << "filter computations complete, targets=" << filter.mTargets.size() << ", parts= " << particles.size() << "parts" << endl ;

    filter.correction(); // for next iteration
    cout << "filter computations complete, " << filter.mTargets.size() << " targetas abd " << particles.size() << "parts" << endl ;
    cout << "tracker feeding complete" << detects.size() << " " << detectsTracking.size() << endl ;
    getTargetProbsArray(filter, targetProbs) ;
    for (int s=0; s<filter.mTargets.size(); s++) cout << "prob " << s << " " << targetProbs[s] << endl ;
    

    
    
    
//    bool display = false ;
    if (display)
    {
        // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        // draw detections
        // $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        for (int d=0; d<detects.size(); d++)
        {
          if (deleted[d]==true)
            continue ;
          CvRect r = det2rect(detects[d]) ;
          vector <point2d3d> cloudTmp,cloud ; 
          laserPointsInDetection  (r,points, cloudTmp) ;
          findClosestSubCloud(cloudTmp,cloud) ;
          rectangle(img[0], r, CV_RGB(255,0,255), 1) ;
          for (vector<point2d3d>::iterator el = cloud.begin() ; el != cloud.end() ; el++)
            rectangle(img[0], cvRect((*el).x2d, (*el).y2d, 10,10), CV_RGB(255,255,255), 3) ;        
        }
    
        // normalize and display if required
        for(int i=startScale; i<=lastScale; i++) normalize(scaledWeights[i], scaledWeights[i], 0.0, 1.0, NORM_MINMAX); 
        for(int i=startScale; i<=lastScale; i++)       
        {
          imshow( "Scale "+std::to_string(i), scaledWeights[i] );
        }
        imshow("img",img[0]);
        char c = waitKey(10);  
        printf("key is %c\n", c) ;
        if (c==' ')
        {
          c = 'x' ;
          while(c!=' ')
            c = waitKey(10) ;
          
        }
    }
    /*
    for( int i=startScale; i<lastScale; i++) 
    {
          char name [1000] ;
          scaledWeights[i] *= 255. ;
          sprintf(name, "img%d_%d.png", ts, i) ;
          imwrite(name, scaledWeights[i]); 
    }*/


    //cout << "Processed " << imgFile << ": " << nbDetections << " hits" << endl; //<< nrHits << " after maxSup" << endl ;
    //WRITE IMG TO fiLE
    /*char outn[1000] ;
    sprintf(outn, "out/%d.png",ts) ;
    cv::resize(img[0],img[2],Size(), 0.5, 0.5, cv::INTER_AREA) ;
    imwrite(outn, img[2]) ;  */

  }

  outFile.close() ;

}

