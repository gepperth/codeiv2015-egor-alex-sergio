#include "field.h"


#define MFORCE 0.1

void VectorField::load(string filename)
{
    ifstream in(filename, ios::binary);
    int size;
    double tmp;
    mField.clear();
    in.read((char*)&fW, sizeof(int));
    in.read((char*)&fH, sizeof(int));
    mField.assign(fW*fH,vector<Direction>());
	for (int i=0; i<fW; i++)
	{
		for (int j=0; j<fH; j++)
		{
			in.read((char*)&size, 4);
			mField[fH*i+j].assign(size,Direction());
			for (int k=0; k<size; k++)
			{
				in.read((char*)&tmp, sizeof(double));
				mField[fH*i+j][k].mD.push_back(tmp);
				in.read((char*)&tmp, sizeof(double));
				mField[fH*i+j][k].mD.push_back(tmp);
				mField[fH*i+j][k].mWeight=1.0/size;
			}
		}
	}
	in.close();
}

vector<Direction> VectorField::get(const State& state,double &modelForce)
{
	SpaceCoordinate *si=dynamic_cast<SpaceCoordinate*>(state[0]);
	SpaceCoordinate *sj=dynamic_cast<SpaceCoordinate*>(state[1]);
	int i=si->mCenter+164;
	int j=sj->mCenter+147;
	vector<Direction> directions;
	if (i>=fW||j>=fH||j<0||i<0)
		modelForce=0.0;
	else
	{
		directions=mField[fH*i+j];
		modelForce=MFORCE;

		if (directions.size()==0)
		{
			modelForce=0.0;
		}
		else
		{
			double newNorm=sqrt(pow(si->mDirection,2.0)+pow(sj->mDirection,2.0));
			for (unsigned int k=0; k<directions.size(); k++)
			{
				double oldNorm=sqrt(pow(directions[k].mD[0],2.0)+pow(directions[k].mD[1],2.0));
				if (oldNorm!=0.0)
					for (int l=0; l<2; l++)
						directions[k].mD[l]*=newNorm/oldNorm;
			}
		}
	}
	return directions;
}


