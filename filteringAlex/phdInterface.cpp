#include "phdInterface.h"


PHDInterface::PHDInterface()
{
	tracklets=NULL;
	maxFrame=0;
	nFrame=0;
}
PHDInterface::~PHDInterface()
{
	unions.close();
	filtered.close();
	particles.close();
	associations.close();
	data.close();
	if (tracklets!=NULL)
		delete tracklets ;
}

void PHDInterface::mergeDetections()
{
	vector<double> dblmax(detections.size(),DBL_MAX);
	vector<vector<double> > overlaps(detections.size(),dblmax);

	for (unsigned int j=0; j < detections.size(); j++)
	{
		for (unsigned int i=j+1; i < detections.size(); i++)
		{
			overlaps[j][i]=detections[j].getState().intersectionRate(detections[i].getState(),config.coordinatesToUse);
		}
	}
	bool stillMerge=true;
    int iMax, jMax;
    vector<int> indToDelete;
	while (stillMerge)
	{
	        double max=0;
	        stillMerge=false;
	    	for (unsigned int j=0; j < detections.size(); j++)
	        {
	        	for (unsigned int i=j+1; i < detections.size(); i++)
	        	{
	        		if (max<overlaps[j][i])
	        		{
	        			max=overlaps[j][i];
	        			iMax=i;
	        			jMax=j;
	        		}
	        	}
	        }

	    	if (max>0.8)
	    	{
	    		if (detections[iMax].getScore()>detections[jMax].getScore())
	    		{
	    			detections[iMax].setState(detections[iMax].getState().merge(detections[jMax].getState(),config.coordinatesToUse));
		    		for (unsigned int i=0; i < detections.size(); i++)
		    			overlaps[jMax][i]=0;
		    		indToDelete.push_back(jMax);
	    		}
	    		else
	    		{
	    			detections[jMax].setState(detections[iMax].getState().merge(detections[jMax].getState(),config.coordinatesToUse));
		    		for (unsigned int j=0; j < detections.size(); j++)
		    			overlaps[j][iMax]=0;
		    		indToDelete.push_back(iMax);
	    		}
	    		stillMerge=true;
	    	}
	}
	vector<Detection> newDetections;
	for (unsigned int i=0; i<detections.size(); i++)
	{
		if (find(indToDelete.begin(), indToDelete.end(), i)==indToDelete.end())
			newDetections.push_back(detections[i]);
	}
	detections=newDetections;
}


int PHDInterface::initialize(char* configName)
{

	ifstream conf;
	unions.close();

	filtered.close();
	particles.close();
	associations.close();

	data.close();
	detections.clear();
    targets.clear();

	nFrame=-1;

	bool goodFiles;
	if(!config.loadFromFile (configName))
		cout<<"BAD CONFIG"<<endl;


	unions.open (config.mergedFile);

	filtered.open (config.filteredFile);
	particles.open (config.particleFile);
	associations.open(config.associatedFile);

	if (config.useTracklets)
	{

		if (tracklets!=NULL)
			delete tracklets ;
		tracklets = new Tracklets();
		goodFiles=tracklets->loadFromFile(config.dataFile);

		string folder="/home/egor/dataForTracking/oxts/data/";
		cardata.read(folder,233);
		//cardata.print();
	}
	else
	{
		data.open(config.dataFile);
		goodFiles=data.good();
	}

	goodFiles=goodFiles&&unions.good()&&associations.good()&&filtered.good()&&particles.good();


	if (!goodFiles)
		return -1;
	State state_init, current_state, randomState;
	int max=0;

	if (config.useTracklets)
	{
		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			max=std::max(max,tracklets->getTracklet (i)->lastFrame());
		}
		line="# "+ std::to_string(nFrame+1);
	}
	else
	{
		cout<<"noTracklets"<<endl;

		while( getline ( data, line ) )
		{
			if(line[0]=='#')
		    	max++;
		}
		data.close();
		data.open(config.dataFile);
		getline(data,line);
	}
	maxFrame=max;

	phdfilter.initialize(config.particlesPerTarget,
						 config.sigmas,
						 config.coordinatesToUse,
						 config.Pfn,
						 config.historyLength,
						 config.fieldFile);

	return max;

}


int PHDInterface::execute()
{

	nFrame++;

	Tracklets::tPose *pose;

	State current_state, randomState;
	double x_noised,y_noised,h,w,l;

	detections.clear();
	targets.clear();

	filtered<<line<<endl;
	particles<<line<<endl;
	unions<<line<<endl;

	associations<<line<<endl;

	if (config.useTracklets)
	{
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();

		normal_distribution<double> distributionX(0.0,0.5);
		normal_distribution<double> distributionY(0.0,0.5);
		default_random_engine generator(seed);

		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			if (tracklets->getPose (i, nFrame, pose))
			{
				x_noised=pose->tx;//+distributionX(generator);
				y_noised=-pose->ty;//+distributionY(generator);
				h=tracklets->getTracklet(i)->h;
				w=tracklets->getTracklet(i)->w;
				l=tracklets->getTracklet(i)->l;

				double alpha=-cardata.positions[nFrame].yaw;
				double xx[] = {x_noised, y_noised};
				double T[] = {cos(alpha), -sin(alpha),
							   sin(alpha), cos(alpha)};

				Matrix Tm(T,2,2);
				Vector X(xx,2);
				Vector Y;

				Y=Tm*X;

				x_noised=Y[0]-cardata.positions[nFrame].n;
				y_noised=Y[1]+cardata.positions[nFrame].e;


				vector<Coordinate*> coordinates;
				coordinates.push_back(new SpaceCoordinate(x_noised,w,0));
				coordinates.push_back(new SpaceCoordinate(y_noised,h,0));
				coordinates.push_back(new SpaceCoordinate(pose->tz,l,0));

				current_state.set(coordinates);

				Detection current_detection(current_state,1.0);


				detections.push_back(current_detection);

			}
		}
		line="# "+ std::to_string(nFrame+1);
	}
	else
	{
		string item;
		double x1,y1,x2,y2,z;
		int id;


		while (getline(data,line)&&line[0]!='#')
		{
			stringstream ss(line);
			ss>>x1>>y1>>x2>>y2>>item>>item>>item>>z>>id;
			z=z/100.0;

			w=(x2-x1)/2.0;
			h=(y2-y1)/2.0;
			x_noised=(x1+x2)/2.0;
			y_noised=(y1+y2)/2.0;

			vector<Coordinate*> coordinates;
			coordinates.push_back(new SpaceCoordinate(x_noised,w,0));
			coordinates.push_back(new SpaceCoordinate(y_noised,h,0));
			coordinates.push_back(new SpaceCoordinate(z,0,0));

			current_state.set(coordinates);

			Detection current_detection(current_state,1.0);

			current_detection.setID(id);
			detections.push_back(current_detection);
		}
	}

	if (config.mergeDetections)
		mergeDetections();


	targets=phdfilter.evaluate(detections,oldWeightedParticles,associations,unions);


	for (unsigned int i=0; i<targets.size(); i++)
	{
		filtered << targets[i];
	}
	phdfilter.printParticles(particles);

	if (nFrame<maxFrame-1)
		return nFrame;
	return -1;
}



