#include "target.h"



Target::Target()
{
	mHistoryLength=2;
	mID=-1;
	mValidity=1.0;
	mModelForce=0.1;
	mHistory.resize(0) ;
	
}
Target::Target(int id, int historyLength)
{
	mID=id;
	mHistoryLength=historyLength;
	mValidity=1.0;
	mModelForce=0.1;
}
Target::~Target()
{ /*
  cout << "target destructor" << endl ;
  for (list<State*>::iterator i = mHistory.begin(); i != mHistory.end(); i++)
    if((*i) != 0)
      delete (*i) ;
  cout << "end target destructor" << endl ;  */

}
void Target::setID(int id)
{
	mID=id;
}
int Target::getID() const
{
	return mID;
}
void Target::setProbability(double probability)
{
	mProbability=probability;
}
double Target::getProbability() const
{
	return mProbability;
}
void Target::setValidity(double validity)
{
	double lambda=0.5;
	mValidity=lambda*validity+(1-lambda)*mValidity;
}
double Target::getValidity() const
{
	return mValidity;
}
Target Target::operator=(const Target i)
{
	this->mHistory=i.mHistory;
	this->mHistoryLength=i.mHistoryLength;
	this->mProbability=i.mProbability;
	this->mID=i.mID;
	this->mValidity=i.mValidity;
	return *this;
}
void Target::setState(State * state)
{
	while ((int)mHistory.size()>mHistoryLength)
	    mHistory.pop_front();
	mHistory.push_back(state->copy());
}

void Target::updateSpeed()
{
  State * first = *(this->mHistory.begin()) ;
  if (this->mHistory.size() >= 2)
    first->setDynamics(first, *((this->mHistory.begin())++)) ;
  else
    first->setSpeedsZero() ;
}


State * Target::getState() const
{
	return (*mHistory.rbegin())->copy();
}

const State * Target::getStateConst() const
{
	return (*mHistory.rbegin());
}


list<State *> Target::getHistory() const
{
	return mHistory;
}
void Target::setHistory(const list<State *>& history)
{
	mHistory=history;
}

double Target::intersection(const Target& i, const vector<double>& sigmas) const
{
        // ****
	return this->getState()->getGaussian(i.getState(),sigmas);
}
Target Target::merge(const Target& i) const
{
	Target merged;
	if (this->mID<i.mID)
		merged=*this;
	else
		merged=i;

	return merged;
}

std::ostream& operator<< (std::ostream& out, Target& target)
{
/*	State * state=target.getState();
	out<<state<<target.getID()<<" "<<target.mValidity<<endl;
	return out; */
}
