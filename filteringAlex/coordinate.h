/*
 * coordinate.h
 *
 *  Created on: Jun 27, 2014
 *      Author: egor
 */

#ifndef COORDINATE_H_
#define COORDINATE_H_

#include <random>
#include <math.h>
#include <iostream>

using namespace std;

class Coordinate
{

public:
	Coordinate(double center, double dimension, double direction) {}
	Coordinate(const Coordinate* i) {}
	virtual ~Coordinate() {};
	virtual Coordinate* operator+(const Coordinate* i) const = 0;
	virtual Coordinate* operator-(const Coordinate* i) const = 0;
	virtual Coordinate* operator/=(double d) = 0;
	virtual Coordinate* operator-() = 0;
	virtual Coordinate* copy() = 0;
	virtual double getGaussian(const vector<double>& sigmas,bool sigmaCorr, bool dimCor, bool dirCorr) const = 0;
	virtual double getSquare() const = 0;
	virtual void shift(double shift) = 0;
	virtual void setDynamics(const Coordinate* begin, const Coordinate* end) = 0;
	virtual void keepRatio(const Coordinate* i, double ratio) = 0;
	virtual void intersection(const Coordinate* i, const Coordinate* j, double& one, double& two) const = 0;
	virtual void randomFluctuate(default_random_engine &generator,const vector<double>& sigmas) = 0;
	virtual void randomFluctuate(default_random_engine &generator,const vector<double>& sigmas, double dir) = 0;
	virtual void merge(const Coordinate* i, const Coordinate* j) = 0;
	virtual void mean(const Coordinate* i, const Coordinate* j) = 0;
	virtual void predict() = 0;
};
class SpaceCoordinate : virtual public Coordinate
{

public:
	double mCenter;
	double mDimension;
	double mDirection;
public:
	SpaceCoordinate(double center, double dimension, double direction);
	SpaceCoordinate(const Coordinate* i);
	~SpaceCoordinate() {};
	SpaceCoordinate* operator+(const Coordinate* i) const;
	SpaceCoordinate* operator-(const Coordinate* i) const;
	SpaceCoordinate* operator/=(double d);
	SpaceCoordinate* operator-();
	SpaceCoordinate* copy();
	double getGaussian(const vector<double>& sigmas,bool sigmaCorr, bool dimCorr, bool dirCorr) const;
	double getSquare() const;
	void shift(double shift);
	void setDynamics(const Coordinate* begin, const Coordinate* end);
	void keepRatio(const Coordinate* i, double ratio);
	void intersection(const Coordinate* i, const Coordinate* j, double& one, double& two) const;
	void randomFluctuate(default_random_engine &generator,const vector<double>& sigmas);
	void randomFluctuate(default_random_engine &generator,const vector<double>& sigmas, double dir);
	void merge(const Coordinate* i, const Coordinate* j);
	void mean(const Coordinate* i, const Coordinate* j);
	void predict();
	friend std::ostream& operator<< (std::ostream& out, const SpaceCoordinate* coord);
};


#endif /* COORDINATE_H_ */
