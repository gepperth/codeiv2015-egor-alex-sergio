#include "coordinate.h"
using namespace std;

SpaceCoordinate::SpaceCoordinate(double center, double dimension, double direction) : Coordinate(center, dimension, direction)
{
	mCenter=center;
	mDimension=dimension;
	mDirection=direction;
}
SpaceCoordinate::SpaceCoordinate(const Coordinate* i) : Coordinate(i)
{
	const SpaceCoordinate *si=dynamic_cast<const SpaceCoordinate*>(i);
	mCenter=si->mCenter;
	mDimension=si->mDimension;
	mDirection=si->mDirection;
}
SpaceCoordinate::~SpaceCoordinate();
SpaceCoordinate* SpaceCoordinate::operator+(const Coordinate* i) const
{
	const SpaceCoordinate* s=dynamic_cast<const SpaceCoordinate*>(i);
	SpaceCoordinate* coordinate=new SpaceCoordinate(this->mCenter+s->mCenter,
													this->mDimension+s->mDimension,
													this->mDirection+s->mDirection);
	return coordinate;
}
SpaceCoordinate* SpaceCoordinate::operator-(const Coordinate* i) const
{
	const SpaceCoordinate* s=dynamic_cast<const SpaceCoordinate*>(i);
	SpaceCoordinate* coordinate=new SpaceCoordinate(this->mCenter-s->mCenter,
													this->mDimension-s->mDimension,
													this->mDirection-s->mDirection);
	return coordinate;
}
SpaceCoordinate* SpaceCoordinate::operator/=(double d)
{
	this->mCenter/=d;
	this->mDimension/=d;
	this->mDirection/=d;
	return new SpaceCoordinate(*this);
}
SpaceCoordinate* SpaceCoordinate::operator-()
{
	SpaceCoordinate* coordinate=new SpaceCoordinate(-this->mCenter,
													-this->mDimension,
													-this->mDirection);
	return coordinate;
}
SpaceCoordinate* SpaceCoordinate::copy()
{
	return new SpaceCoordinate(this);
}
double SpaceCoordinate::getGaussian(const vector<double>& sigmas,bool sigmaCorr, bool dimCorr, bool dirCorr) const
{
	double mult=1.0;
	/*if (sigmaCorr&&mDimension!=0)
	{
		mult=mDimension;
	}*/
	double ex=mCenter*mCenter/(2.0*sigmas[0]*sigmas[0]*mult*mult);
	if (dimCorr)
		ex+=mDimension*mDimension/(2.0*sigmas[1]*sigmas[1]*mult*mult);
	if (dirCorr)
		ex+=mDirection*mDirection/(2.0*sigmas[2]*sigmas[2]*mult*mult);

        if (dimCorr && dirCorr)
  	  return exp(-0.33333*ex);
        else
        if (!dimCorr && dirCorr)
  	  return exp(-0.5*ex);
        else
        if (dimCorr && !dirCorr)
  	  return exp(-0.5*ex);
        else
  	  return exp(-1.*ex);


}
double SpaceCoordinate::getSquare() const
{
	return mCenter*mCenter;
}
void SpaceCoordinate::shift(double shift)
{
	mCenter+=shift;
}
void SpaceCoordinate::setDynamics(const Coordinate* begin, const Coordinate* end)
{
	const SpaceCoordinate *b=dynamic_cast<const SpaceCoordinate*>(begin);
	const SpaceCoordinate *e=dynamic_cast<const SpaceCoordinate*>(end);
	mDirection=e->mCenter-b->mCenter;
}
void SpaceCoordinate::keepRatio(const Coordinate* i, double ratio)
{
	const SpaceCoordinate *s=dynamic_cast<const SpaceCoordinate*>(i);
	mDimension=s->mDimension*ratio;
}
void SpaceCoordinate::intersection(const Coordinate* i, const Coordinate* j, double& one, double& two) const
{
	const SpaceCoordinate *si=dynamic_cast<const SpaceCoordinate*>(i);
	const SpaceCoordinate *sj=dynamic_cast<const SpaceCoordinate*>(j);
	double sOne=2*si->mDimension;
	double sTwo=2*sj->mDimension;
	double s1=std::max(si->mCenter-si->mDimension,sj->mCenter-sj->mDimension);
	double s2=std::min(si->mCenter+si->mDimension,sj->mCenter+sj->mDimension);
	double sCommon=(s2-s1);
	if (sCommon<0||sOne==0||sTwo==0)
	{
		one=0;
		two=0;
	}
	else
	{
		one=sCommon/sOne;
		two=sCommon/sTwo;
	}
}
void SpaceCoordinate::randomFluctuate(default_random_engine &generator,const vector<double>& sigmas)
{

	normal_distribution<double> distributionCoord(0.0,sigmas[0]);//*mDimension);
	normal_distribution<double> distributionDim(0.0,sigmas[1]);//0.5
	normal_distribution<double> distributionSpeed(0.0,sigmas[2]);//0.1

	mCenter+=distributionCoord(generator);
	mDimension+=distributionDim(generator);
	mDirection+=distributionSpeed(generator);
        //printf("part is %f %f %f\n", mCenter, mDimension, mDirection) ;
}
void SpaceCoordinate::randomFluctuate(default_random_engine &generator,const vector<double>& sigmas, double dir)
{
	mDirection=dir;
	randomFluctuate(generator,sigmas);
}
void SpaceCoordinate::merge(const Coordinate* i, const Coordinate* j)
{
	const SpaceCoordinate *si=dynamic_cast<const SpaceCoordinate*>(i);
	const SpaceCoordinate *sj=dynamic_cast<const SpaceCoordinate*>(j);
	double s1=std::min(si->mCenter-si->mDimension,sj->mCenter-sj->mDimension);
	double s2=std::max(si->mCenter+si->mDimension,sj->mCenter+sj->mDimension);
	mCenter=(s2+s1)/2.0;
	mDimension=(s2-s1)/2.0;
}
void SpaceCoordinate::mean(const Coordinate* i, const Coordinate* j)
{
	const SpaceCoordinate *si=dynamic_cast<const SpaceCoordinate*>(i);
	const SpaceCoordinate *sj=dynamic_cast<const SpaceCoordinate*>(j);
	mCenter=(si->mCenter+sj->mCenter)/2.0;
	mDimension=(si->mDimension+sj->mDimension)/2.0;
}
void SpaceCoordinate::predict()
{
	mCenter+=mDirection;
}
ostream& operator<< (std::ostream& out, const SpaceCoordinate* coord)
{
	out<<coord->mCenter-coord->mDimension<<" "<<coord->mCenter+coord->mDimension<<" ";
	return out;
}




