
#include "newstate.h"

int area(visualRect r)
{
  return r.width * r.height ;
}


int intersect(visualRect & r1, visualRect & r2)
{
  visualRect intersection;

// find overlapping region
  intersection.x = (r1.x < r2.x) ? r2.x : r1.x;
  intersection.y = (r1.y < r2.y) ? r2.y : r1.y;
  intersection.width = (r1.x + r1.width < r2.x + r2.width) ? r1.x + r1.width : r2.x + r2.width;
  intersection.width -= intersection.x;
  intersection.height = (r1.y + r1.height < r2.y + r2.height) ? r1.y + r1.height : r2.y + r2.height;
  intersection.height -= intersection.y;
  
  if ((intersection.width <= 0) || (intersection.height <= 0)) 
    return 0 ;
  else
    return area (intersection) ;
}

float overlap(visualRect r1, visualRect r2)
{
  int inter = intersect(r1,r2) ;
  int join = area(r1) + area(r2) - inter ;
  
  return float (inter)/ float (join) ;
}


visualRect visualState2Rect(const visualState * s)
{
  visualRect r ;
  r.x = (s->xc-s->xs) ;
  r.y = (s->yc-s->ys) ;
  r.width = (2.*s->xs) ;
  r.height = (2.*s->ys) ;
  return r ;
}



float overlapVisual(const visualState * s1, const visualState * s2)
{
  return overlap(visualState2Rect(s1),visualState2Rect(s2)) ;
}



visualState::visualState()
{
  this->compareSize = this->compareSpeed=false ;
  this->targetRatio = 0.5 ;
  xc=yc=xs=ys=xv=yv=0 ;
  
}


visualState::visualState(double xc, double xs, double xv, double yc, double ys, double yv)
{
  this->compareSize = this->compareSpeed=false ;
  this->targetRatio = 0.5 ;
  this->xc = xc ;
  this->yc = yc ;
  this->xs = xs ;
  this->ys = ys ;
  this->xv = xv ;
  this->yv = yv ;
}


visualState::visualState( const State * i)
{
  this->compareSize = this->compareSpeed=false ;
  this->targetRatio = 0.5 ;
  const visualState * j = dynamic_cast<const visualState *> (i) ;
  this->xc = j->xc ;
  this->yc = j->yc ;
  this->xs = j->xs ;
  this->ys = j->ys ;
  this->xv = j->xv ;
  this->yv = j->yv ;
}

visualPlusLaserState::visualPlusLaserState( const State * i)
{
  this->compareSize = this->compareSpeed=false ;
  this->targetRatio = 0.5 ;
  const visualPlusLaserState * j = dynamic_cast<const visualPlusLaserState *> (i) ;
  this->xc = j->xc ;
  this->yc = j->yc ;
  this->xs = j->xs ;
  this->ys = j->ys ;
  this->xv = j->xv ;
  this->yv = j->yv ;
  this->lxc = j->lxc ;
  this->lyc = j->lyc ;
  this->lzc = j->lzc ;
  this->lxv = j->lxv ;
  this->lyv = j->lyv ;
  this->lzv = j->lzv ;

}

void visualState::setDynamics( const State * begin,  const State * end)
{
  const visualState * _begin = dynamic_cast<const visualState *> (begin) ;
  const visualState * _end = dynamic_cast<const visualState *> (end) ;
  this->xv = _end->xc - _begin->xc ;
  this->yv = _end->yc - _begin->yc ;
  
}


void visualPlusLaserState::setDynamics( const State * begin,  const State * end)
{
  const visualPlusLaserState * _begin = dynamic_cast<const visualPlusLaserState *> (begin) ;
  const visualPlusLaserState * _end = dynamic_cast<const visualPlusLaserState *> (end) ;
  this->xv = _end->xc - _begin->xc ;
  this->yv = _end->yc - _begin->yc ;
  this->lxv = _end->lxc - _begin->lxc ;
  this->lyv = _end->lyc - _begin->lyc ;
  this->lzv = _end->lzc - _begin->lzc ;
}



// set from raw data arranged as xc xs xv yc ys yv
void visualState::set(const vector<double> & data)
{
  this->xc = data[0] ;
  this->xs = data[1] ;
  this->xv = data[2] ;
  this->yc = data[3] ;
  this->ys = data[4] ;
  this->yv = data[5] ;
  
}

// set from raw data arranged as xc xs xv yc ys yv lxc lxv lyc lyv
void visualPlusLaserState::set(const vector<double> & data)
{
  visualState::set(data) ;
  this->lxc = data[6] ;
  this->lxv = data[7] ;
  this->lzv = data[8] ;
  this->lyc = data[9] ;
  this->lyv = data[10] ;  
  this->lzv = data[11] ;  
}


void visualState::get(vector <double> & data) const
{
  data.resize(6) ;
  data[0] = this->xc ;
  data[1] = this->xs ;
  data[2] = this->xv ;
  data[3] = this->yc ;
  data[4] = this->ys ;
  data[5] = this->yv ;
}

void visualPlusLaserState::get(vector <double> & data) const
{
  visualState::get(data) ;
  data.resize(9) ;
  data[6] = this->lxc ;
  data[7] = this->lxv ;
  data[8] = this->lzv ;
  data[9] = this->lyc ;
  data[10] = this->lyv ;
  data[11] = this->lzv ;
}



void visualState::set( const State * i)
{
  const visualState * j = dynamic_cast<const visualState *> (i) ;
  this->xc = j->xc ;
  this->yc = j->yc ;
  this->xs = j->xs ;
  this->ys = j->ys ;
  this->xv = j->xv ;
  this->yv = j->yv ;
  
}


void visualPlusLaserState::set( const State * i)
{
  const visualPlusLaserState * j = dynamic_cast<const visualPlusLaserState *> (i) ;
  this->xc = j->xc ;
  this->yc = j->yc ;
  this->xs = j->xs ;
  this->ys = j->ys ;
  this->xv = j->xv ;
  this->yv = j->yv ;
  this->lxv = j->lxv ;
  this->lyv = j->lyv ;
  this->lzv = j->lzv ;
  this->lxc = j->lxc ;
  this->lyc = j->lyc ;
  this->lzc = j->lzc ;
  
}



void visualState::add( const State * i) 
{
  const visualState * j = dynamic_cast<const visualState *> (i) ;
  this->xc += j->xc ;
  this->yc += j->yc ;
  this->xs += j->xs ;
  this->ys += j->ys ;
  this->xv += j->xv ;
  this->yv += j->yv ;
  
}


void visualPlusLaserState::add( const State * i) 
{
  visualState::add(i) ;
  const visualPlusLaserState * j = dynamic_cast<const visualPlusLaserState *> (i) ;
  this->lxc += j->lxc ;
  this->lyc += j->lyc ;
  this->lzc += j->lzc ;
  this->lxv += j->lxv ;
  this->lyv += j->lyv ;
  this->lzv += j->lzv ;
  
}



void visualState::sub( const State * i) 
{
  const visualState * j = dynamic_cast<const visualState *> (i) ;
  this->xc -= j->xc ;
  this->yc -= j->yc ;
  this->xs -= j->xs ;
  this->ys -= j->ys ;
  this->xv -= j->xv ;
  this->yv -= j->yv ;
  
}
void visualPlusLaserState::sub( const State * i) 
{
  visualState::sub(i) ;
  const visualPlusLaserState * j = dynamic_cast<const visualPlusLaserState *> (i) ;
  this->lxc -= j->lxc ;
  this->lyc -= j->lyc ;
  this->lzc -= j->lzc ;  
  this->lxv -= j->lxv ;
  this->lyv -= j->lyv ;
  this->lzv -= j->lyv ;
  
}

void visualState::divBy(double d) 
{

  this->xc /=d ;
  this->yc /=d ;
  this->xs /=d ;
  this->ys /=d ;
  this->xv /=d ;
  this->yv /=d ;
  
}


void visualPlusLaserState::divBy(double d) 
{
  visualState::divBy(d) ;
  this->lxc /=d ;
  this->lyc /=d ;
  this->lzc /=d ;
  this->lxv /=d ;
  this->lyv /=d ;  
  this->lzv /=d ;  
}


// sorry, inefficient!!
double visualState::at (int i) const
{
  vector <double> data ;
  this->get(data) ;
  return data[i]  ;
}


void visualState::predict()
{
  this->xc += this->xv ;
  this->yc += this->yv ;
}


void visualPlusLaserState::predict()
{
  visualState::predict();
  this->lxc += this->lxv ;
  this->lyc += this->lyv ;  
  this->lzc += this->lzv ;  
}


double visualState::intersectionRate(State* i) const
{
/*
	double sOne=1.0, sTwo=1.0,one,two;
	for (unsigned int j=0; j<mCoordinates.size(); j++)
	{
		if (used[j])
		{
			mCoordinates[j]->intersection(i.mCoordinates[j],this->mCoordinates[j],one,two);
			sOne*=one;
			sTwo*=two;
		}
	}

	return max(sOne,sTwo); */
  return 0.0 ;
}

void visualState::merge( State * i)
{
  visualState * j = dynamic_cast<visualState *> (i) ;
  double x1 = std::min(xc-xs, j->xc-j->xs) ;
  double y1 = std::min(yc-ys, j->yc-j->ys) ;
  double x2 = std::max(xc+xs, j->xc+j->xs) ;
  double y2 = std::min(yc-ys, j->yc-j->ys) ;
  
  this->xc = (x1+x2)/2. ;
  this->yc = (y1+y2)/2. ;

  this->xs = (x2-x1)/2. ;
  this->ys = (y2-y1)/2. ;

  this->xv = (xv+j->xv)/2. ;
  this->yv = (yv+j->yv)/2. ;

}



double visualState::getGaussian( State * i,const vector<double>& sigmas ) const
{
  visualState * j = dynamic_cast<visualState *> (i) ;
  visualState * diff = new visualState (this) ;
  diff->sub(j) ;
  //cout << "visgaussian: sigmas=" << sigmas[0] << ", diff=" ; 
  //this->printState() ;
  //j->printState() ;
  //diff->printState() ;
  double scaleFactor= std::min(this->xs/24., j->xs/24. );
  double exponent = ((diff->xc)*(diff->xc)+(diff->yc)*(diff->yc))/(scaleFactor*scaleFactor*2.*sigmas[0]*sigmas[0]) ;
  
  double scaleRatio = j->xs / this->xs ;
  if (scaleRatio < 1.) scaleRatio = 1./scaleRatio ;
  scaleRatio = log(scaleRatio)/log(1.4142) ;
  exponent += scaleRatio/(2.*sigmas[1]*sigmas[1]) ; // + (expression that is 0 when the 2 scales are identical, 1 if they differ by 1.4142)
  // NEW
  //exponent = (1.-overlapVisual(this,j))/(2.*sigmas[0]*sigmas[0])  ; 
  // 1-overlap to that max overlap has the highest value in the Gaussian
  
  //if (this->compareSize==true)
   /* 
   // eventually tu use!!
   double sc1 = log(this->xs/48.)/log(1.4142) ;
    double sc2 = log(j->xs/48.)/log(1.4142) ;
    exponent += 0.5*(sc1-sc2)*(sc1-sc2)/(sigmas[1]*sigmas[1]) ; */
  //if (this->compareSpeed==true)
    exponent += 0.0*((diff->xv)*(diff->xv)+(diff->yv)*(diff->yv))/(2.*sigmas[2]*sigmas[2]) ;
  double distance = exp(-exponent) ;
  delete diff ;
  return distance*100.;
}


double visualPlusLaserState::getGaussian( State * i,const vector<double>& sigmas ) const
{
  //return visualState::getGaussian(i, sigmas) ;
  visualPlusLaserState * j = dynamic_cast<visualPlusLaserState *> (i) ;
  visualPlusLaserState * diff = new visualPlusLaserState (this) ;
  diff->sub(j) ;
  
  double exponent = ((diff->lxc)*(diff->lxc)) /(sigmas[3]*sigmas[3]*2.)    +
                    ((diff->lyc)*(diff->lyc))/(sigmas[4]*sigmas[4]*2.) +
                    ((diff->lzc)*(diff->lzc))/(sigmas[5]*sigmas[5]*2.) ;
  //if (this->compareSpeed==true)
  //  exponent += ((diff->lxv)*(diff->lxv)+(diff->lyv)*(diff->lyv)+(diff->lzv)*(diff->lzv))/(sigmas[6]*sigmas[6]*2.); 
  double distance = exp(-exponent) ;
  return distance*100.;
}



void visualState::applyRandomFluctuation(const vector<double> & sigmas, default_random_engine &generator)
										
{
  normal_distribution<double> distributionCoord(0.0,sigmas[0]);//*mDimension);
  normal_distribution<double> distributionDim(0.0,sigmas[1]);//0.5
  normal_distribution<double> distributionSpeed(0.0,sigmas[2]);//0.1
  // XXXXX
  double scaleFactor= (2.*this->xs/48.) ;
  if (scaleFactor < 1.) scaleFactor = 1. ;
  
  xc += distributionCoord(generator)*scaleFactor ;
  yc += distributionCoord(generator)*scaleFactor ;
  double fluctDim = distributionDim(generator) ;
  xs *= (1.+fluctDim) ;
  ys *= (1.+fluctDim) ;
  xv += distributionSpeed(generator)*scaleFactor ;
  yv += distributionSpeed(generator)*scaleFactor ;
  
  // maje fluctiation respect target ratio
  xs /= (xs/ys)/(this->targetRatio) ; 
  
}


void visualPlusLaserState::applyRandomFluctuation(const vector<double> & sigmas, default_random_engine &generator)
										
{
  //visualState::applyRandomFluctuation(sigmas, generator) ;
  normal_distribution<double> distributionX(0.0,sigmas[3]);//*mDimension);
  normal_distribution<double> distributionY(0.0,sigmas[4]);//*mDimension);
  normal_distribution<double> distributionZ(0.0,sigmas[5]);//*mDimension);
  normal_distribution<double> distributionSpeed(0.0,sigmas[6]);//0.1
  
  lxc += distributionX(generator) ;
  lyc += distributionY(generator) ;
  lzc += distributionZ(generator) ;
  
  lxv += distributionSpeed(generator) ;
  lyv += distributionSpeed(generator) ;
  lzv += distributionSpeed(generator) ;
  
  
}

 void visualState::setConsiderSpeed() {this->compareSpeed=true;};
 void visualState::setDisregardSpeed () {this->compareSpeed=false;};
 void visualState::setConsiderSize() {this->compareSize=true;}; 
 void visualState::setDisregardSize() {this->compareSize=false;};
 
State * visualState::copy()
 {
   visualState * tmp = new visualState(this) ;
   return tmp ;
 }
 
State * visualPlusLaserState::copy()
 {
   visualPlusLaserState * tmp = new visualPlusLaserState(this) ;
   return tmp ;
 }
 
 void visualPlusLaserState::printState ()
{
  cout << "LASER=[ " << lxc << "/" << lyc << "/"<<lzc << " " << lxv <<"/" << lyv << "/" << lzv << endl << "] VISION="  ;
  visualState::printState() ;
}


 void visualState::printState ()
{
  cout << xc << "/" << yc << " " << xs <<"/" << ys << " " << xv << "/" << yv << endl ;
}


 double visualState::getSimilarityToObs( const State * i, const vector <double> & sigmas)  
{
   return this->getGaussian(i,sigmas) ;
}


 double visualPlusLaserState::getSimilarityToObs( const State * i, const vector <double> & sigmas)  
{
  State * tmp = i->copy() ;
  this->compareSpeed=true ;
  visualPlusLaserState * j = dynamic_cast<visualPlusLaserState *> (tmp) ;
  j->lxv = j->lxc - this->lxc ;
  j->lyv = j->lyc - this->lyc ;
  j->lzv = j->lzc - this->lzc ;
  
  double res =  this->getGaussian(i,sigmas) ;
  this->compareSpeed=false ;
  delete tmp ;
  return res ;
  
}

void visualState::setSpeedsZero()
{
  this->xv = this->yv = 0 ;
}

void visualPlusLaserState::setSpeedsZero()
{
  visualState::setSpeedsZero() ;
  this->lxv = this->lyv = this->lzv = 0 ;
}







 

