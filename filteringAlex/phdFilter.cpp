#include "phdFilter.h"

//#define MFORCE 0.1

PHDFilter::PHDFilter()
{

}
PHDFilter::~PHDFilter()
{
	for (unsigned int i=0; i < mTargets.size(); i++)
		delete mTargets[i];
}

void PHDFilter::initialize(int num_particles,
							const vector<double>& sigmas,
							double pfn,
							int historyLength,
							const string& fieldName)
{
	mSigmas=sigmas;
 targetHistoryLength=historyLength;
        mPfn=pfn;

	mAssociationCoef=10.0;//30.0;//10
	mResampleCoef=0.2;//0.1;//0.01;
	mInfluenceCoef=0.1;//0.05;//0.01;

	mPd=0.1;
	mPb=1.0-3*mPd;

	idNum=0;
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	//unsigned seed=0;
	mGenerator.seed(seed);
	//modelForce=0.1;



	nParticlesPerObject = num_particles;
	mObservations.clear();
	mParticles.clear();

}


double PHDFilter::getTargetProbability(int i)
{
  return this->mTargets[i]->getProbability() ;
}

void PHDFilter::prediction()
{
	for (unsigned int j=0; j < mParticles.size(); j++)
	{
		for (unsigned int i=0; i < mParticles[j].size(); i++)
		{
	        //Motion model
			//mParticles[j][i].setState(mParticles[j][i].getState().predict(mUsed)); // ****
			mParticles[j][i].predict() ;
		}
	}
}

void PHDFilter::association(const vector<Detection>& detections)
{
  State * randomFluctuation; // ****
  mObservations.clear();
  mObservations=detections;
  State * tmp; // ****
  vector<bool> usedTargets(mTargets.size(),false);
  usedObservations.assign(mObservations.size(),false) ;
  vector<vector<double> > distances(mObservations.size(),vector<double> (mTargets.size()));

  // calculate distances
  for (unsigned int j=0; j < mObservations.size(); j++)
    for (unsigned int i=0; i < mTargets.size(); i++)
    {
    	// make copy of state, do not forget to delete..
    	tmp=mTargets[i]->getState();
    	tmp->predict(); // ****
    	// just take distance here not speeds
    	distances[j][i]=tmp->getSimilarityToObs(mObservations[j].getState(),mSigmas); // ****
//    	cout << "assoc " << i << " " << j << " " << tmp->getGaussian(mObservations[j].getState(),mSigmas) << endl;
//    	cout << "target="; tmp->printState() ;
//    	cout << "obs=" ; mObservations[j].getState()->printState () ;
    	delete tmp ;
    }

    bool stillAssociate=true;
    int jMax, iMax;
    double max;
    double threshold=mAssociationCoef ;

    // identify all tracks for which there are observations havinbg a similarity >= threshold
    //threshold=tmp.getGaussianEthalon(mSigmas,mUsed,mAssociationCoef);
        // find maximal distance/similarity between any target and any observation
      	for (unsigned int i=0; i < mTargets.size(); i++)
    	  for (unsigned int j=0; j < mObservations.size(); j++)
          {
             max=-10.;
             if ((distances[j][i]>threshold))
             {
               usedObservations[j]=true;
               usedTargets[i]  = true ;
//               cout<<"successful assoc " << max << " " << mObservations[j].getID()<<" "<<mTargets[i]->getID()<<endl;
             }
          }
        // increase or decrease probability for tracks
      	for (unsigned int i=0; i < mTargets.size(); i++)
        {
          if(usedTargets[i]==true)
          {
            mTargets[i]->setProbability(min(mTargets[i]->getProbability()+mPdeltaPlus,1.0));
            for (unsigned int j=0; j < mParticles[i].size(); j++)
            {
               mParticles[i][j].setWeight(mTargets[i]->getProbability()/nParticlesPerObject);
            }
          }
          else
          {
            mTargets[i]->setProbability(std::max(0.0,mTargets[i]->getProbability()-mPdeltaMinus));
            for (unsigned int j=0; j < mParticles[j].size(); j++)
              mParticles[i][j].setWeight(mTargets[i]->getProbability()/nParticlesPerObject);
          }
        }
    

    // check for unassigned observations
    for (unsigned int j=0;j < mObservations.size(); j++)
    {

    	if (	! usedObservations[j] 	) //&&
//    			! (		distances[j].size()>0 	&&
  //  					*std::max_element(distances[j].begin(),distances[j].end())>threshold)
    //		)
        //create target
    	{
//    	        cout << "create" << endl ;
    		Target* newTarget=new Target(idNum++,targetHistoryLength);
    		
    		// copy!!
    		State * originalState = mObservations[j].getState() ;
    		newTarget->setState(originalState);
    		visualPlusLaserState * p= dynamic_cast<visualPlusLaserState*>(originalState) ;
//    		cout << "target state is " << p->lxc << "/" << p->lyc << endl ;
    		
    		newTarget->setProbability(this->mPdeltaPlus);
    		usedTargets.push_back(true);
    		mTargets.push_back(newTarget);
    		mParticles.push_back(vector<Particle>());
//    		cout << usedTargets.size() << "/" << mTargets.size() << "/" << mParticles.size() << endl ;
                
//    		cout<<mObservations[j].getID()<<" "<<newTarget->getID()<<endl;

    		vector<double> sigmas=mSigmas;
    		
    		for (unsigned int i=0; i<sigmas.size(); i++)
    			sigmas[i] *=mResampleCoef ;

    			

    		for (int i=0; i < nParticlesPerObject; i++)
    		{
    			//randomFluctuation=mObservations[j].getState().getRandomFieldFluctuation(sigmas,mUsed,mGenerator,mDirections,0);
                        Particle sampledParticle;
    			sampledParticle.setState(originalState);
    			sampledParticle.applyRandomFluctuation(sigmas, mGenerator); // ****
    			sampledParticle.setWeight(newTarget->getProbability()/nParticlesPerObject);
    			sampledParticle.setTarget(newTarget);
    			mParticles.back().push_back(sampledParticle);    			
    		}
//		cout << "created target" << endl ;
		delete originalState ;
    	}
    }
        // pruning targets
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		if (mTargets[i]->getProbability()<mPd && !usedTargets[i])
		{       visualPlusLaserState * p= dynamic_cast<visualPlusLaserState*>(mTargets[i]->getState()) ;
//		        cout << "used" << (usedTargets[i]==true) << ", prob=" << mTargets[i]->getProbability() << endl ;
//			cout << "pruning target " << p->lxc << "/" << p->lyc << endl ;
			delete mTargets[i];
			mTargets.erase (mTargets.begin()+i);
			mParticles.erase (mParticles.begin()+i);
			i--;
		}
	}
}

void PHDFilter::observation()
{

	double sum;
	vector<double> oneComponent,newWeights;
	newWeights.assign(mParticles.size()*nParticlesPerObject,0.0);

	vector<double> sigmas=mSigmas;
	for (unsigned int i=0; i<sigmas.size(); i++)
		sigmas[i]*=mInfluenceCoef;

        // for each detection..
	for (unsigned int j=0; j < mObservations.size(); j++)
        if(usedObservations[j]==true)
	{
		State  * oneState=mObservations[j].getState(); // ****
                //..collect contribs from each detection..
		oneComponent.assign(mParticles.size()*nParticlesPerObject,0.0);
		sum=0.0;
		unsigned int ik=0;


		for (unsigned int i=0; i < mParticles.size(); i++)
		{
                        // set speed vector of detection such that it corresponds to difference vetor between part and det
			oneState->setDynamics(mTargets[i]->getState(),oneState);
			for (unsigned int k=0; k < mParticles[i].size(); k++)
			{
                                // each part weight multiplied by Gaussian similarity state-part
                                // !!!! MAKE sure velocity is compared here!!
				oneComponent[ik] = mParticles[i][k].getWeight()*oneState->getGaussian(mParticles[i][k].getState(),
				  sigmas);
				sum += oneComponent[ik];
				ik++;
			}
		}
		delete oneState ;

	        for (ik=0; ik < oneComponent.size(); ik++)
    	        {
			if (sum!=0)
			{
				oneComponent[ik] = oneComponent[ik]/sum;
				newWeights[ik] = newWeights[ik]+oneComponent[ik];
			}
	        }
	}
	int ik=0;
	for (unsigned int i=0; i < mParticles.size(); i++)
	{
		for (unsigned int k=0; k < mParticles[i].size(); k++)
		{
			newWeights[ik] = newWeights[ik]+mPfn*mTargets[i]->getProbability()/nParticlesPerObject;
			mParticles[i][k].setWeight(newWeights[ik]);
			ik++;
		}
	}
/* // leave out vector field part!!
	for (unsigned int i=0; i < mTargets.size(); i++)
	{
		mDirections=field.get(mTargets[i]->getState(),modelForce);
		setValidity(i, mDirections.size());
	} */
}



void PHDFilter::getCumulatedProbability(int numTarget, vector<double>& cumulatedProbabilities, double& sum)
{
	sum=0.0;
	cumulatedProbabilities.clear();
	for (unsigned int i=0; i < mParticles[numTarget].size(); i++)
	{
		sum+=mParticles[numTarget][i].getWeight();
		cumulatedProbabilities.push_back(sum);
	}
}


void PHDFilter::oneResample(const vector<Particle>& oldParticles, int numTarget, const vector<double>& cumulatedProbabilities, double sum)
{
    uniform_real_distribution<double> uniformDistribution(0.0,sum);
    State * randomFluctuation;
    vector<double> sigmas=mSigmas;

    float prob = mTargets[numTarget]->getProbability() ;
    if (prob < 0.01) prob=0.01 ;
    double multiplicator=1.0/mTargets[numTarget]->getProbability();//mPb/max(0.001,sum);
    for (unsigned int i=0; i<sigmas.size(); i++)
    	sigmas[i]=sigmas[i]*mResampleCoef*multiplicator;
	for (int i=0; i < nParticlesPerObject; i++)
	{
		int j=0;
	    double randomNumber=uniformDistribution(mGenerator);
	    while ( randomNumber>cumulatedProbabilities[j])
	    	j++;

	    //randomFluctuation=oldParticles[j].getState().getRandomFieldFluctuation (sigmas,mUsed,mGenerator,mDirections,0);
	    //mParticles[numTarget][i].setState(randomFluctuation);
	    mParticles[numTarget][i].setState(oldParticles[j].getState()) ;
	    mParticles[numTarget][i].applyRandomFluctuation(sigmas, mGenerator) ; // ****	    
	    //mParticles[numTarget][i].setTarget(oldParticles[j].getTarget());
	}
}
int PHDFilter::randomFieldFluctuation(int i)
{
}

void PHDFilter::resample()
{
	vector<double> cumulatedProbabilities;
	double sum;
	double totalSum=0.0;

	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		getCumulatedProbability(i,cumulatedProbabilities,sum);
		//mTargets[i]->setProbability(sum);

		vector<Particle> oldParticles(mParticles[i]);
		oneResample(oldParticles,i,cumulatedProbabilities,sum);
		totalSum+=sum;
	}
/*
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		mTargets[i]->setProbability(mTargets[i]->getProbability()/totalSum);
	}
*/
}

void PHDFilter::merge()
{
	vector<double> sigmas=mSigmas;
	for (unsigned int i=0; i<sigmas.size(); i++)
		sigmas[i]*=mInfluenceCoef;
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		for (unsigned int j=i+1; j<mTargets.size(); j++)
		{
			double intersection=mTargets[i]->getStateConst()->getGaussian(mTargets[j]->getStateConst(), mSigmas) ;
			//max=std::max(max, intersection);
			cout << "Tatrget " << i << " " << j << "sim" << intersection << endl ;
			if ((intersection>mAssociationCoef*2) /*&& (mTargets[i]->getProbability() > mTargets[j]->getProbability())*/)
			{
				//cout<<"DELETE"<<endl;
//				*mTargets[i]=mTargets[i]->merge(*mTargets[j],mUsed);
				delete mTargets[j];
				mTargets.erase(mTargets.begin()+j);
				mParticles.erase(mParticles.begin()+j);
				j--;
			}
		}
	}
	
}

void PHDFilter::correction()
{
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		State * meanState; // ****
		for (unsigned int j=0; j < mParticles[i].size(); j++)
		{
		        if(j==0)
		          meanState = mParticles[i][j].getState();
		        else
  			  meanState->add(mParticles[i][j].getState());
		}
		//cout << "correction: div by " << double(mParticles[i].size()) << endl ;
		meanState->divBy(double(mParticles[i].size()));
		mTargets[i]->setState(meanState);
		//mTargets[i]->updateSpeed() ;
		cout << "tar state after correction is "; 
		meanState->printState() ;
	}

}





void PHDFilter::evaluate(const vector<Detection>& detections, vector<vector<Particle> >& weightedParticles)
{

	prediction();
	association(detections);
	observation();
	weightedParticles=mParticles;
	resample();
	merge();
	correction();	
}

int PHDFilter::getTargetsNumber()
{
	return mParticles.size();
}
int PHDFilter::getParticlesNumberInTarget(int numTarget)
{
	return mParticles[numTarget].size();
}
Particle PHDFilter::getParticle(int numTarget, int numParticle)
{
	return mParticles[numTarget][numParticle];
}
vector<vector<Particle> > PHDFilter::getParticles()
{
	return mParticles;
}


void PHDFilter::setValidity(int i, int numDirections)
{
	double totalSum=0;
	double oneDirectionSum;
	double maxOneDirectionSum=0;
	for (int j=0; j<nParticlesPerObject; j++)
	{
		totalSum+=mParticles[i][j].getWeight();
	}
	for (int k=0; k<numDirections; k++)
	{
		oneDirectionSum=0;
		for (int j=nParticlesPerObject*(1.0-modelForce*(1-k/numDirections)); j<nParticlesPerObject*(1.0-modelForce*(1-(k+1)/numDirections)); j++)
		{
			oneDirectionSum+=mParticles[i][j].getWeight();
		}
		maxOneDirectionSum=max(maxOneDirectionSum,oneDirectionSum);
	}
	//cout<<"CANDIDATE "<<maxOneDirectionSum*numDirections/(totalSum*modelForce)<<endl;
	if (modelForce!=0&&totalSum!=0)
		mTargets[i]->setValidity(maxOneDirectionSum*numDirections/(totalSum*modelForce));
}

void PHDFilter::printParticles(ostream& out)
{
/*	for (unsigned int i=0; i<mParticles.size(); i++)
	{
		for (unsigned int j=0; j<mParticles[i].size(); j++)
		{
			out << mParticles[i][j]<<": ";
		}
	}
	if (mParticles.size()!=0)
		out << endl; */
}
