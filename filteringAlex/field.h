/*
 * field.h
 *
 *  Created on: Jun 26, 2014
 *      Author: egor
 */

#ifndef FIELD_H_
#define FIELD_H_

#include <fstream>
#include "state.h"
#include "detection.h"
#include "target.h"

class VectorField
{
	int fW;
	int fH;
	vector<vector<Direction> > mField;
public:
	VectorField() {};
	~VectorField() {};
	void load(string filename);
	vector<Direction> get(const State& state,double &modelForce);
};


#endif /* FIELD_H_ */
