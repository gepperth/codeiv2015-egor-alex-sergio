#ifndef TARGET_H
#define TARGET_H


#include <list>
#include "newstate.h"

using namespace std;

class Target
{
private:
	list<State *> mHistory;
	int mHistoryLength;
	int mID;
	double mProbability;
	double mValidity;
	double mModelForce;
 public:

    Target ();
    Target(int idNumber, int historyLength);
    ~Target();
    Target operator=(const Target i);
    void setState(State * state);
    State * getState() const;
    const State * getStateConst() const;
    void setValidity(double validity);
    double getValidity() const;
    void updateSpeed() ;
    int getID() {return mID ;}

    list<State *> getHistory() const;
    void setHistory(const list<State *>& history);

	double intersection(const Target& i, const vector<double>& sigmas) const;
	Target merge(const Target& i) const;
    void setID(int id);
    int getID() const;
    void setProbability(double probability);
    double getProbability() const;

    friend std::ostream& operator<< (std::ostream& out, Target& target);
};


#endif /* TARGET_H_ */
