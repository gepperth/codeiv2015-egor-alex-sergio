#include "matrix.h"

/*
Matrix::Matrix()
{

}
*/
Matrix::Matrix(int nrow, int ncol)
{
	M.assign(nrow*ncol,0);
	Nrow=nrow;
	Ncol=ncol;
}
Matrix::Matrix(vector<double> m, int nrow, int ncol)
{
	M=m;
	Nrow=nrow;
	Ncol=ncol;
}
Matrix::Matrix(const double m[], int nrow, int ncol)
{
	vector<double> mtmp (m, m + nrow*ncol);
	M=mtmp;
	Nrow=nrow;
	Ncol=ncol;
}
Matrix::~Matrix()
{

}
void Matrix::set(vector<double> m, int nrow, int ncol)
{
	M=m;
	Nrow=nrow;
	Ncol=ncol;
}
Vector Matrix::operator* (Vector v)
{

	if (this->Ncol!=(int)v.size())
		return Vector(vector<double>());
	Vector result(vector<double>(this->Nrow,0.0));
	for (int i=0; i<Nrow; i++)
	{
		double sum=0;
		for (int j=0; j<Ncol; j++)
		{
			sum+=v[j]*this->M[i*Ncol+j];
		}
		result[i]=sum;
	}
	return result;
}
Matrix Matrix::operator* (Matrix matrix)
{
	if (this->Ncol!=matrix.Nrow)
		return Matrix(0,0);
	Matrix result(this->Nrow,matrix.Ncol);
	for (int i=0; i<this->Nrow; i++)
	{
		for (int j=0; j<matrix.Ncol; j++)
		{
			double sum=0;
			for (int k=0; k<this->Ncol; k++)
			{
				sum+=this->M[i*this->Ncol+k]*matrix.M[k*matrix.Ncol+j];
			}
			result.M[i*this->Ncol+j]=sum;
		}
	}
	return result;
}
Matrix Matrix::operator+ (Matrix matrix)
{
	if (this->Ncol!=matrix.Ncol||this->Nrow!=matrix.Nrow)
		return Matrix(0,0);
	Matrix result(this->Nrow,this->Ncol);
	for (int i=0; i<Nrow; i++)
	{
		for (int j=0; j<Ncol; j++)
		{
			result.M[i*Ncol+j]=this->M[i*Ncol+j]+matrix.M[i*Ncol+j];
		}
	}
	return result;
}
Matrix Matrix::operator- (Matrix matrix)
{
	if (this->Ncol!=matrix.Ncol||this->Nrow!=matrix.Nrow)
		return Matrix(0,0);
	Matrix result(this->Nrow,this->Ncol);
	for (int i=0; i<Nrow; i++)
	{
		for (int j=0; j<Ncol; j++)
		{
			result.M[i*Ncol+j]=this->M[i*Ncol+j]-matrix.M[i*Ncol+j];
		}
	}
	return result;
}
Matrix Matrix::transpose ()
{
	Matrix result(Ncol,Nrow);
	for (int i=0; i<Nrow; i++)
	{
		for (int j=0; j<Ncol; j++)
		{
			result.M[i*Ncol+j]=this->M[j*Nrow+i];
		}
	}
	return result;
}
Matrix Matrix::invert ()
{
	if (Ncol!=Nrow)
		return Matrix(0,0);
	Matrix result(M,Nrow,Ncol);
	int N=Ncol;
	double det=this->determinant();
	for (int i=0; i<N; i++)
	{
		for (int j=0; j<N; j++)
		{
			result.M[i*N+j]=pow(-1,i+j)*this->minorM(i,j).determinant()/det;
		}
	}
	result=result.transpose();

	return result;
}
double Matrix::determinant()
{
	if (Ncol!=Nrow)
		return 0;
	if (Ncol==1) return M[0];
	if (Ncol==2) return this->M[0]*this->M[3]-this->M[1]*this->M[2];
	double det=0;
	for (int i=0; i<Ncol; i++)
	{
		det+=M[i]*pow(-1,i)*minorM(0,i).determinant();
	}

	return det;
}
Matrix Matrix::minorM(int iM, int jM)
{
	if (Ncol!=Nrow)
		return Matrix(0,0);
	int N=Ncol;
	Matrix result(N-1,N-1);
	int icorr=0,jcorr=0;
	for (int i=0; i<N; i++)
	{
		jcorr=0;
		if (i>iM) icorr=1;
		if (i!=iM)
		for (int j=0; j<N; j++)
		{

			if (j>jM)
				jcorr=1;
			if (j!=jM)
				result.M[(i-icorr)*(N-1)+(j-jcorr)]=M[i*N+j];
		}
	}
	return result;
}
void Matrix::show()
{
	for (int i=0; i<Nrow; i++)
	{
		for (int j=0; j<Ncol; j++)
		{
			cout<<M[i*Ncol+j]<<" ";
		}
		cout<<endl;
	}
}
