#include "particle.h"

Particle::Particle()
{
	mWeight=0.0;
	mState=0;
}
Particle::~Particle()
{
  /*
  cout << "particle destructor" << endl ;
 
  if(mState!=0)
    delete mState ;
  cout << "end particle destructor" << endl ;
  */
  

}
void Particle::setState(State * state)
{
        if(mState != 0)
  	  mState->set(state);
  	else
  	  mState= state->copy() ;
  	
}
State * Particle::getState() const
{
	return mState->copy();
}


const State * Particle::getStateConst() const
{
  return mState ;
}


void Particle::setWeight(double weight)
{
	mWeight=weight;
}
double Particle::getWeight() const
{
	return mWeight;
}
void Particle::setTarget(Target* target)
{
	mTarget=target;
}
Target* Particle::getTarget() const
{
	return mTarget;
}

void Particle::operator=(Particle in)
{
	setState(in.mState);
	this->mWeight=in.mWeight;
	this->mTarget=in.mTarget;
}

bool Particle::operator() (Particle i, Particle j)
{
    return ( i.mWeight > j.mWeight );
}

bool Particle::operator< (Particle i)
{
    return ( this->mWeight < i.mWeight );
}
std::ostream& operator<< (std::ostream& out, Particle& particle)
{
	return out<<particle.mState;
}


