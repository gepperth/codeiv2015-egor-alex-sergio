import matplotlib.pyplot as plt
import matplotlib


matplotlib.rc('xtick', labelsize=8) #  Andern Sie die Grosse der Schrift hier
matplotlib.rc('ytick', labelsize=8) 

matplotlib.rcParams.update({'font.size': 8})

fileFolder="/home/egor/soms/"


tprateCam=[]
fprateCam=[]
tprateLas=[]
fprateLas=[]

tprateBoth=[]
fprateBoth=[]

fig=plt.figure(figsize=(8, 8), dpi=800)
axe1=fig.add_subplot(1,1,1)

f=open(fileFolder+"fnfp2.txt", 'r')
line=f.readline()

while line:
   decompose=line.split(' ')

   fpCam=float(decompose[1])
   fnCam=float(decompose[2])
   tpCam=float(decompose[3])
   tnCam=float(decompose[4])
   fpLas=float(decompose[5])
   fnLas=float(decompose[6])
   tpLas=float(decompose[7])
   tnLas=float(decompose[8])
   fpBoth=float(decompose[9])
   fnBoth=float(decompose[10])
   tpBoth=float(decompose[11])
   tnBoth=float(decompose[12])

   fprateCam.append(fpCam/(fpCam+tnCam))
   tprateCam.append(tpCam/(tpCam+fnCam))

   fprateLas.append(fpLas/(fpLas+tnLas))
   tprateLas.append(tpLas/(tpLas+fnLas))

   fprateBoth.append(fpBoth/(fpBoth+tnBoth))
   tprateBoth.append(tpBoth/(tpBoth+fnBoth))

   line=f.readline()

f.close()

axe1.plot(tprateCam,fprateCam,'r*-', label='Camera->Laser prediction')
axe1.plot(tprateLas,fprateLas,'b*-', label='Laser->camera prediction')
axe1.plot(tprateBoth,fprateBoth,'g*-', label='Both prediction')

axe1.set_ylabel('FP rate')
axe1.set_xlabel('TP rate')
axe1.set_title('ROC curve')
handles, labels = axe1.get_legend_handles_labels()
axe1.legend(handles, labels)



plt.savefig(fileFolder+'fnfp2.png',dpi=120)


"""

fnCam=[]
fpCam=[]

fnLas=[]
fpLas=[]

tpCam=[]
tpLas=[]

fig=plt.figure(figsize=(16, 8), dpi=800)#figsize=(8,8))
axe1=fig.add_subplot(1,2,1)
axe2=fig.add_subplot(1,2,2)

axe1.set_xlim([-0.01,1.05])
axe2.set_xlim([-0.01,1.05])
axe1.set_ylim([-0.01,1.05])
axe2.set_ylim([-0.01,1.05])

iterLimit=float('inf') #200     30000#

f=open(fileFolder+"fnfp.txt", 'r')
line=f.readline()
it=0
while line:
   decompose=line.split(' ')

   fpCam.append(float(decompose[1]))
   fnCam.append(float(decompose[2]))

   
   fpLas.append(float(decompose[3]))
   fnLas.append(float(decompose[4]))

   tpCam.append(float(decompose[5]))
   tpLas.append(float(decompose[6]))


   line=f.readline()
   it=it+1
f.close()


axe1.plot(fnCam,fpCam,'g*-')
axe1.plot(fnLas,fpLas,'r*-')
axe1.set_ylabel('fp')
axe1.set_xlabel('fn')
axe1.set_title('Camera and laser')

axe2.plot(tpCam,fpCam,'r*-')
axe2.plot(tpLas,fpLas,'b*-')
axe2.set_ylabel('fp')
axe2.set_xlabel('tp')
axe2.set_title('Camera-laser')



plt.savefig(fileFolder+'fnfp.png',dpi=120)
"""

