#-------------------------------------------------
#
# Project created by QtCreator 2014-12-18T10:48:19
#
#-------------------------------------------------

QT       += xml

TARGET = base
TEMPLATE = lib
CONFIG += staticlib
CONFIG += c++11
INCLUDEPATH += /usr/include/pcl-1.7/
INCLUDEPATH += /usr/include/eigen3/


SOURCES += \
    detection.cpp \
    feeder.cpp \
    field.cpp \
    filereader.cpp \
    gmphdFilter.cpp \
    kalman.cpp \
    matrix.cpp \
    mcmcda.cpp \
    module.cpp \
    msInterface.cpp \
    particle.cpp \
    particleFilter.cpp \
    phdFilter.cpp \
    phdInterface.cpp \
    plane3D.cpp \
    target.cpp \
    som.cpp \
    fusionsom.cpp \
    state2D.cpp \
    state2Ddim.cpp \
    state3D.cpp \
    detect.cpp \
    circle.cpp \
    cluster.cpp

HEADERS += \
    configuration.h \
    detection.h \
    feeder.h \
    field.h \
    filereader.h \
    gaussian.h \
    gmphdFilter.h \
    gnuplot.hpp \
    kalman.h \
    matrix.h \
    mcmcda.h \
    module.h \
    msInterface.h \
    particle.h \
    particleFilter.h \
    phdFilter.h \
    phdInterface.h \
    plane3D.h \
    point.h \
    state.h \
    target.h \
    tracklets.h \
    som.h \
    fusionsom.h \
    state2D.h \
    state2Ddim.h \
    state3D.h \
    circle.h \
    cluster.h \
    node.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}


