#ifndef POINT_H_
#define POINT_H_

#include <random>
#include <chrono>
#include <iostream>
#include <cfloat>

class Point
{
public:
	double mX;
	double mY;
	double mW;
	double mH;
	double mVx;
	double mVy;
	double mAlpha;
	int nFrame;
	Point() {};
	~Point() {};
    void generate(int frame, int wWidth, int wHeight, double w, double h)
    {
    	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    	default_random_engine generator(seed);
    	//seed = chrono::system_clock::now().time_since_epoch().count();
    	uniform_real_distribution<double> uniformDistributionX(0.0,wWidth);
    	uniform_real_distribution<double> uniformDistributionY(0.0,wHeight);
    	normal_distribution<double> distributionW(1.0,0.2);
    	normal_distribution<double> distributionH(1.0,0.2);

    	this->mX=uniformDistributionX(generator);
    	this->mW=w*distributionW(generator);
    	this->mY=uniformDistributionY(generator);
    	this->mH=h*distributionH(generator);
    	this->nFrame=frame;
    }
    void fluctuateSize()
    {
    	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    	default_random_engine generator(seed);
    	normal_distribution<double> distribution(1.0,0.2);
    	double multiplier=distribution(generator);
    	this->mW*=multiplier;
    	this->mH*=multiplier;
    }
    void fluctuatePosition(double sigma)
    {
    	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    	default_random_engine generator(seed);
    	normal_distribution<double> distribution(0.0,sigma);
    	this->mX+=distribution(generator);
    	this->mY+=distribution(generator);
    }
    bool isTaken (double p)
    {
    	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    	default_random_engine generator(seed);
    	uniform_real_distribution<double> uniformDistribution(0.0,1.0);
    	return (uniformDistribution(generator)>p);
    }
    void coordinatesToLine(double i, double j, const Point &end, double &x, double &y) const
    {
    	double a=end.mY-this->mY;
    	double b=this->mX-end.mX;
    	double c=-this->mX*a-this->mY*b;
    	x=(b*(b*i-a*j)-a*c)/(a*a+b*b)-i;
    	y=(a*(-b*i+a*j)-b*c)/(a*a+b*b)-j;
    }
    double distanceToLine(double i, double j, const Point &end) const
    {
    	double a=end.mY-this->mY;
    	double b=this->mX-end.mX;
    	double c=-this->mX*a-this->mY*b;
    	double x=(b*(b*i-a*j)-a*c)/(a*a+b*b);
    	double y=(a*(-b*i+a*j)-b*c)/(a*a+b*b);
    	if (x<=max(this->mX,end.mX)&&x>=min(this->mX,end.mX)
    	    	&&y<=max(this->mY,end.mY)&&y>=min(this->mY,end.mY))
    	  return abs(a*i+b*j+c)/sqrt(a*a+b*b);
    	else return DBL_MAX;
    }
};



#endif /* POINT_H_ */
