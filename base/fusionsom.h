#ifndef FUSIONSOM_H
#define FUSIONSOM_H


#include <QTextStream>
#include <QFile>
#include "som.h"
#include "filereader.h"

class fusionSOM
{
public:
    vector<vector<SomPoint> > data;
    vector<vector<SomPoint> > camData;
    vector<vector<SomPoint> > lasData;
    vector<vector<SomPoint> > trueData;
    vector<vector<SomPoint> > falseData;
    Som somCam;
    Som somLas;
    Som somRectSize;
    bool isTracklets;
    fusionSOM();
    ~fusionSOM();
    void learn();
    void reduceData(int i);
    void associateEval(int nTimes);
    void associateLearn(int maxK);
    void falsePosNegCalc2();
    void falsePosNegCalc3();
    void falsePosNegCalc4();
    void falsePosNegCalc5();
    void desassociate();
    double approaching(int numCam, int numLas, int& numCamNew, int& numLasNew);
    void divideData(vector<vector<SomPoint> > data, vector<vector<SomPoint> >& bigPart, vector<vector<SomPoint> >& smallPart);
    void transformData(vector<vector<SomPoint> >& data);
    void getFrameDetects(int k, vector<int> &nodesCam, vector<int> &nodesLas, vector<vector<vector<double> > > &weights);
};

#endif // FUSIONSOM_H
