#include "phdInterface.h"


PHDInterface::PHDInterface()
{

}
PHDInterface::~PHDInterface()
{

}

int PHDInterface::initialize(vector<string> args)
{
	if(!config.loadFromFile (args))
		cout<<"BAD CONFIG"<<endl;

	vector<shared_ptr<Module> > tmp;
	return module.configurate(config.windows[0],tmp);
}


int PHDInterface::execute()
{
	return module.execute();
}



