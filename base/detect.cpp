/* Observations:
- the resampling variances for dimension and speed are hard-set in state.cpp :: predict -- changed!!

- the vectors used ans sigmas are totalyly dcorrelated: used indexes coordinates, sigmas components of coordinates (pos, size, dir)

- likewise the scale dependency of prediction is hardcoded there

- teh resamplingCoeff is basically used to increase or decrease the sigmas during reqsampling. The sigmas thus do n,ot necessarily mean
  anything by themselves

- no noise is added during prediction

- seems that opencv hogs vectors are not normalized to 1.0. So any linear SVM trained on normalized vecgtors needs to be adapted.

- new birth a	nd death model: unassigned dets create a track with prob mPDeltaPlus. Each assoc increases the prob by mPDeltaPlus, each missed assoc
  decreases it my mPdeltaMinus. When prob reqches mPb it is official, when track reaches mPd it gets deleted.

- pfn increases weight of each particle at ech step by probTrack*mPfn/nrParticles so that parts do not die in case of no assignments
*/

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/core/mat.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "detection.h"
#include "phdFilter.h"
#include "state2Ddim.h"

using namespace std ;
using namespace cv ;

int area(CvRect r)
{
  return r.width * r.height ;
}


CvRect det2rect(const Detection & d)
{
  CvRect r;
  const shared_ptr<State2Ddim> st =   dynamic_pointer_cast<State2Ddim> (d.getState()) ;
  r.x = st->mX - st->mDx ;
  r.y = st->mY - st->mDy ;
  r.width = 2*st->mDx ;
  r.height = 2*st->mDy ;
  return r;
}


int areaDet(const Detection & d)
{

  return area(det2rect(d));

}


int intersect(CvRect & r1, CvRect & r2)
{
  CvRect intersection;

// find overlapping region
  intersection.x = (r1.x < r2.x) ? r2.x : r1.x;
  intersection.y = (r1.y < r2.y) ? r2.y : r1.y;
  intersection.width = (r1.x + r1.width < r2.x + r2.width) ? r1.x + r1.width : r2.x + r2.width;
  intersection.width -= intersection.x;
  intersection.height = (r1.y + r1.height < r2.y + r2.height) ? r1.y + r1.height : r2.y + r2.height;
  intersection.height -= intersection.y;

  if ((intersection.width <= 0) || (intersection.height <= 0))
    return 0 ;
  else
    return area (intersection) ;
}

float overlap(CvRect r1, CvRect r2)
{
  int inter = intersect(r1,r2) ;
  int join = area(r1) + area(r2) - inter ;

  return float (inter)/ float (join) ;
}


float overlapDets(Detection & d1, Detection & d2)
{
  return overlap(det2rect(d1),det2rect(d2)) ;
}

bool sortFunction (const Detection & d1, const Detection & d2)
{
  return d1.getScore() > d2.getScore() ;
}



void maxSuppress (vector <Detection> & detects, float thrOver, float areaFactor )
{

  vector <bool> deleted(detects.size()) ;
  std::fill (deleted.begin(), deleted.end(), false ) ;

  std::sort(detects.begin(), detects.end(), sortFunction) ;

  if (detects.size()==0)
    return ;

  for(int killer=0; killer<(int)detects.size(); killer++)
  {
    for (int victim=killer+1; victim < (int)detects.size(); victim++)
    {
      if (overlapDets(detects[killer], detects[victim]) >= thrOver )
        deleted[victim] = true ;
    }

  }

  int k=0; int i=0 ;
  while (k<(int)detects.size())
  {
    if (deleted[i])
    {
      // delete from detects
      detects.erase(detects.begin()+k) ;
      k-=1;
    }
    k++ ;
    i++ ;
  }


}

void maxSuppressGeneric (vector <Detection> & detects, float thrOver, float areaFactor, vector <bool> & deleted )
{

  deleted.resize(detects.size()) ;
  std::fill (deleted.begin(), deleted.end(), false ) ;

  std::sort(detects.begin(), detects.end(), sortFunction) ;

  if (detects.size()==0)
    return ;

  for(int killer=0; killer<(int)detects.size(); killer++)
  {
    for (int victim=killer+1; victim < (int)detects.size(); victim++)
    {
      if (overlapDets(detects[killer], detects[victim]) >= thrOver )
        deleted[victim] = true ;
    }

  }
/*
  int k=0; int i=0 ;
  while (k<detects.size())
  {
    if (deleted[i])
    {
      // delete from detects
      detects.erase(detects.begin()+k) ;
      k-=1;
    }
    k++ ;
    i++ ;
  }
  */

}


// Used to sort two associated vectors
struct CmpWeight
{
    CmpWeight(vector<double>& vec) : values(vec){}
    bool operator() (const int& a, const int& b) const
    {
        return values[a] < values[b];
    }
    vector<double>& values;
};


double gauss(double x, double y, double x0, double y0, double sigma)
{
  return exp(-1.0/(2.0*sigma*sigma) * ((x-x0)*(x-x0)+(y-y0)*(y-y0))) ;
}



void modifyScoreImage (int scale, double scaleFactor, cv::Mat scores, vector <vector <Particle> > & particles, vector <double> & targetProbs, cv::Size win_size, cv::Size block_stride, double sigma, double targetProbThr, double coeff)
{
  ///return ;
  double realScale = pow(scaleFactor,double(scale)) ;
  double widthScore = (float(win_size.width)*realScale*0.5);
  double heightScore =(float(win_size.height)*realScale*0.5);
  int index=0 ;
  for (int t=0; t<(int)particles.size(); t++)
    if(targetProbs[t] >=targetProbThr)

    for(int p=0; p<(int)particles[t].size(); p++)
    {
      const shared_ptr<State2Ddim> s =   dynamic_pointer_cast<State2Ddim> (particles[t][p].getState()) ;
      double w0=s->mDx ;
      double x0=s->mX; // WHY ?????
      double y0=s->mY ; // WHY ????
//      double x0=s->xs-w0 ; // WHY ?????
//      double y0=s->ys-2.*w0 ; // WHY ????
      float particleScaleIndex = log(2.*w0/double(win_size.width) )/ log(sqrt(2.0)) ;
      //double h0=c1->mDimension ;
      double h0=s->mDy ; // ****
      int deltaY = 10 ;
      int deltaX = 10 ;
    //  cout << "Particle " << t << "/" << p << ": " << x0 << "/" << y0 << "/" << w0 << endl ;

      double tildey0 = int ((y0-2.*w0) / realScale / double(block_stride.height) ) ;
      double tildex0 = int ((x0-w0) / realScale / double(block_stride.width) ) ;
  //    cout << "Particle " << t << "/" << p << ": " << tildex0 << "/" << tildey0 << "/" << w0 << endl ;
      for(int y=int(tildey0)-deltaY;y< int(tildey0)+deltaY ; y++)
      {
        for(int x=int(tildex0)-deltaX; x<int(tildex0)+deltaX ; x++)
        {

//	    cout << "...." << endl ;
            if((y<0) || (y>=scores.rows))
              continue ;
            if((x<0) || (x>=scores.cols))
              continue ;
        double & sc = scores.at<double>(y,x) ;
//	    cout << "...." << endl ;

        // determine CENTER coordinates of image coord bounding box
        double xDet = double(x)*realScale*double(block_stride.width)+widthScore ;
        double yDet = double(y)*realScale*double(block_stride.height)+heightScore ;

        float size=s->mDy ;
        //float size=c1->mDimension ;
        float w= particles[t][p].getWeight();
        //cout << w << endl ;
        //double coeffScale=gauss(0,double(scale), 0, particleScaleIndex,1.) ;
        double coeffSpace = gauss (xDet,yDet,x0,y0,double(widthScore)) ;

        double coeffScale=gauss(0,scale, 0, particleScaleIndex, 1.0) ;
    //    if (coeffSpace >= 0.5)
        //      cout << "!!" << w0/float(win_size.width) << endl ;
        //if (coeffSpace >= 0.0 &&coeffScale >= 0.9)
        //  cout << xDet << " " << yDet << " " << x0 << " " << y0 << " " << scale << " " << particleScaleIndex <<  " " << realScale << endl ;
/*   if (fabs(scale - particleScaleIndex) < 0.5)
          cout << scale - particleScaleIndex << " " << scale << " " << coeffScale << endl;
        if (coeffSpace >= 0.5 && coeffScale >= 0.9)
        {
          cout << x << " " << y << " " << x0 << " " << y0 << " " << scale << " " << particleScaleIndex <<  endl ;
          cout << coeffSpace << " " << coeffScale << " " << w << endl ;
        } */

        sc *= (1.+coeff*coeffSpace*coeffScale*w) ;

      }
    }
  }
}



void modifyScoreImageJournal (double scaleFactor, double scale, cv::Mat scores, vector <vector <Particle> > & particles, vector <Target*> & targets, cv::Size win_size, cv::Size block_stride, double sigma, double targetProbThr, double coeff)
{
  ///return ;
  //cout << "Influence streng="<< coeff << "/" << targetProbThr << "/" << targets.size() << endl ;
  double widthScore = (float(win_size.width)*scaleFactor*0.5);
  double heightScore =(float(win_size.height)*scaleFactor*0.5);
  int index=0 ;
  for (int t=0; t<(int)targets.size(); t++)
  {
    if(targets[t]->getProbability() >=targetProbThr)
    {
      double maxScale=-10000, maxSpace = -10000, maxCoeff=-10000 ;
//      double tildey0 = int ((y0-2.*w0) / realScale / double(block_stride.height) ) ;
//      double tildex0 = int ((x0-w0) / realScale / double(block_stride.width) ) ;

      const shared_ptr<State2Ddim> st =   dynamic_pointer_cast<State2Ddim>(targets[t]->getState());


      double t_x = st->mX ;
      double t_y = st->mY ;

      // determine score position XY for target
      // in order just to modify scores close to actual targets
      int tScorePosY = int ((t_y - heightScore) / double(block_stride.width)) ;
      int tScorePosX = int ((t_x - widthScore) / double(block_stride.height)) ;
      int tScoreDeltaX = int (widthScore / double(block_stride.width))+1 ;
      int tScoreDeltaY = int (heightScore / double(block_stride.height))+1 ;
      for(int y=std::max(0,tScorePosY-tScoreDeltaY);y<std::min(tScorePosY+tScoreDeltaY,scores.rows) ; y++)
      {
        // determine CENTER coordinates of image coord bounding box
        double yDet = double(y)*scaleFactor*double(block_stride.height)+heightScore ;
        for(int x=std::max(0,tScorePosX-tScoreDeltaX);x<std::min(tScorePosX+tScoreDeltaX,scores.cols) ; x++)
        {
      double xDet = double(x)*scaleFactor*double(block_stride.width)+widthScore ;

          double & sc = scores.at<double>(y,x) ;
          double coeffScale = 0. ;
          double coeffSpace = 0., coeffSum=0.0  ;
          double wsum = 0.0 ;
          for(int p=0; p<particles[t].size(); p++)
          {
            const shared_ptr<State2Ddim> s =   dynamic_pointer_cast<State2Ddim>(particles[t][p].getState()) ;

            double w0=s->mDx ;
            double h0=s->mDy ;
            double x0=s->mX ;
            double y0=s->mY ;
            float particleScaleIndex = log(2.*w0/double(win_size.width) )/ log(sqrt(2.0)) ;
            //double h0=c1->mDimension ;
            //  cout << "Particle " << t << "/" << p << ": " << x0 << "/" << y0 << "/" << w0 << endl ;

            //    cout << "Particle " << t << "/" << p << ": " << tildex0 << "/" << tildey0 << "/" << w0 << endl ;
            //	    cout << "...." << endl ;
//	    cout << "...." << endl ;


        double size=s->mDy ;
        double w= particles[t][p].getWeight();
        wsum += w ;
        coeffSpace = gauss (xDet,yDet,x0,y0,double(widthScore)) ;

        coeffScale =gauss(0,scale, 0, particleScaleIndex, 1.) ;
        coeffSum += w*coeffSpace*coeffScale ;


      }
      //cout << "wsum for target " << t << " is " << wsum << endl ;
      if(coeff*coeffSum/wsum > maxCoeff)
        maxCoeff = coeff*coeffSum/wsum ;

      sc = (sc+3)*(1.+coeffSum*coeff/wsum)-3. ;
       }
     }
   //cout << "at infleunce of " << coeff << ", maxCoeff of target " << t << "= " << maxCoeff << endl ;
   }
  }
}



void drawParticles(cv::Mat & img, vector <Target *> & targets, vector <vector <Particle > > & particles, vector <double> & targetProbs, double targetProbThr, int shade, bool drawTargets, bool drawParticles, bool drawSpeed)
{
    //cout << "Draxing " << targets.size() << "targets" << " " << targetProbThr << endl ;
    for (int i=0; i<(int)targets.size(); i++)
    {
      //cout << "probab of target "<< i << " " << targetProbs[i] << endl ;
    if(targetProbs[i] >=targetProbThr)
    {

        const shared_ptr<State2Ddim> st2 =   dynamic_pointer_cast<State2Ddim>(targets[i]->getState());

        int xLeft=st2->mX - st2->mDx ;
        int xRight=st2->mX + st2->mDx ;
        int yTop=st2->mY - st2->mDy ;
        int yBottom=st2->mY + st2->mDy ;

        drawTargets=true;
        if(drawTargets)
        {
          CvRect r;
          r.x = xLeft; r.y = yTop ;
          r.width = xRight-xLeft;
          r.height = yBottom-yTop ;
          rectangle(img, r, CV_RGB(0,0,50), 1) ;
    }

    for (int p=0; p<(int)particles[i].size(); p++)
    {

      const shared_ptr<State2Ddim> s =   dynamic_pointer_cast<State2Ddim>(particles[i][p].getState()) ;
      float xSpeed=s->mVx ;
      float ySpeed=s->mVy ;
      float x=s->mX ;
      float y=s->mY ;

      float size=s->mDy * 0.1 ;
      float w= particles[i][p].getWeight();
      //cout << x << " " << y << " " << size << endl ;
      if( drawParticles)
      {
        line(img,cv::Point(x+0.5*size,y+0.5*size), cv::Point(x-0.5*size,y-0.5*size),CV_RGB(shade,shade,shade),1,CV_AA,0);
        line(img,cv::Point(x+0.5*size,y-0.5*size), cv::Point(x-0.5*size,y+0.5*size),CV_RGB(shade,shade,shade),1,CV_AA,0);
      }
      if(drawSpeed)
          { //cout << "speed draw!" << xSpeed << " " << ySpeed << endl ;
        line(img,cv::Point(x,y), cv::Point(x+size*xSpeed,y+size*ySpeed),CV_RGB(50,50,50),1,CV_AA,0);
          }
    }

    }
    }

}


void getTargetProbsArray(PHDFilter & filter, vector<double> & targetProbs)
{
  targetProbs.resize(0) ;
  for (int z=0; z<filter.getTargetsNumber(); z++)
    targetProbs.push_back( filter.getTargetProbability(z) )  ;
}

class point2d3d
{
  public:
  float x,y,z ;
  float x2d,y2d ;
  const float dist3d(const point2d3d & py) const {return sqrt(  (x-py.x)*(x-py.x)+(y-py.y)*(y-py.y)+(z-py.z)*(z-py.z) ) ;}
} ;




void readLaserData(std::string& laser2dFile, std::string&laserPointsFile, vector <point2d3d> & points )
{
  std::vector <float> laser2dX;
  std::vector <float> laser2dY;
  std::vector <float> laser3dX;
  std::vector <float> laser3dY;
  std::vector <float> laser3dZ ;
  ifstream p3d (laserPointsFile.c_str()) ;
  ifstream p2d (laser2dFile.c_str()) ;
  laser2dX.resize(0) ;
  laser2dY.resize(0) ;
  laser3dX.resize(0) ;
  laser3dY.resize(0) ;
  laser3dZ.resize(0) ;

  while (!p3d.eof())
  {
    float v1,v2,v3,v4,v5 ;
    p3d >> v1 >> v2 >> v3 >> v4 >> v5 ;

    laser3dX.push_back(v3) ;
    laser3dY.push_back(v4) ;
    laser3dZ.push_back(v5) ;
    p2d >> v1 >> v2  ;
//    cout << v1 << v2 << endl ;
    laser2dX.push_back(v1) ;
    laser2dY.push_back(v2) ;
  }

  p3d.close() ;
  p2d.close() ;

  points.resize(0) ;
  point2d3d pt;
  for (int i=0; i<laser2dX.size(); i++)
  {
    pt.x = laser3dX[i] ;
    pt.y = laser3dY[i] ;
    pt.z = laser3dZ[i] ;
    pt.x2d = laser2dX[i] ;
    pt.y2d = laser2dY[i] ;
    points.push_back(pt) ;
  }
}


void laserPointsInDetection(CvRect r, vector<point2d3d> & points, vector <point2d3d> & result )
{
  result.resize(0) ;

  float xMin;float yMin; float xMax;float yMax;
  xMin = r.x ; yMin = r.y ; xMax = r.x+r.width ; yMax = r.y+r.height ;

  for (vector<point2d3d>::iterator el = points.begin() ; el != points.end(); el++)
  {
    if ((xMin <= (*el).x2d) && (xMax >= (*el).x2d) && (yMin <= (*el).y2d) && (yMax >= (*el).y2d))
      result.push_back(*el);
  }
}

bool predicate(const point2d3d & a, const point2d3d & b)
{
  if (a.dist3d(b) >= 0.5)
    return false ;
  else
    return true ;
}

void findClosestSubCloud (vector <point2d3d> & cloud, vector <point2d3d> & result)
{
  vector <int> labels ;
  cv::partition<point2d3d>(cloud, labels, predicate) ;

  point2d3d zero ;

  // how many clusters?
  int maxLabel = -1 ;
  for (int l=0; l<(int)labels.size(); l++)
    if (labels[l] > maxLabel) maxLabel = l;

  //cout << "nrClusters " << maxLabel << endl ;

  // min disdtances in each cluster
  vector <float> minDistances (maxLabel+1);
  for (vector<float>::iterator f=minDistances.begin() ; f != minDistances.end(); f++)
    (*f)=10000000. ;

  zero.x=0 ; zero.y = 0 ;zero.z = 0 ;
  for (int el = 0 ; el < (int)cloud.size(); el++)
  {
    int label = labels[el] ;
    if (zero.dist3d(cloud[el]) < minDistances[label])
      minDistances[label] =zero.dist3d(cloud[el]) ;
  }
  //cout << "min didstances" << endl ;
  //for (vector<float>::iterator f=minDistances.begin() ; f != minDistances.end(); f++)
  //  cout << (*f) << endl ;



  int bestLabel = -1 ;
  float bestDist = 1000000000;
  for (int l=0; l <= maxLabel; l++)
    if (minDistances[l] < bestDist)
    {
      bestLabel = l ;
      bestDist = minDistances[l] ;
    }
//  cout << "best Label "<< bestLabel << endl ;

  result.resize(0) ;
  for (int el = 0 ; el < (int)cloud.size(); el++)
    if(labels[el]==bestLabel)
      result.push_back(cloud[el]) ;


}


void calcCOG(vector <point2d3d> & cloud, float & x, float & y, float & z)
{
  x = y = 0. ;
  int sz = cloud.size() ;

  for (int el=0; el<sz; el++)
  {
    x+=cloud[el].x ; // left/right
    y+=cloud[el].y ; // far/near
  }

  x /= float(sz) ;
  y /= float(sz) ;
  z = 1.0 ;


}
