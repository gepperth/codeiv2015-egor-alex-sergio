#ifndef MCMCDA_H_
#define MCMCDA_H_

#include "detection.h"
#include "particleFilter.h"
#include "kalman.h"

class MCMCDA
{
public:
	vector<vector<Detection> > mW;
    vector<vector<Detection> > mWnew;
    default_random_engine generator;
    ParticleFilter particleFilter;
    int nt;
    int maxFrame;
public:
    MCMCDA();
    ~MCMCDA();
    void evaluate(vector<Detection> detections,int nFrame);
    bool birth(int nFrame);
    bool death(int nFrame);
    bool split(int nFrame);
    bool merge(int nFrame);
    bool extention(int nFrame);
    bool reduction(int nFrame);
    bool update(int nFrame);
    bool switchf(int nFrame);
    double pAccepted(int nFrame);
    double P(vector<vector<Detection> >& W, int nFrame);
    double Q(vector<vector<Detection> >& W);
};




#endif /* MCMCDA_H_ */
