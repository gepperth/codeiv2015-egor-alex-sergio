/*
 * cartesian2D.cpp
 *
 *  Created on: Aug 7, 2014
 *      Author: egor
 */
#include "state2D.h"

using namespace std;

State2D::State2D()
{

}

void State2D::add(const shared_ptr<State> i)
{
    const shared_ptr<State2D> s=dynamic_pointer_cast<State2D>(i);
    mX+=s->mX;
    mY+=s->mY;
    mVx+=s->mVx;
    mVy+=s->mVy;
}

void State2D::sub(const shared_ptr<State> i)
{
    const shared_ptr<State2D> s=dynamic_pointer_cast<State2D>(i);
    mX-=s->mX;
    mY-=s->mY;
    mVx-=s->mVx;
    mVy-=s->mVy;
}

void State2D::divBy(double d)
{
    mX/=d;
    mY/=d;
    mVx/=d;
    mVy/=d;
}

shared_ptr<State> State2D::copy()
{
    shared_ptr<State2D> copy=make_shared<State2D>();

    copy->mX=mX;
    copy->mY=mY;
    copy->mVx=mVx;
    copy->mVy=mVy;

    return copy;
}

void State2D::set(const vector<double> & data)
{
    if (data.size()!=4)
        cout<<"error data size"<<endl;
    mX=data[0];
    mY=data[1];
    mVx=data[2];
    mVy=data[3];

}

void State2D::get(vector <double> & data) const
{
    data.clear();
    data.push_back(mX);
    data.push_back(mY);
    data.push_back(mVx);
    data.push_back(mVy);
}

double State2D::getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const
{
    const shared_ptr<State2D> s=dynamic_pointer_cast<State2D>(i);
    double ex=(pow(mX-s->mX,2)+pow(mY-s->mY,2))/(2.0*sigmas[0]*sigmas[0]);
    ex+=(pow(mVx-s->mVx,2)+pow(mVy-s->mVy,2))/(2.0*sigmas[1]*sigmas[1]);

    return exp(-ex)*1000000;
}
void State2D::setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end)
{
    const shared_ptr<State2D> b=dynamic_pointer_cast<State2D>(begin);
    const shared_ptr<State2D> e=dynamic_pointer_cast<State2D>(end);
    mVx=e->mX-b->mX;
    mVy=e->mY-b->mY;
}

void State2D::applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator)
{
    normal_distribution<double> distributionCoord(0.0,sigmas[0]);
    normal_distribution<double> distributionSpeed(0.0,sigmas[1]);

    mX+=distributionCoord(generator);
    mY+=distributionCoord(generator);
    mVx+=distributionSpeed(generator);
    mVy+=distributionSpeed(generator);
}

void State2D::predict ()
{
    mX+=mVx;
    mY+=mVy;
}

ostream& State2D::print(ostream& out)
{
    out<<mX<<" "<<mY<<" "<<mVx<<" "<<mVy<<" ";
    return out;
}

void State2D::draw(QPainter& painter,double& dim)
{
    if (dim<=0)
        painter.drawPoint (QPointF(mX, mY));
    else
        painter.drawRect ( QRectF (mX-dim, mY-dim, 2*dim, 2*dim));
}

void State2D::drawConnection(QPainter& painter,const shared_ptr<State> i)
{
    double xold=mX;
    double yold=mY;
    double xnew=dynamic_pointer_cast<const State2D>(i)->mX;
    double ynew=dynamic_pointer_cast<const State2D>(i)->mY;
    painter.drawLine ( QLineF(xold,yold,xnew,ynew) );
}

void State2D::drawPoint(QPainter& painter)
{
    painter.drawPoint (QPointF(mX, mY));
}

void State2D::drawText(QPainter& painter,QString text)
{
    painter.drawText( QPoint (mX, mY), text);
}




