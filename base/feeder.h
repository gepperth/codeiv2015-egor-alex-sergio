/*
 * feeder.h
 *
 *  Created on: Nov 21, 2014
 *      Author: egor
 */

#ifndef SRC_FEEDER_H_
#define SRC_FEEDER_H_

#include "phdFilter.h"
#include "tracklets.h"
#include "filereader.h"
#include <dirent.h>

class Module;

class Feeder
{
public:
	int numIter;
	int numIterMax;
	string type;
	Tracklets *tracklets;
	shared_ptr<Module> module;
	ifstream data;
	string dataFolder;
    string imageFolder;
    vector<string> imageNames;
	CarData cardata;
	string line;
public:
	Feeder();
	~Feeder();
	int connect(string detectionsFileName,string imageFolderName,string cardataName, vector<shared_ptr<Module> >& modules);
	int get(vector<Detection>& detections,string& imageName,string& outName);
};



#endif /* SRC_FEEDER_H_ */
