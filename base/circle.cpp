#include "circle.h"


vector<Circle> circling::create(int num, double rad, double xmin, double ymin, double xmax, double ymax)
{
    default_random_engine generator;
    uniform_real_distribution<> distrX(xmin,xmax);
    uniform_real_distribution<> distrY(ymin,ymax);
    vector<Circle> circles;
    for (int i=0; i<num; i++)
    {
        Circle circle;
        circle.mX=distrX(generator);
        circle.mY=distrY(generator);
        circle.mR=rad;
        circles.push_back(circle);
    }
    return circles;
}
void circling::vote(vector<ClusterPoint>& points, vector<Circle>& circles)
{
    for (unsigned int i=0; i<points.size(); i++)
        for (unsigned int j=0; j<circles.size(); j++)
            circles[j].process(points[i]);
}
