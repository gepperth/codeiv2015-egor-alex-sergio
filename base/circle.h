#ifndef CIRCLE_H
#define CIRCLE_H

#include "filereader.h"

class Circle
{
public:
    double mX;
    double mY;
    double mR;
    int mNum;

public:
    Circle()
    {
        mNum=0;
    }
    void process(ClusterPoint& point)
    {
        if (sqrt(pow(mX-point.mX,2)+pow(mY-point.mY,2))<mR)
            mNum++;
    }
};
namespace circling
{
    vector<Circle> create(int num, double rad, double xmin, double ymin, double xmax, double ymax);
    void vote(vector<ClusterPoint>& points, vector<Circle>& circles);
}

#endif // CIRCLE_H
