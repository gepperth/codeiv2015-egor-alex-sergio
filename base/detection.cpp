#include "detection.h"

using namespace std;

Detection::Detection()
{
	mScore=0;
}
Detection::Detection(const shared_ptr<State> state, double score)
{
    mState=state->copy();
	mScore=score;

}
Detection::~Detection()
{

}
void Detection::setID(int id)
{
	mID=id;
}
int Detection::getID() const
{
	return mID;
}
void Detection::setScore(double score)
{
	mScore=score;
}
double Detection::getScore() const
{
	return mScore;
}

Detection Detection::operator=(const Detection i)
{
    this->mState=i.mState->copy();
	this->mScore=i.mScore;
	return *this;
}
void Detection::setState(const shared_ptr<State> state)
{
    mState=state->copy();
}
shared_ptr<State> Detection::getState() const
{
    return mState->copy();
}


std::ostream& operator<< (std::ostream& out, const Detection& detection)
{
    const shared_ptr<State> state=detection.getState();
    state->print(out);
    out<<detection.getScore()<<" ";
	return out;
}




