#include "phdFilter.h"

//#define MFORCE 0.1

PHDFilter::PHDFilter()
{
	mPfn=0;
	mPb=0;
	mPd=0;
    mPdeltaPlus=0;
    mPdeltaMinus=0;
	idNum=0;
	mAssociationCoef=1.0;
	mResampleCoef=1.0;
	mInfluenceCoef=1.0;
	targetHistoryLength=0;
	nParticlesPerObject=0;
}
PHDFilter::~PHDFilter()
{
	for (unsigned int i=0; i < mTargets.size(); i++)
		delete mTargets[i];
}

void PHDFilter::initialize(int num_particles,
							const vector<double>& sigmas,
							double pfn,
                            double assocC,
                            double resampC,
                            double inflC,
							int historyLength,
							const string& fieldName)
{
	mSigmas=sigmas;
	targetHistoryLength=historyLength;
    mPfn=pfn;

    mAssociationCoef=assocC;
    mResampleCoef=resampC;
    mInfluenceCoef=inflC;

	mPd=0.1;
	mPb=1.0-3*mPd;
    mPdeltaPlus = 0.1;
    mPdeltaMinus = 0.1;

	idNum=0;
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	mGenerator.seed(seed);


	if (fieldName!="")
	{
        /*
        field.load(fieldName);
        cout<<"LOADING FIELD"<<endl;
        */
	}


	nParticlesPerObject = num_particles;
	mParticles.clear();

}
void PHDFilter::prediction()
{
	for (unsigned int j=0; j < mParticles.size(); j++)
	{
		for (unsigned int i=0; i < mParticles[j].size(); i++)
		{
	        //Motion model
            shared_ptr<State> state=mParticles[j][i].getState();
            state->predict();
            mParticles[j][i].setState(state);
		}
	}
}

void PHDFilter::association(const vector<Detection>& detections, ostream& out)
{
    Particle sampledParticle;
    vector<bool> usedTargets(mTargets.size(),false);
    vector<bool> usedObservations(detections.size(),false);
    vector<vector<double> > distances(detections.size(),vector<double> (mTargets.size()));
    for (unsigned int j=0; j < detections.size(); j++)
    	for (unsigned int i=0; i < mTargets.size(); i++)
    	{
            shared_ptr<State> tmp=mTargets[i]->getState();
            tmp->predict();
            distances[j][i]=tmp->getGaussian(detections[j].getState(),mSigmas);
    	}

    double threshold=mAssociationCoef;

    for (unsigned int j=0; j < detections.size(); j++)
    {
        for (unsigned int i=0; i < mTargets.size(); i++)
        {
            if (distances[j][i]>threshold)
            {
                usedTargets[i]=true;
                usedObservations[j]=true;
            }
        }
    }


	for (unsigned int j=0; j < mTargets.size(); j++)
	{
		if (!usedTargets[j])
            mTargets[j]->setProbability(std::max(0.0,mTargets[j]->getProbability()-mPdeltaMinus));
        else
            mTargets[j]->setProbability(min(mTargets[j]->getProbability()+mPdeltaPlus,1.0));
	}

    for (unsigned int j=0;j < detections.size(); j++)
    {

        if (!usedObservations[j])
    	{
    		Target* newTarget=new Target(idNum++,targetHistoryLength);
    		newTarget->setState(detections[j].getState());
    		newTarget->setProbability(mPb);
    		mTargets.push_back(newTarget);
    		mParticles.push_back(vector<Particle>());
    		out<<detections[j].getID()<<" "<<newTarget->getID()<<endl;


    		vector<double> sigmas=mSigmas;
    		for (unsigned int i=0; i<sigmas.size(); i++)
    			sigmas[i]*=mResampleCoef;
    		for (int i=0; i < nParticlesPerObject; i++)
    		{
                shared_ptr<State> randomFluctuation=detections[j].getState()->copy();
                randomFluctuation->applyRandomFluctuation(sigmas,mGenerator);

                      //detections[j].getState().getRandomFieldFluctuation(sigmas,mGenerator,vector<Direction>(),0);
    			sampledParticle.setState(randomFluctuation);
    			sampledParticle.setTarget(newTarget);
    			mParticles.back().push_back(sampledParticle);
    		}
    	}
    }
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
        if (mTargets[i]->getProbability()<mPd)
		{
			delete mTargets[i];
			mTargets.erase (mTargets.begin()+i);
			mParticles.erase (mParticles.begin()+i);
			i--;
		}
	}
}

void PHDFilter::observation(const vector<Detection>& detections)
{

	double sum;
	vector<double> oneComponent,newWeights;
	newWeights.assign(mParticles.size()*nParticlesPerObject,0.0);


	vector<double> sigmas=mSigmas;
	for (unsigned int i=0; i<sigmas.size(); i++)
		sigmas[i]*=mInfluenceCoef;

	for (unsigned int j=0; j < detections.size(); j++)
	{
		oneComponent.assign(mParticles.size()*nParticlesPerObject,0.0);
		sum=0.0;
		unsigned int ik=0;
        shared_ptr<State> oneState=detections[j].getState();


		for (unsigned int i=0; i < mParticles.size(); i++)
		{
            oneState->setDynamics(mTargets[i]->getState(),oneState);
			for (unsigned int k=0; k < mParticles[i].size(); k++)
			{
                oneComponent[ik] = oneState->getGaussian(mParticles[i][k].getState(),sigmas);
				sum += oneComponent[ik];
				ik++;
			}
		}
		if (sum!=0)
		{
			for (ik=0; ik < oneComponent.size(); ik++)
			{
				oneComponent[ik] = oneComponent[ik]/sum;
				newWeights[ik] = newWeights[ik]+oneComponent[ik];
			}
	    }
	}

	int ik=0;
	for (unsigned int i=0; i < mParticles.size(); i++)
	{
		for (unsigned int k=0; k < mParticles[i].size(); k++)
		{
			newWeights[ik] = newWeights[ik]+mPfn*mTargets[i]->getProbability()/nParticlesPerObject;
			mParticles[i][k].setWeight(newWeights[ik]);
			ik++;
		}
	}

	/*
	for (unsigned int i=0; i < mTargets.size(); i++)
	{
		mDirections=field.get(mTargets[i]->getState());
		setValidity(i, mDirections.size());
	}*/

}



void PHDFilter::getCumulatedProbability(int numTarget, vector<double>& cumulatedProbabilities, double& sum)
{
	sum=0.0;
	cumulatedProbabilities.clear();
	for (unsigned int i=0; i < mParticles[numTarget].size(); i++)
	{
		sum+=mParticles[numTarget][i].getWeight();
		cumulatedProbabilities.push_back(sum);
	}
}


void PHDFilter::oneResample(const vector<Particle>& oldParticles, int numTarget, const vector<double>& cumulatedProbabilities, double sum)
{
	uniform_real_distribution<double> uniformDistribution(0.0,sum);

    vector<double> sigmas=mSigmas;

    double multiplicator=1.0/mTargets[numTarget]->getProbability();
    for (unsigned int i=0; i<sigmas.size(); i++)
    	sigmas[i]=sigmas[i]*mResampleCoef*multiplicator;
	for (int i=0; i < nParticlesPerObject; i++)
	{
		int j=0;
	    double randomNumber=uniformDistribution(mGenerator);
	    while ( randomNumber>cumulatedProbabilities[j])
	    	j++;

        shared_ptr<State> randomFluctuation=oldParticles[j].getState()->copy();
        randomFluctuation->applyRandomFluctuation(sigmas,mGenerator);
	    //mDirections=field.get(oldParticles[j].getState());
	    mParticles[numTarget][i].setState(randomFluctuation);
	    mParticles[numTarget][i].setTarget(oldParticles[j].getTarget());
	}
}
/*
void PHDFilter::modifyWithDirection()
{
	for (int j=0; j<(int)mTargets.size(); j++)
	{
		mDirections=field.get(mTargets[j]->getState());
		for (int k=0; k<(int)mDirections.size(); k++)
		{
			for (int i=nParticlesPerObject*(1.0-mTargets[j]->mModelForce); i<nParticlesPerObject*(1.0-mTargets[j]->mModelForce*(1.0-(1.0+k)/mDirections.size())); i++)
			{
                State modified=mParticles[j][i].getState().modifyWithDirection(mDirections[k]);
				mParticles[j][i].setState(modified);
			}
		}
	}
}*/
int PHDFilter::randomFieldFluctuation(int i, int numTarget)
{
	if (i<nParticlesPerObject*(1-mTargets[numTarget]->mModelForce)||mDirections.size()==0)
		 return 0;
	else
	{
		int k=1;
		 while (i>=nParticlesPerObject*(1.0-mTargets[numTarget]->mModelForce*(1.0-(float)k/mDirections.size())))
		    k++;
		 return k;
	}
}

void PHDFilter::resample()
{
	vector<double> cumulatedProbabilities;
	double sum;
	double totalSum=0.0;

	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		getCumulatedProbability(i,cumulatedProbabilities,sum);

		vector<Particle> oldParticles(mParticles[i]);
		oneResample(oldParticles,i,cumulatedProbabilities,sum);
		totalSum+=sum;
	}
}

void PHDFilter::merge(ostream& out)
{
    //vector<double> sigmas=mSigmas;
    //for (unsigned int i=0; i<sigmas.size(); i++)
    //    sigmas[i]*=mAssociationCoef;

    double threshold=mAssociationCoef*10;
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		for (unsigned int j=i+1; j<mTargets.size(); j++)
		{
            double intersection=mTargets[i]->intersection(*mTargets[j],mSigmas);
            if (intersection>threshold)
			{
                *mTargets[i]=mTargets[i]->merge(*mTargets[j]);
				delete mTargets[j];
				out<<mTargets[j]->getID()<<endl;
				mTargets.erase(mTargets.begin()+j);
				mParticles.erase(mParticles.begin()+j);
				j--;
			}
		}
	}
}

vector<Target> PHDFilter::correction()
{
	vector<Target> outTargets(mTargets.size());
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
        shared_ptr<State> meanState=mParticles[i][0].getState();
        for (unsigned int j=1; j < mParticles[i].size(); j++)
		{
            meanState->add(mParticles[i][j].getState());
		}
        meanState->divBy(mParticles[i].size());
		mTargets[i]->setState(meanState);
		outTargets[i]=*mTargets[i];
	}
    return outTargets;

}





vector<Target> PHDFilter::evaluate(const vector<Detection>& detections, vector<vector<Particle> >& weightedParticles, ostream& associations,ostream& unions)
{

	prediction();

    association(detections,associations);

	//modifyWithDirection();

    observation(detections);

	weightedParticles=mParticles;

	resample();

	merge(unions);

	return correction();
}
int PHDFilter::getTargetsNumber()
{
	return mParticles.size();
}
double PHDFilter::getTargetProbability(int i)
{
  return mTargets[i]->getProbability() ;
}
int PHDFilter::getParticlesNumberInTarget(int numTarget)
{
	return mParticles[numTarget].size();
}
Particle PHDFilter::getParticle(int numTarget, int numParticle)
{
	return mParticles[numTarget][numParticle];
}
vector<vector<Particle> > PHDFilter::getParticles()
{
	return mParticles;
}


void PHDFilter::setValidity(int i, int numDirections)
{
	double totalSum=0;
	double oneDirectionSum;
	double maxOneDirectionSum=0;

	for (int j=0; j<nParticlesPerObject; j++)
	{
		totalSum+=mParticles[i][j].getWeight();
	}

	for (int k=0; k<numDirections; k++)
	{
		oneDirectionSum=0;
		for (int j=nParticlesPerObject*(1.0-mTargets[i]->mModelForce*(1-double(k)/numDirections)); j<nParticlesPerObject*(1.0-mTargets[i]->mModelForce*(1-double(k+1)/numDirections)); j++)
		{
			oneDirectionSum+=mParticles[i][j].getWeight();
		}
		maxOneDirectionSum=max(maxOneDirectionSum,oneDirectionSum);
	}

	if (mTargets[i]->mModelForce!=0&&totalSum!=0)
		mTargets[i]->setValidity(maxOneDirectionSum*numDirections/(totalSum*mTargets[i]->mModelForce));
}

void PHDFilter::printParticles(ostream& out)
{
	for (unsigned int i=0; i<mParticles.size(); i++)
	{
		for (unsigned int j=0; j<mParticles[i].size(); j++)
		{
			out << mParticles[i][j]<<": ";
		}
	}
	if (mParticles.size()!=0)
		out << endl;
}
