#include "particleFilter.h"


ParticleFilter::ParticleFilter()
{
	nParticles=0;
	mResampleCoef=1.0;
	mInfluenceCoef=1.0;
}
ParticleFilter::~ParticleFilter()
{

}
void ParticleFilter::initialize(const shared_ptr<State> state, int num_particles, int history_length, vector<double>& sigmas)
{

	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	mGenerator.seed(seed);
	mSigmas=sigmas;
	nParticles = num_particles;
	mParticles.clear();
	mParticles.resize(nParticles);
	mTarget.set(0,history_length);
	vector<Direction> directions;
	vector<double> ssigmas;
	for (unsigned int i=0; i<mSigmas.size(); i++)
		ssigmas.push_back(mSigmas[i]*mResampleCoef);
    for (int i=0; i < nParticles; i++)
    {
        shared_ptr<State> randomFluctuation=state->copy();
        randomFluctuation->applyRandomFluctuation(ssigmas,mGenerator);//getRandomFieldFluctuation(ssigmas,mGenerator,directions,0);
        mParticles[i].setState(randomFluctuation);
    }
    mTarget.setState(state);
}


void ParticleFilter::prediction()
{
    for (int i=0; i < nParticles; i++)
    {
        //with model or without
        shared_ptr<State> state=mParticles[i].getState();
        state->predict();
        mParticles[i].setState(state);
    }
}
shared_ptr<State> ParticleFilter::correction()
{
    shared_ptr<State> meanState=mParticles[0].getState();
    for (int i=1; i < nParticles; i++)
    {
        meanState->add(mParticles[i].getState());
    }
    meanState->divBy(nParticles);

    mTarget.setState(meanState);

    return meanState;

}
void ParticleFilter::observation(const shared_ptr<State> state)
{

	double sum=0.0;

	vector<double> sigmas;
	for (unsigned int i=0; i<mSigmas.size(); i++)
		sigmas.push_back(mSigmas[i]*mInfluenceCoef);
    for (int i=0; i < nParticles; i++)
    {
        mParticles[i].setWeight(mParticles[i].getState()->getGaussian(state,sigmas));
    	sum += mParticles[i].getWeight();
    }
    if (sum!=0)
    {
    	for (int i=0; i < nParticles; i++)
    	{
    		mParticles[i].setWeight(mParticles[i].getWeight()/sum);
    	}
    }
    setValidity(2);

}

void ParticleFilter::resample()
{
    vector<Particle> newParticles(nParticles);

    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	default_random_engine uniformGenerator(seed);

	uniform_real_distribution<double> uniformDistribution(0.0,1.0);
	vector<double> cumulatedProbabilities;
	double sum=0.0;
	for (int i=0; i < nParticles; i++)
	{
		sum+=mParticles[i].getWeight();
		cumulatedProbabilities.push_back(sum);
	}
	vector<double> sigmas;
	for (unsigned int i=0; i<mSigmas.size(); i++)
		sigmas.push_back(mSigmas[i]*mResampleCoef);

    shared_ptr<State> randomFluctuation;
	int i, j;
	double randomNumber;
    //vector<Direction> directions;

	for (i=0; i < nParticles; i++)
	{
		if (sum==0)
			j=i;
		else
		{
			j=0;
			randomNumber=uniformDistribution(uniformGenerator);
			while ( randomNumber>cumulatedProbabilities[j])
				j++;
		}
        randomFluctuation=mParticles[j].getState()->copy();
        randomFluctuation->applyRandomFluctuation(sigmas,mGenerator);
        //randomFluctuation=mParticles[j].getState().getRandomFieldFluctuation(sigmas,mGenerator,directions,0);
		newParticles[i].setState(randomFluctuation);
	}
    mParticles.clear();
    mParticles=newParticles;

}
/*
void ParticleFilter::modifyWithDirection()
{
	uniform_real_distribution<double> uniformDistribution(0.0,1.0);

	vector<Direction> directions;
	directions.push_back(Direction());
	directions.push_back(Direction());
	vector<double> dir;
	dir.push_back(10);
	dir.push_back(0);
	directions[0].mD=dir;
	dir[0]=0;
	dir[1]=10;
	directions[1].mD=dir;

	for (int k=0; k<(int)directions.size(); k++)
	{
		for (int i=nParticles*(1.0-mTarget.mModelForce); i<nParticles*(1.0-mTarget.mModelForce*(1.0-(1.0+k)/directions.size())); i++)
		{
            shared_ptr<State> modified=mParticles[i].getState()->modifyWithDirection(directions[k]);
			mParticles[i].setState(modified);
		}
	}
}*/
shared_ptr<State> ParticleFilter::evaluate(const shared_ptr<State> state)
{
	prediction();
    //modifyWithDirection();
	observation(state);
	resample();
	return correction();
}
int ParticleFilter::getParticlesNumber()
{
	return nParticles;
}
Particle ParticleFilter::getParticle(int number)
{
	return mParticles[number];
}
vector<Particle> ParticleFilter::getParticles()
{
	return mParticles;
}
void ParticleFilter::setValidity(int numDirections)
{
	double totalSum=0;
	double oneDirectionSum;
	double maxOneDirectionSum=0;
	for (int j=0; j<nParticles; j++)
	{
		totalSum+=mParticles[j].getWeight();
	}
	for (int k=0; k<numDirections; k++)
	{
		oneDirectionSum=0;
		for (int j=nParticles*(1.0-mTarget.mModelForce*(1-double(k)/numDirections)); j<nParticles*(1.0-mTarget.mModelForce*(1-double(k+1)/numDirections)); j++)
		{
			oneDirectionSum+=mParticles[j].getWeight();
		}
		maxOneDirectionSum=max(maxOneDirectionSum,oneDirectionSum);
	}
	if (mTarget.mModelForce!=0&&totalSum!=0)
	{
		//mTarget.setValidity(min(1.0,maxOneDirectionSum*numDirections/(totalSum*mTarget.mModelForce)));
		//mTarget.setValidity(maxOneDirectionSum/totalSum);
		mTarget.setValidity(maxOneDirectionSum*numDirections/(totalSum*mTarget.mModelForce));
	}
	else
		mTarget.setValidity(0);
	//cout<<"Val "<<mTarget.getValidity()<<endl;
}
void ParticleFilter::printParticles(ostream& out)
{
	for (int i=0; i<nParticles; i++)
	{
		out << mParticles[i];
	}
	out << "\n";
}
