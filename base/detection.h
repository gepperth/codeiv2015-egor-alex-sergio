#ifndef DETECTION_H_
#define DETECTION_H_

#include "state.h"

using namespace std;

class Detection
{
private:
    shared_ptr<State> mState;
	double mScore;
	int mID;
 public:

    Detection ();
    Detection(const shared_ptr<State> state, double score);
    ~Detection();
    Detection operator=(const Detection i);
    void setState(shared_ptr<State> state);
    shared_ptr<State> getState() const;
    void setID(int id);
    int getID() const;
    void setScore(double score);
    double getScore() const;
    friend std::ostream& operator<< (std::ostream& out, const Detection& detection);
};


#endif /* DETECTION_H_ */
