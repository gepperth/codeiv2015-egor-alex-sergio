#ifndef PHDFILTER_H
#define PHDFILTER_H

#include <vector>
#include <random>
#include <chrono>
#include <cfloat>
#include <fstream>
#include <algorithm>
#include "state.h"
#include "detection.h"
#include "particle.h"
#include "field.h"

using namespace std;

class PHDFilter
{
private:
	vector<vector<Particle> > mParticles;

	int targetHistoryLength;
	int nParticlesPerObject;
	vector<Direction> mDirections;

	default_random_engine mGenerator;
    int idNum;
public:
    vector<Target*> mTargets;
	double mPfn;
	double mPb;
	double mPd;
    double mPdeltaPlus,mPdeltaMinus ;


public:

	VectorField field;
	double mAssociationCoef;
	double mResampleCoef;
	double mInfluenceCoef;
	vector<double> mSigmas;
public:
	PHDFilter();
	~PHDFilter();
	void resample();
	void initialize(int num_particles,
					 const vector<double>& sigmas,
					 double pfn,
                     double assocC,
                     double resampC,
                     double inflC,
					 int historyLength,
					 const string& fieldName);
	void prediction();
	void association(const vector<Detection>& detections, ostream& out);
	void merge(ostream& out);
	void observation(const vector<Detection>& detections);
	void modifyWithDirection();
	vector<Target> correction();
    vector<Target> evaluate(const vector<Detection>& detections,vector<vector<Particle> >& weightedParticles, ostream& associations, ostream& unions);
	int getTargetsNumber();
    double getTargetProbability(int i) ;
	int getParticlesNumberInTarget(int numTarget);
	Particle getParticle(int numTarget, int numParticle);
	vector<vector<Particle> > getParticles();
	void oneResample(const vector<Particle>& oldParticles, int numTarget, const vector<double>& cumulatedProbabilities, double sum);
	void getCumulatedProbability(int numTarget, vector<double>& cumulatedProbabilities, double& sum);
	void setValidity(int i, int numDirections);
	int randomFieldFluctuation(int i, int numTarget);
	void printParticles(ostream& out);
};






#endif /* PHDFILTER_H_ */
