#include "mcmcda.h"


MCMCDA::MCMCDA()
{
}
MCMCDA::~MCMCDA()
{
}
void MCMCDA::evaluate(vector<Detection> detections, int nFrame)
{

	if (mW.size()<1)
	{
		mW.push_back(detections);
	}
	else
	{
		mW[0].insert( mW[0].end(), detections.begin(), detections.end() );
	}
	mWnew=mW;
	nt=detections.size();
	int nmc=1000;//1000;//100;
	int m;
	bool yes=true;
	srand (time(NULL));
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	generator.seed(seed);
	//default_random_engine generator(seed);
	uniform_real_distribution<double> uniformDistribution(0.0,1.0);

	//uniform_int_distribution<> tDistribution(0,nFrame);
	uniform_int_distribution<> mDistribution(0,7);


	for (int i=0; i < nmc; i++)
	{
		m=mDistribution(generator);//rand() % 2;//8;
		//if (nFrame<7) m=1;
		//if (nFrame>=7) m=0; if (nFrame==9) m=7;
		yes=false;
		switch (m)
		{
			case 0: yes=birth(nFrame); break;
			case 1: yes=death(nFrame); break;
			case 2: yes=split(nFrame); break;
			case 3: yes=merge(nFrame); break;
			case 4: yes=extention(nFrame); break;
			case 5: yes=reduction(nFrame); break;
			case 6: yes=update(nFrame); break;
			case 7: yes=switchf(nFrame); break;
			default: cout<<"ERROR, wrong random function"<<endl; break;
		}
		if (yes)
		{
			//cout<<"YES"<<endl;

			double tmp=uniformDistribution(generator);
			if (pAccepted(nFrame)>tmp)
			{
				mW.clear();
				mW=mWnew;
			}
		}
	}

	mWnew.clear();


}
bool MCMCDA::birth(int nFrame)
{
	cout<<"BIRTH"<<endl;
	//cout<<"B";
	srand (time(NULL));
	if (nFrame<2) return false;
	int dmax=1;
	uniform_int_distribution<> dDistribution(1,dmax);
	int d=dDistribution(generator);
	//double gamma=0.1;
	uniform_real_distribution<double> uniformDistribution(0.0,1.0);
	uniform_int_distribution<> tDistribution(0,nFrame-1);
	double v=200.0;
	int t=tDistribution(generator);

	vector<int> indexes;

	vector<Detection> Wzero=mWnew[0];

	for (unsigned int j=0; j<Wzero.size(); j++)
	{
		if (Wzero[j].getScore()==t )
		{
			bool isPresent=false;
			for (unsigned int i=0; i<Wzero.size(); i++)
			{
                /*
                State substr=Wzero[j].getState();            //state class changed
                substr.sub(Wzero[i].getState());
				if ( Wzero[i].getScore()==t+d &&
                        (substr).getEuclidean()<v*d)
				{
					isPresent=true;
					i=Wzero.size();
				}
                */
			}
			if (isPresent)
				indexes.push_back(j);
		}
	}
	if (indexes.size()==0)
		return false;


	uniform_int_distribution<> k1Distribution(0,indexes.size()-1);
	int k1=k1Distribution(generator);
	vector<Detection> wK;
	wK.push_back(Wzero[indexes[k1]]);

	Wzero.erase (Wzero.begin()+indexes[k1]);

	bool isContinue=true;
	while (isContinue)
	{
		indexes.clear();
		for (unsigned int i=0; i<Wzero.size(); i++)
		{
            /*
            State substr=wK.back().getState();      state class changed
            substr.sub(Wzero[i].getState());
			if ( Wzero[i].getScore()==t+d &&
                        (substr).getEuclidean()<v*d)
                indexes.push_back(i);*/
		}
		if (indexes.size()!=0)
		{
			uniform_int_distribution<> k1Distribution(0,indexes.size()-1);
			int k1=k1Distribution(generator);
			wK.push_back(Wzero[indexes[k1]]);

			Wzero.erase (Wzero.begin()+indexes[k1]);
		}
		else isContinue=false;

		t=t+d;
		d=dDistribution(generator);
		//isContinue=(gamma<uniformDistribution(generator));
	}
	if (wK.size()<2)
	{
		return false;
	}
	else
	{

		mWnew[0]=Wzero;
		mWnew.push_back(wK);

		return true;
	}

}
bool MCMCDA::death(int nFrame)
{
	cout<<"DEATH"<<endl;
	uniform_real_distribution<double> uniformDistribution(0.0,1.0);
	//if (0.01<uniformDistribution(generator))
	//	return false;
	if (mWnew.size()<2)
		return false;

	uniform_int_distribution<> dDistribution(1,mWnew.size()-1);
	int k=dDistribution(generator);
	mWnew[0].insert( mWnew[0].end(), mWnew[k].begin(), mWnew[k].end() );
	mWnew.erase (mWnew.begin()+k);
	return true;
}
bool MCMCDA::split(int nFrame)
{
	cout<<"SPLIT"<<endl;
	vector<int> indexes;

	for (unsigned int i=1; i<mWnew.size(); i++)
	{
		if (mWnew[i].size()>=4)
			indexes.push_back(i);
	}
	if (indexes.size()==0) return false;
	uniform_int_distribution<> sDistribution(0,indexes.size()-1);
	int ind=sDistribution(generator);
	uniform_int_distribution<> wDistribution(2,mWnew[indexes[ind]].size()-2);
	int separator=wDistribution(generator);
	vector<Detection> newW(mWnew[indexes[ind]].begin()+separator, mWnew[indexes[ind]].end());
	mWnew.push_back(newW);
	mWnew[indexes[ind]].erase(mWnew[indexes[ind]].begin()+separator,mWnew[indexes[ind]].end());

	return true;
}
bool MCMCDA::merge(int nFrame)
{
	cout<<"MERGE"<<endl;
	if (mWnew.size()<3) return false;
	int dmax=1;
	double v=200.0;
	vector<int> indexesi;
	vector<int> indexesj;
	for (unsigned int i=1; i<mWnew.size(); i++)
	{
		for (unsigned int j=i+1; j<mWnew.size(); j++)
		{
            /*
            State substr=mWnew[i].back().getState();            state chass changed
            substr.sub(mWnew[j].begin()->getState());
            if (
					mWnew[j].begin()->getScore()<=mWnew[i].back().getScore()+dmax &&
					mWnew[j].begin()->getScore()>mWnew[i].back().getScore() &&
                    substr.getEuclidean()<v*abs(mWnew[i].back().getScore()-mWnew[j].begin()->getScore()))
			{
				indexesi.push_back(i);
				indexesj.push_back(j);
				j=mWnew.size()-1;
			}
            */
		}
	}
	uniform_int_distribution<> sDistribution(0,indexesi.size()-1);
	int ind=sDistribution(generator);
	if (indexesi.size()==0)
		return false;
	else
	{
		mWnew[indexesi[ind]].insert( mWnew[indexesi[ind]].end(), mWnew[indexesj[ind]].begin(), mWnew[indexesj[ind]].end() );
		mWnew.erase(mWnew.begin()+indexesj[ind]);

		return true;
	}
}
bool MCMCDA::extention(int nFrame)
{

	cout<<"EXTENTION"<<endl;
	if (mWnew.size()<2)
		return false;
	uniform_int_distribution<> kDistribution(1,mWnew.size()-1);
	int k=kDistribution(generator);

	int dmax=1;
	uniform_int_distribution<> dDistribution(1,dmax);
	int d=dDistribution(generator);
	bool isContinue=true;
	int t=0;
	int k1;
	vector<int> indexes;
	double v=200.0;

	t=mWnew[k].back().getScore();
	while (isContinue)
	{
		indexes.clear();
		for (unsigned int i=0; i<mWnew[0].size(); i++)
		{
            /*
            State substr=mWnew[k].back().getState();    class state changed
            substr.sub(mWnew[0][i].getState());
            if ( mWnew[0][i].getScore()==t+d &&
                        substr.getEuclidean()<v*d)
				indexes.push_back(i);
                */
		}
		if (indexes.size()!=0)
		{
			uniform_int_distribution<> k1Distribution(0,indexes.size()-1);
			k1=k1Distribution(generator);
			mWnew[k].push_back(mWnew[0][indexes[k1]]);
			mWnew[0].erase (mWnew[0].begin()+indexes[k1]);
		}
		else isContinue=false;

		t=t+d;
		d=dDistribution(generator);
		//isContinue=(gamma<uniformDistribution(generator));
	}

	return true;
}
bool MCMCDA::reduction(int nFrame)
{
	cout<<"REDUCTION"<<endl;
	if (mWnew.size()<2)
		return false;
	uniform_int_distribution<> kDistribution(1,mWnew.size()-1);
	int k=kDistribution(generator);
	if (mWnew[k].size()<3)
		return false;
	uniform_int_distribution<> wDistribution(2,mWnew[k].size()-1);
	int separator=wDistribution(generator);

	mWnew[0].insert( mWnew[0].end(), mWnew[k].begin()+separator, mWnew[k].end() );
	mWnew[k].erase(mWnew[k].begin()+separator,mWnew[k].end());

	return true;

}
bool MCMCDA::update(int nFrame)
{
	cout<<"UPDATE"<<endl;
	if (mWnew.size()<2) return false;


	uniform_int_distribution<> kDistribution(1,mWnew.size()-1);
	int k=kDistribution(generator);
	uniform_int_distribution<> wDistribution(1,mWnew[k].size()-1);
	int begin=wDistribution(generator);

	vector<Detection> Wzero=mWnew[0];
	vector<Detection> Wk=mWnew[k];

	Wzero.insert( Wzero.end(), Wk.begin()+begin, Wk.end() );
	Wk.erase(Wk.begin()+begin,Wk.end());

	int k1;
	double v=200.0;
	int dmax=1;
	uniform_int_distribution<> dDistribution(1,dmax);
	int d=dDistribution(generator);
	int t=0;
	vector<int> indexes;

	t=Wk.back().getScore();
	bool isContinue=true;

	while (isContinue)
	{
		indexes.clear();
		for (unsigned int i=0; i<Wzero.size(); i++)
		{
            /*
            State substr=Wk.back().getState();      state class changed
            substr.sub(Wzero[i].getState());
            if ( Wzero[i].getScore()==t+d &&
                        substr.getEuclidean()<v*d)
				indexes.push_back(i);
                */
		}

		if (indexes.size()!=0)
		{
			uniform_int_distribution<> k1Distribution(0,indexes.size()-1);
			k1=k1Distribution(generator);
			Wk.push_back(Wzero[indexes[k1]]);
			Wzero.erase (Wzero.begin()+indexes[k1]);
		}
		else isContinue=false;

		t=t+d;
		d=dDistribution(generator);
		//isContinue=(gamma<uniformDistribution(generator));
	}


	if (Wk.size()<2)
		return false;
	else
	{
		mWnew[k]=Wk;
		mWnew[0]=Wzero;
		return true;
	}

}
bool MCMCDA::switchf(int nFrame)
{
	cout<<"SWITCH"<<endl;
	if (mWnew.size()<3) return false;
	int dmax=1;
	double v=200.0;
	vector<int> indexesi;
	vector<int> indexesj;
	vector<int> indexesii;
	vector<int> indexesjj;
	for (unsigned int i=1; i<mWnew.size(); i++)
	{
		for (unsigned int j=i+1; j<mWnew.size(); j++)
		{
			for (unsigned int ii=1; ii<mWnew[i].size()-1; ii++)
			{
				for (unsigned int jj=1; jj<mWnew[j].size()-1; jj++)
				{
                    /*
                    State substr=mWnew[i][ii].getState();                   state class changed
                    substr.sub(mWnew[j][jj].getState());
                    if (
							mWnew[j][jj].getScore()<=mWnew[i][ii].getScore()+dmax &&
							mWnew[j][jj].getScore()>mWnew[i][ii].getScore() &&
                            substr.getEuclidean()<v*abs(mWnew[i][ii].getScore()-mWnew[j][jj].getScore()))
					{
						indexesi.push_back(i);
						indexesj.push_back(j);
						indexesii.push_back(ii);
						indexesjj.push_back(jj);

					}
                    */
				}
			}
		}
	}
	if (indexesi.size()==0)
		return false;
	else
	{
		uniform_int_distribution<> sDistribution(0,indexesi.size()-1);
		int ind=sDistribution(generator);
		vector<Detection> tmpVec(mWnew[indexesj[ind]].begin()+indexesjj[ind],mWnew[indexesj[ind]].end());

		mWnew[indexesj[ind]].erase(mWnew[indexesj[ind]].begin()+indexesjj[ind],mWnew[indexesj[ind]].end());
		mWnew[indexesj[ind]].insert( mWnew[indexesj[ind]].end(), mWnew[indexesi[ind]].begin()+indexesii[ind], mWnew[indexesi[ind]].end() );

		mWnew[indexesi[ind]].erase(mWnew[indexesi[ind]].begin()+indexesii[ind],mWnew[indexesi[ind]].end());
		mWnew[indexesi[ind]].insert( mWnew[indexesi[ind]].end(), tmpVec.begin(), tmpVec.end() );

		return true;
	}
}
double MCMCDA::pAccepted(int nFrame)
{
	double pW=P(mW, nFrame);
	double pWnew=P(mWnew, nFrame);
	double qWnew=Q(mW);//1.0;
	double qW=Q(mWnew);//1.0;
	cout<<"ZERO "<<(pWnew*qWnew)/(pW*qW)<<endl;
	return min(1.0,(pWnew*qWnew)/(pW*qW));
}
double MCMCDA::P(vector<vector<Detection> >& W, int nFrame)
{
	double pWY=1.0;
	double lambdaF=0.1, lambdaB=0.05, pz=0.1, pd=0.1;
	for (int nF=0; nF<maxFrame; nF++)
	{
		int zt=0,at=0,et=0,ct,ft,gt,dt;

		for (unsigned int i=1; i<W.size(); i++)
		{
			for (unsigned int j=0; j<W[i].size(); j++)
			{
				if (W[i][j].getScore()==nFrame-1)
				{
					et++;
					if (j==W[i].size()-1)
						zt++;
				}

			}
			if (W[i][0].getScore()==nFrame)
			{
				at++;
			}
		}
		//cout<<"et,at,zt "<<et<<" "<<at<<" "<<zt<<endl;

		//cout<<"nt "<<nt<<endl;
		///dt??????
		dt=(ct+at+nt)/2;
		///
		ct=et-zt;
		gt=ct+at-dt;
		ft=nt-dt;

		pWY*=pow(pz,zt)*pow((1-pz),zt)*pow(pd,dt)*pow((1-pd),gt)*pow(lambdaB,at)*pow(lambdaF,ft);
	}

	return pWY;
}
double MCMCDA::Q(vector<vector<Detection> >& W)
{
	vector<double> sigmas;
	sigmas.push_back(40);
	sigmas.push_back(40);

	double result=1;
	/*
	for (unsigned int i=1; i<W.size(); i++)
	{
		ParticleFilter particleFilter;

		particleFilter.initialize(W[i][0].getState(),30,15,sigmas,used);

		for (unsigned int j=1; j<W[i].size(); j++)
		{
			State state=particleFilter.evaluate(W[i][j].getState());
			result*=W[i][j].getState().getGaussian(state,sigmas,used);
		}
	}*/
	for (unsigned int i=1; i<W.size(); i++)
	{
		Kalman kalmanFilter;

		kalmanFilter.reset();

		for (unsigned int j=0; j<W[i].size(); j++)
		{
            //State state=kalmanFilter.evaluate(W[i][j].getState());            state class changed
            //result*=W[i][j].getState().getGaussian(state,sigmas,true);//,true,true);
		}
	}


	return result;
}




