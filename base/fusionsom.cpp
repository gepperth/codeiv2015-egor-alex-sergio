#include "fusionsom.h"

fusionSOM::fusionSOM()
{
    int sizeCam=30;
    int sizeLas=20;
    isTracklets=false;
    somCam.init(sizeCam,sizeCam,2,  {   {0,0},      {sizeLas,sizeLas},    {1,5}});
    somLas.init(sizeLas,sizeLas,2,  {{sizeCam,sizeCam},      {0,0},       {1,5}});
    somRectSize.init(1,5, 2,  {{sizeCam,sizeCam},   {sizeLas,sizeLas},    {0,0}});
    //========Tracklets=================================================
    if (isTracklets)
    {
        filereader::trackletsToSom("/home/egor/gitStorage/data/tracklets",data);
    }
    //========Tracklets==end============================================

    //========Real======================================================
    else
    {
        filereader::camToSom("/home/egor/dataForTracking/out/",camData);
        filereader::lasToSom("/home/egor/dataForTracking/outLas/",lasData);
        filereader::camLasToSom("/home/egor/dataForTracking/outTrue/",trueData);
        filereader::camLasToSom("/home/egor/dataForTracking/outFalse/",falseData);
    }
    //========Real==end=================================================

}

fusionSOM::~fusionSOM()
{

}
void fusionSOM::associateEval(int nTimes)
{
    vector<vector<SomPoint> > small, big;

    //========Tracklets=================================================
    if (isTracklets)
    {
        divideData(data,big,small);
        data=big;

        camData=big;
        lasData=big;
    }
    //========Tracklets==end============================================
    data.clear();
    associateLearn(nTimes);

    desassociate();
    somCam.normalize();
    somLas.normalize();
    somRectSize.normalize();

    //========Tracklets=================================================
    if (isTracklets)
    {
        data=small;
    }
    //========Tracklets==end============================================

    //========Real======================================================
    if (!isTracklets)
    {
        data=lasData;
        transformData(data);
    }

    //========Real==end=================================================

    //falsePosNegCalc2();
    //falsePosNegCalc3();
    falsePosNegCalc4();
    //falsePosNegCalc5();

}

void fusionSOM::associateLearn(int maxK)
{
    default_random_engine generator;
    uniform_int_distribution<> distrN(0,camData.size()-1);
    double camX, camY, lasX, lasY, sizeX, sizeY;
    int camI, camJ, lasI, lasJ, sizeI, sizeJ;
    int camH, lasH, sizeH;


    camH=somCam.mH;
    lasH=somLas.mH;
    sizeH=somRectSize.mH;
    int currentLimit=1000;
    int step=1000;
    int numIt=0;

    QFile fileLogTrue("/home/egor/soms/logTrue.txt");
    QFile fileLogFalse("/home/egor/soms/logFalse.txt");
    QFile fileLogBin("/home/egor/soms/logBin.txt");
    fileLogTrue.open(QIODevice::WriteOnly | QIODevice::Text);
    fileLogFalse.open(QIODevice::WriteOnly | QIODevice::Text);
    fileLogBin.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream logTrue(&fileLogTrue);
    QTextStream logFalse(&fileLogFalse);
    QTextStream logBin(&fileLogBin);



    for (int k=0; k<maxK; k++)
    {
        if (k>=currentLimit)
        {
            cout<<currentLimit<<endl;
            currentLimit+=step;
        }
        int numFrame=distrN(generator);
        for (unsigned int i=0; i<camData[numFrame].size(); i++)
        {
            camX=camData[numFrame][i].camX;
            camY=camData[numFrame][i].camY;
            somCam.findTheNearest({camX,camY},camI,camJ);

            sizeX=camData[numFrame][i].camW;
            sizeY=camData[numFrame][i].camH;
            somRectSize.findTheNearest({sizeX,sizeY},sizeI,sizeJ);

            somCam.mNodes[camI*camH+camJ].addWeights(2,sizeI,sizeJ);
            somRectSize.mNodes[sizeI*sizeH+sizeJ].addWeights(0,camI,camJ);

            for (unsigned int j=0; j<lasData[numFrame].size(); j++)
            {
                lasX=lasData[numFrame][j].lasX;
                lasY=lasData[numFrame][j].lasY;
                somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                numIt++;

                somCam.mNodes[camI*camH+camJ].addWeights(1,lasI,lasJ);
                somLas.mNodes[lasI*lasH+lasJ].addWeights(0,camI,camJ);

                somLas.mNodes[lasI*lasH+lasJ].addWeights(2,sizeI,sizeJ);
                somRectSize.mNodes[sizeI*sizeH+sizeJ].addWeights(1,lasI,lasJ);
            } 
        }
    }



    fileLogTrue.close();
    fileLogFalse.close();
    fileLogBin.close();
}


void fusionSOM::falsePosNegCalc2()
{
    double camX, camY, lasX, lasY;
    //double sizeW,sizeH;
    int camI, camJ, lasI, lasJ;
    int camH,  lasH;//,lasW,camW;
    double truth;
    int totalnum;
    int fnCam, fpCam, tpCam, tnCam;
    int fnLas, fpLas, tpLas, tnLas;
    int fnBoth, fpBoth, tpBoth, tnBoth;
    double currentLimit=0.01;
    double step=0.01;
    camH=somCam.mH;
    lasH=somLas.mH;
    QFile filefpfn("/home/egor/soms/fnfp2.txt");
    filefpfn.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream fpfn(&filefpfn);
    double fracMax=0.151;
    double fracStep=0.01;//0.001;
    for (double frac=0.0; frac<=fracMax; frac+=fracStep ) //0.4
    {
        fpCam=0;
        fnCam=0;
        tpCam=0;
        tnCam=0;

        fpLas=0;
        fnLas=0;
        tpLas=0;
        tnLas=0;

        fpBoth=0;
        fnBoth=0;
        tpBoth=0;
        tnBoth=0;

        totalnum=0;

        if (frac>=currentLimit)
        {
            cout<<currentLimit<<endl;
            currentLimit+=step;
        }
        for (unsigned int k=0; k<data.size(); k++)
        {
            for (unsigned int i=0; i<data[k].size(); i++)
            {

                camX=data[k][i].camX;
                camY=data[k][i].camY;
                somCam.findTheNearest({camX,camY},camI,camJ);

                for (unsigned int j=0; j<data[k].size(); j++)
                {
                    lasX=data[k][j].lasX;
                    lasY=data[k][j].lasY;
                    //sizeW=data[k][j].camW;
                    //sizeH=data[k][j].camH;
                    somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                    truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);
                    //truth=somCam.getWeight3(camI*camH+camJ,lasI*lasH+lasJ,1);

                    if (truth>=frac)
                    {
                        if (i==j)
                            tpCam++;
                        else
                            fpCam++;
                    }
                    else
                    {
                        if (i==j)
                            fnCam++;
                        else
                            tnCam++;
                    }

                    truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);
                    //truth=somLas.getWeight3(lasI*lasH+lasJ,camI*camH+camJ,0);


                    if (truth>=frac)
                    {
                        if (i==j)
                            tpLas++;
                        else
                            fpLas++;
                    }
                    else
                    {
                        if (i==j)
                            fnLas++;
                        else
                            tnLas++;
                    }

                    //truth=somCam.getWeight(camI*camH+camJ,lasI*lasH+lasJ)*somLas.getWeight(lasI*lasH+lasJ,camI*camH+camJ);

                    truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;
                    //truth=(somCam.getWeight3(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight3(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;


                    if (truth>=frac)//*0.005)
                    {
                        if (i==j)
                            tpBoth++;
                        else
                            fpBoth++;
                    }
                    else
                    {
                        if (i==j)
                            fnBoth++;
                        else
                            tnBoth++;
                    }
                    totalnum++;
                }

            }
        }
        fpfn<<frac<<" "<<fpCam<<" "<<fnCam<<" "<<tpCam<<" "<<tnCam<<" "<<fpLas<<" "<<fnLas<<" "<<tpLas<<" "<<tnLas<<" "<<fpBoth<<" "<<fnBoth<<" "<<tpBoth<<" "<<tnBoth<<" "<<totalnum<<endl;
    }
    filefpfn.close();
}


void fusionSOM::falsePosNegCalc5()
{
    double camX, camY, lasX, lasY;
    //double sizeW,sizeH;
    int camI, camJ, lasI, lasJ;
    int camH,  lasH;//,lasW,camW;
    double truth;
    int totalnum;
    int fnCam, fpCam, tpCam, tnCam;
    int fnLas, fpLas, tpLas, tnLas;
    int fnBoth, fpBoth, tpBoth, tnBoth;
    double currentLimit=0.01;
    double step=0.01;
    camH=somCam.mH;
    lasH=somLas.mH;
    int numCamNew, numLasNew;
    QFile filefpfn("/home/egor/soms/fnfp2.txt");
    filefpfn.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream fpfn(&filefpfn);
    double fracMax=0.151;
    double fracStep=0.01;//0.001;
    for (double frac=0.0; frac<=fracMax; frac+=fracStep ) //0.4
    {
        fpCam=0;
        fnCam=0;
        tpCam=0;
        tnCam=0;

        fpLas=0;
        fnLas=0;
        tpLas=0;
        tnLas=0;

        fpBoth=0;
        fnBoth=0;
        tpBoth=0;
        tnBoth=0;

        totalnum=0;

        if (frac>=currentLimit)
        {
            cout<<currentLimit<<endl;
            currentLimit+=step;
        }
        for (unsigned int k=0; k<data.size(); k++)
        {
            for (unsigned int i=0; i<data[k].size(); i++)
            {

                camX=data[k][i].camX;
                camY=data[k][i].camY;
                somCam.findTheNearest({camX,camY},camI,camJ);

                for (unsigned int j=0; j<data[k].size(); j++)
                {
                    lasX=data[k][j].lasX;
                    lasY=data[k][j].lasY;
                    //sizeW=data[k][j].camW;
                    //sizeH=data[k][j].camH;
                    somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                    //truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);
                    truth=somCam.getWeight4(camI*camH+camJ,lasI*lasH+lasJ,1,numLasNew);

                    if (truth>=frac)
                    {
                        if (i==j)
                            tpCam++;
                        else
                            fpCam++;
                    }
                    else
                    {
                        if (i==j)
                            fnCam++;
                        else
                            tnCam++;
                    }

                    //truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);
                    //truth=somLas.getWeight3(lasI*lasH+lasJ,camI*camH+camJ,0);
                    truth=somLas.getWeight4(lasI*lasH+lasJ,camI*camH+camJ,0,numCamNew);

                    if (truth>=frac)
                    {
                        if (i==j)
                            tpLas++;
                        else
                            fpLas++;
                    }
                    else
                    {
                        if (i==j)
                            fnLas++;
                        else
                            tnLas++;
                    }

                    //truth=somCam.getWeight(camI*camH+camJ,lasI*lasH+lasJ)*somLas.getWeight(lasI*lasH+lasJ,camI*camH+camJ);
                    truth=approaching(camI*camH+camJ, lasI*lasH+lasJ, numCamNew, numLasNew);
                    //truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;
                    //truth=(somCam.getWeight3(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight3(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;


                    if (truth>=frac)//*0.005)
                    {
                        if (i==j)
                            tpBoth++;
                        else
                            fpBoth++;
                    }
                    else
                    {
                        if (i==j)
                            fnBoth++;
                        else
                            tnBoth++;
                    }
                    totalnum++;
                }

            }
        }
        fpfn<<frac<<" "<<fpCam<<" "<<fnCam<<" "<<tpCam<<" "<<tnCam<<" "<<fpLas<<" "<<fnLas<<" "<<tpLas<<" "<<tnLas<<" "<<fpBoth<<" "<<fnBoth<<" "<<tpBoth<<" "<<tnBoth<<" "<<totalnum<<endl;
    }
    filefpfn.close();
}



void fusionSOM::falsePosNegCalc3()
{
    double camX, camY, lasX, lasY;
    //double sizeW,sizeH;
    int camI, camJ, lasI, lasJ;
    int camH,  lasH,camW;//,lasW,;
    double truth;
    int totalnum;
    int fnCam, fpCam, tpCam, tnCam;
    int fnLas, fpLas, tpLas, tnLas;
    int fnBoth, fpBoth, tpBoth, tnBoth;
    double currentLimit=0.01;
    double step=0.01;
    camW=somCam.mW;
    camH=somCam.mH;
    //lasW=somLas.mW;
    lasH=somLas.mH;
    QFile filefpfn("/home/egor/soms/fnfp2.txt");
    filefpfn.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream fpfn(&filefpfn);
    double fracMax=0.151;
    double fracStep=0.01;//0.001;
    for (double frac=0.0; frac<=fracMax; frac+=fracStep ) //0.4
    {
        fpCam=0;
        fnCam=0;
        tpCam=0;
        tnCam=0;

        fpLas=0;
        fnLas=0;
        tpLas=0;
        tnLas=0;

        fpBoth=0;
        fnBoth=0;
        tpBoth=0;
        tnBoth=0;

        totalnum=0;

        if (frac>=currentLimit)
        {
            cout<<currentLimit<<endl;
            currentLimit+=step;
        }
        for (unsigned int k=0; k<data.size(); k++)
        {
            for (unsigned int i=0; i<data[k].size(); i++)
            {

                camX=data[k][i].camX;
                camY=data[k][i].camY;
                somCam.findTheNearest({camX,camY},camI,camJ);

                for (unsigned int j=0; j<data[k].size(); j++)
                {
                    lasX=data[k][j].lasX;
                    lasY=data[k][j].lasY;

                    somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                    //==============================================

                    double ri=somCam.getMaxIndex(camI*camH+camJ,2);
                    double rW=somRectSize.mNodes[ri].coords[0];
                    double rH=somRectSize.mNodes[ri].coords[1];

                    double max=-1;
                    int indmax=0;
                    for (int ii=0; ii<camW; ii++)
                    {
                        for (int jj=0; jj<camH; jj++)
                        {
                            if (    abs(camX-somCam.mNodes[camH*ii+jj].coords[0])<rW &&
                                    abs(camY-somCam.mNodes[camH*ii+jj].coords[1])<rH)
                            {
                                double candidate=somLas.getWeight2(lasI*lasH+lasJ, camH*ii+jj, 0);
                                if (candidate>max)
                                {
                                    indmax=camH*ii+jj;
                                    max=candidate;
                                }
                            }
                        }
                    }



                    //==============================================
                    truth=max;
                    //truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);

                    if (truth>=frac)
                    {
                        if (i==j)
                            tpLas++;
                        else
                            fpLas++;
                    }
                    else
                    {
                        if (i==j)
                            fnLas++;
                        else
                            tnLas++;
                    }



                    truth=somCam.getWeight2(indmax,lasI*lasH+lasJ,1);
                    //truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);

                    if (truth>=frac)
                    {
                        if (i==j)
                            tpCam++;
                        else
                            fpCam++;
                    }
                    else
                    {
                        if (i==j)
                            fnCam++;
                        else
                            tnCam++;
                    }



                    //truth=somCam.getWeight(camI*camH+camJ,lasI*lasH+lasJ)*somLas.getWeight(lasI*lasH+lasJ,camI*camH+camJ);

                    //truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;

                    truth=(somCam.getWeight2(indmax,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,indmax,0))/2.0;


                    if (truth>=frac)//*0.005)
                    {
                        if (i==j)
                            tpBoth++;
                        else
                            fpBoth++;
                    }
                    else
                    {
                        if (i==j)
                            fnBoth++;
                        else
                            tnBoth++;
                    }
                    totalnum++;
                }

            }
        }
        fpfn<<frac<<" "<<fpCam<<" "<<fnCam<<" "<<tpCam<<" "<<tnCam<<" "<<fpLas<<" "<<fnLas<<" "<<tpLas<<" "<<tnLas<<" "<<fpBoth<<" "<<fnBoth<<" "<<tpBoth<<" "<<tnBoth<<" "<<totalnum<<endl;
    }
    filefpfn.close();
}


void fusionSOM::falsePosNegCalc4()
{
    double camX, camY, lasX, lasY;
    //double sizeW,sizeH;
    int camI, camJ, lasI, lasJ;
    int camH,  lasH;//,lasW,camW;
    double truth;
    int totalnum;
    int fnCam, fpCam, tpCam, tnCam;
    int fnLas, fpLas, tpLas, tnLas;
    int fnBoth, fpBoth, tpBoth, tnBoth;

    camH=somCam.mH;
    lasH=somLas.mH;
    QFile filefpfn("/home/egor/soms/fnfp2.txt");
    filefpfn.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream fpfn(&filefpfn);
    double currentLimit=0.01;
    double step=0.01;
    double fracMax=0.41;//0.21
    double fracStep=0.001;//0.001;
    for (double frac=0.0; frac<=fracMax; frac+=fracStep ) //0.4
    {
        fpCam=0;
        fnCam=0;
        tpCam=0;
        tnCam=0;

        fpLas=0;
        fnLas=0;
        tpLas=0;
        tnLas=0;

        fpBoth=0;
        fnBoth=0;
        tpBoth=0;
        tnBoth=0;

        totalnum=0;

        if (frac>=currentLimit)
        {
            cout<<currentLimit<<endl;
            currentLimit+=step;
        }
        for (unsigned int k=0; k<trueData.size(); k++)
        {

            for (unsigned int i=0; i<trueData[k].size(); i++)
            {

                camX=trueData[k][i].camX;
                camY=trueData[k][i].camY;
                somCam.findTheNearest({camX,camY},camI,camJ);

                lasX=trueData[k][i].lasX;
                lasY=trueData[k][i].lasY;
                somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);

                if (truth>=frac)
                {
                    tpCam++;
                }
                else
                {
                    fnCam++;
                }

                truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);

                if (truth>=frac)
                {
                    tpLas++;
                }
                else
                {
                    fnLas++;
                }

                truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;


                if (truth>=frac)//*0.005)
                {
                    tpBoth++;
                }
                else
                {
                    fnBoth++;
                }
                totalnum++;
            }
        }

        for (unsigned int k=0; k<falseData.size(); k++)
        {

            for (unsigned int i=0; i<falseData[k].size(); i++)
            {

                camX=falseData[k][i].camX;
                camY=falseData[k][i].camY;
                somCam.findTheNearest({camX,camY},camI,camJ);

                lasX=falseData[k][i].lasX;
                lasY=falseData[k][i].lasY;
                somLas.findTheNearest({lasX,lasY},lasI,lasJ);

                truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);

                if (truth>=frac)
                {
                    fpCam++;
                }
                else
                {
                    tnCam++;
                }

                truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);

                if (truth>=frac)
                {
                    fpLas++;
                }
                else
                {
                    tnLas++;
                }

                //truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;
                truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)*somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))*6.0;


                if (truth>=frac)//*0.005)
                {
                    fpBoth++;
                }
                else
                {
                    tnBoth++;
                }
                totalnum++;
            }
        }

        fpfn<<frac<<" "<<fpCam<<" "<<fnCam<<" "<<tpCam<<" "<<tnCam<<" "<<fpLas<<" "<<fnLas<<" "<<tpLas<<" "<<tnLas<<" "<<fpBoth<<" "<<fnBoth<<" "<<tpBoth<<" "<<tnBoth<<" "<<totalnum<<endl;
    }
    filefpfn.close();
}


void fusionSOM::divideData(vector<vector<SomPoint> > data, vector<vector<SomPoint> >& bigPart, vector<vector<SomPoint> >& smallPart)
{
    default_random_engine generator;
    uniform_real_distribution<> distr(0.0,1.0);
    bigPart.clear();
    smallPart.clear();
    int bigIm=0;
    int smallIm=0;
    int bigObj=0;
    int smallObj=0;
    for (int k=0; k<(int)data.size(); k++)
    {
        if (distr(generator)<0.7)
        {
           bigPart.push_back(data[k]);
           bigIm++;
           bigObj+=data[k].size();
        }
        else
        {
           smallPart.push_back(data[k]);
           smallIm++;
           smallObj+=data[k].size();
        }
    }
    cout<<"Base divided: "<<bigIm<<" "<<bigObj<<" "<<smallIm<<" "<<smallObj<<endl;
}
void fusionSOM::reduceData(int num)
{
    for (int i=0; i<(int)data.size(); i++)
    {
        if ((int)data[i].size()>num)
        {
            data.erase(data.begin() + i);
            i--;
        }
    }
}
void fusionSOM::transformData(vector<vector<SomPoint> >& data)
{
    vector<vector<SomPoint> > newdata;
    for (unsigned int i=0; i<data.size(); i++)
    {
        if (data[i].size()>0)
        {
            newdata.push_back(data[i]);
            for (unsigned int j=0; j<newdata.back().size(); j++)
            {
                filereader::lidar2img(newdata.back()[j].lasX, newdata.back()[j].lasY,newdata.back()[j].lasZ, newdata.back()[j].camX, newdata.back()[j].camY);
            }
        }
    }
    data=newdata;
}

void fusionSOM::getFrameDetects(int k, vector<int>& nodesCam, vector<int>& nodesLas,vector<vector<vector<double> > >& weights)
{
    nodesCam.clear();
    nodesLas.clear();
    weights.clear();
    int camI, camJ, lasI, lasJ;
    int camH=somCam.mH;
    int lasH=somLas.mH;
    for (unsigned int i=0; i<data[k].size(); i++)
    {

        double camX=data[k][i].camX;
        double camY=data[k][i].camY;
        somCam.findTheNearest({camX,camY},camI,camJ);

        weights.push_back(vector<vector<double> >());
        nodesCam.push_back(camI*camH+camJ);

        double lasX=data[k][i].lasX;
        double lasY=data[k][i].lasY;
        somLas.findTheNearest({lasX,lasY},lasI,lasJ);
        nodesLas.push_back(lasI*lasH+lasJ);


        for (unsigned int j=0; j<data[k].size(); j++)
        {
            lasX=data[k][j].lasX;
            lasY=data[k][j].lasY;
            somLas.findTheNearest({lasX,lasY},lasI,lasJ);

            weights.back().push_back(vector<double> ());

            double truth=somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1);

            weights.back().back().push_back(truth);

            truth=somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0);

            weights.back().back().push_back(truth);

            truth=(somCam.getWeight2(camI*camH+camJ,lasI*lasH+lasJ,1)+somLas.getWeight2(lasI*lasH+lasJ,camI*camH+camJ,0))/2.0;

            weights.back().back().push_back(truth);
        }

    }
}
void fusionSOM::desassociate()
{
    int nSomCam=0;
    int nSomLas=1;
    double frac=6.0;

    cout<<"desassociating CAM"<<endl;

    for (unsigned int i=0; i<somCam.mNodes.size(); i++)
    {
        double selfhalf=0;
        int nums=0;

        for (unsigned int j=0; j<somCam.mNodes[i].weights[nSomLas].size(); j++)
        {
            selfhalf+=somCam.mNodes[i].weights[nSomLas][j];
        }
        selfhalf/=(double)somCam.mNodes[i].weights[nSomLas].size();
        for (unsigned int j=0; j<somCam.mNodes[i].weights[nSomLas].size(); j++)
        {
            if (somCam.mNodes[i].weights[nSomLas][j]>selfhalf)
                nums++;
        }
        if (nums>(double)somCam.mNodes[i].weights[nSomLas].size()/frac)
        {

            for (unsigned int j=0; j<somCam.mNodes[i].weights[nSomLas].size(); j++)
            {
                //somCam.mNodes[i].weights[nSomLas][j]=0.0000001;
                somLas.mNodes[j].weights[nSomCam][i]=0.0000001;
            }
        }
    }

    cout<<"desassociating LAS"<<endl;

    for (unsigned int i=0; i<somLas.mNodes.size(); i++)
    {
        double selfhalf=0;
        int nums=0;

        for (unsigned int j=0; j<somLas.mNodes[i].weights[nSomCam].size(); j++)
        {
            selfhalf+=somLas.mNodes[i].weights[nSomCam][j];
        }
        selfhalf/=(double)somLas.mNodes[i].weights[nSomCam].size();
        for (unsigned int j=0; j<somLas.mNodes[i].weights[nSomCam].size(); j++)
        {
            if (somLas.mNodes[i].weights[nSomCam][j]>selfhalf)
                nums++;
        }
        if (nums>(double)somLas.mNodes[i].weights[nSomCam].size()/frac)
        {

            for (unsigned int j=0; j<somLas.mNodes[i].weights[nSomCam].size(); j++)
            {
                //somLas.mNodes[i].weights[nSomCam][j]=0.0000001;
                somCam.mNodes[j].weights[nSomLas][i]=0.0000001;
            }
        }
    }


}


double fusionSOM::approaching(int numCam, int numLas, int& numCamNew, int& numLasNew)
{
    bool approaching=true;
    double out;
    int newLas,newCam;

    while (approaching)
    {
        out=somCam.getWeight4(numCam,numLas,1,newLas);
        if (numLas==newLas)
            approaching=false;
        else
        {
            numLas=newLas;
            out=somLas.getWeight4(numLas,numCam,0,newCam);
            if (numCam==newCam)
                approaching=false;
            else
                numCam=newCam;
        }
        approaching=false;
    }
    numCamNew=numCam;
    numLasNew=numLas;

    return out;
}

void fusionSOM::learn()
{
    //========Tracklets=================================================
    if (isTracklets)
    {
        somCam.learn(data,0);
        somLas.learn(data,1);
    }
    //========Tracklets==end============================================

    //========Real======================================================
    else
    {
        cout<<"Data sizes: "<<camData.size()<<" "<<lasData.size()<<endl;
        somCam.learn(camData,0);
        somLas.learn(lasData,1);
        somRectSize.learn(camData,2);
    }
    //========Real==end=================================================

}
