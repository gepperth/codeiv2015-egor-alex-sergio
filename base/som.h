#ifndef SOM_H
#define SOM_H

#include "stdio.h"
#include <random>
#include <iostream>
#include <vector>
#include <cfloat>
#include <chrono>
#include <QPainter>

#include "node.h"

using namespace std;



class Som
{
public:
    int mW;
    int mH;
    int mNodeDim;
    vector<Node> mNodes;
    Som();
    Som(int w, int h, int nodeDim, const vector<pair<int, int> > &sizes);
    void init(int w, int h, int nodeDim, const vector<pair<int, int> > &sizes);
    void draw(QPainter& p,double radius1, double radius2);
    void drawChosen(QPainter& p,int I, int J, double radius1, double radius2);
    void drawChoice(QPainter& p, int realInd, const vector<int>& indices, double radius1, double radius2);
    void drawWeights(QPainter& p,int nSom,Node &node, double radius1, double radius2);
    void attract(int nI, int nJ, double sigma, double rate, const vector<double>& coords );
    void learn(vector<vector<SomPoint> > &data, int identifier);
    void findTheNearest(const vector<double>& coords, int &I, int &J);
    double getWeight2(int Isource, int Ireal, int nSom);
    double getWeight3(int Isource, int Ireal, int nSom);
    double getWeight4(int Isource, int Ireal, int nSom, int& IrealDescented);
    double distance(const vector<double>& coords1, const vector<double>& coords2);
    int getMaxIndex(int Isource, int nSom);
    void normalize();
    int calcDataSize(vector<vector<SomPoint> >& data);
};

#endif // SOM_H
