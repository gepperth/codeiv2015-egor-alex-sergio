#ifndef PARTICLE_H
#define PARTICLE_H


#include "state.h"
#include "target.h"

class Particle
{
private:
    shared_ptr<State> mState;
    double mWeight;
    Target* mTarget;
 public:
    Particle ();
    ~Particle();
    void setState(const shared_ptr<State> state);
    shared_ptr<State> getState() const;
    void setWeight(double weight);
    double getWeight() const;
    void setTarget(Target *target);
    Target* getTarget() const;
    void operator= (Particle in);
    bool operator() ( Particle i, Particle j);
    bool operator< ( Particle i);
    friend std::ostream& operator<< (std::ostream& out, Particle& particle);
};



#endif /* PARTICLE_H */
