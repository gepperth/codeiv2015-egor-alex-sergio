#ifndef PHDINTERFACE_H_
#define PHDINTERFACE_H_

#include "configuration.h"
#include "tracklets.h"
#include "phdFilter.h"
#include "matrix.h"
#include "filereader.h"
#include "module.h"

class PHDInterface
{
public:
	Configuration config;
	Module module;

public:
	PHDInterface();
	~PHDInterface();
	int initialize(vector<string> args);
	int execute();
};




#endif /* PHDINTERFACE_H_ */
