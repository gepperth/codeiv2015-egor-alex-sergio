/*
 * field.h
 *
 *  Created on: Jun 26, 2014
 *      Author: egor
 */

#ifndef FIELD_H_
#define FIELD_H_

#include <fstream>
#include "state.h"

#include "detection.h"
#include "target.h"


class VectorField
{
public:
	int xMin;
	int yMin;
	int xMax;
	int yMax;
	vector<vector<Direction> > mField;
public:
	VectorField() {xMin=-1; yMin=-1; xMax=-1; yMax=-1;};
	~VectorField() {};
	void load(string filename);
	vector<Direction> get(const State& state);
};


#endif /* FIELD_H_ */
