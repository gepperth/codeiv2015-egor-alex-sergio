#include "gmphdFilter.h"

GMPHDFilter::GMPHDFilter()
{

}
GMPHDFilter::~GMPHDFilter()
{
	for (unsigned int i=0; i < mTargets.size(); i++)
		delete mTargets[i];
}

void GMPHDFilter::initialize(const vector<double>& sigmas, const vector<bool>& used, double pfn, int historyLength)
{
	mSigmas=sigmas;
	mUsed=used;
	targetHistoryLength=historyLength;
    mPfn=pfn;

	mPd=0.1;
	mPs=1.0+mPd;
	mPb=1.0-3*mPd;
    mAssociationCoef=1.0;
    //mPs=mPd+mPb;
    //mPs=1.0;
    //mPfn=0.0;

	idNum=0;
	//unsigned seed = chrono::system_clock::now().time_since_epoch().count();
	unsigned seed=0;
	mGenerator.seed(seed);
	mObservations.clear();

}
void GMPHDFilter::association(ostream& out)
{

}

void GMPHDFilter::observation(vector<Detection> detections)
{
/*
	double sum;
	mObservations.clear();
	mObservations=detections;
	vector<double> oneComponent,newWeights;
	newWeights.assign(mParticles.size()*nParticlesPerObject,0.0);


	vector<double> sigmas=mSigmas;
	for (unsigned int i=0; i<sigmas.size(); i++)
		sigmas[i]*=mInfluenceCoef;

	for (unsigned int j=0; j < detections.size(); j++)
	{
		oneComponent.assign(mParticles.size()*nParticlesPerObject,0.0);
		sum=0.0;
		unsigned int ik=0;
		State oneState=detections[j].getState();


		for (unsigned int i=0; i < mParticles.size(); i++)
		{
			oneState.setSpeed(mTargets[i]->getState(),oneState);
			for (unsigned int k=0; k < mParticles[i].size(); k++)
			{

				double tmp=mParticles[i][k].getWeight()*oneState.getGaussian1(mParticles[i][k].getState(),sigmas,mUsed,true,true,true);
				//double tmp=mParticles[i][k].getWeight()*oneState.getMahalanobis(mParticles[i][k].getState(),sigmas,mUsed,1.0);
				//if (tmp>0.001)
					oneComponent[ik] = tmp;
				sum += oneComponent[ik];
				ik++;
			}
		}

	    for (ik=0; ik < oneComponent.size(); ik++)
	    {
			if (sum!=0)
			{
				//reject far detections
				//if (sum>0.05)
				{
					oneComponent[ik] = oneComponent[ik]/sum;
					newWeights[ik] = newWeights[ik]+oneComponent[ik];
				}
			}
	    }
	}
	int ik=0;
	for (unsigned int i=0; i < mParticles.size(); i++)
	{
		for (unsigned int k=0; k < mParticles[i].size(); k++)
		{
			newWeights[ik] = newWeights[ik]+mParticles[i][k].getWeight()*mPfn;
			mParticles[i][k].setWeight(newWeights[ik]);
			ik++;
		}
	}
	*/
}


void GMPHDFilter::resample2()
{
	/*
	vector<double> cumulatedProbabilities;
	double sum;
	double totalSum=0.0;

	double nTargets=mTargets.size();
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		getCumulatedProbability(i,cumulatedProbabilities,sum);
		mTargets[i]->setProbability(sum);

		//if (sum>0.01)//0.25/nTargets)
		if (sum>mPb*mPd*mPd*mPd*mPd)
		{
			vector<Particle> oldParticles(mParticles[i]);
			oneResample(oldParticles,i,cumulatedProbabilities,sum);
			totalSum+=sum;
		}
		else
		{
			//cout<<"DELETE"<<endl;
			delete mTargets[i];
			mTargets.erase (mTargets.begin()+i);
			mParticles.erase (mParticles.begin()+i);
			i--;
		}
	}
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		mTargets[i]->setProbability(mTargets[i]->getProbability()/totalSum);
	}
*/
}

void GMPHDFilter::merge(ostream& out)
{
	/*
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		for (unsigned int j=i+1; j<mTargets.size(); j++)
		{
			if (mTargets[i]->isIntersected(*mTargets[j],mUsed))
			{
				//cout<<"MERGE "<<mTargets[i]->getID()<<" "<<mTargets[j]->getID()<<endl;
				*mTargets[i]=mTargets[i]->merge(*mTargets[j],mUsed);
				delete mTargets[j];
				out<<mTargets[j]->getID()<<endl;
				mTargets.erase(mTargets.begin()+j);
				mParticles.erase(mParticles.begin()+j);
				j--;
			}
		}
	}
	*/
}

vector<Target> GMPHDFilter::correction()
{
	vector<Target> outTargets(mTargets.size());
	/*
	for (unsigned int i=0; i<mTargets.size(); i++)
	{
		State meanState;
		for (unsigned int j=0; j < mParticles[i].size(); j++)
		{
			meanState = meanState+mParticles[i][j].getState();
		}
		meanState/=double(mParticles[i].size());
		mTargets[i]->setState(meanState);
		outTargets[i]=*mTargets[i];
	}
	*/
    return outTargets;

}





vector<Target> GMPHDFilter::evaluate(vector<Detection> detections,ostream& associations,ostream& unions)
{

	//prediction();

	association(associations);

	observation(detections);

	resample2();

	merge(unions);

	return correction();
}
int GMPHDFilter::getTargetsNumber()
{
	return mTargets.size();
}
/*
vector<vector<Particle> > PHDFilter::getParticles()
{
	return mParticles;
}*/

void GMPHDFilter::printParticles(ostream& out)
{
	/*
	for (unsigned int i=0; i<mParticles.size(); i++)
	{
		for (unsigned int j=0; j<mParticles[i].size(); j++)
		{
			out << mParticles[i][j]<<": ";
		}
	}
	if (mParticles.size()!=0)
		out << endl;
		*/
}
