#include "field.h"



#define MFORCE 0.2

void VectorField::load(string filename)
{
    ifstream in(filename, ios::binary);
    int size;
    //double tmp;
    mField.clear();


    xMin=0;
    yMin=0;
    xMax=1000;
    yMax=1000;
    mField.assign((xMax-xMin)*(yMax-yMin),vector<Direction>());
    for (int i=0; i<xMax-xMin; i++)
    {
    	for (int j=0; j<yMax-yMin; j++)
    	{
    		size=2;
    		mField[(yMax-yMin)*i+j].assign(size,Direction());
    		mField[(yMax-yMin)*i+j][0].mD.push_back(0);
    		mField[(yMax-yMin)*i+j][0].mD.push_back(-10);
    		mField[(yMax-yMin)*i+j][1].mD.push_back(-10);
    		mField[(yMax-yMin)*i+j][1].mD.push_back(0);
    	}
    }
    /*

    in.read((char*)&xMin, sizeof(int));
    in.read((char*)&yMin, sizeof(int));
    in.read((char*)&xMax, sizeof(int));
    in.read((char*)&yMax, sizeof(int));
    //xMin+=1000;
    //yMin+=1000;
    //xMax+=1000;
    //yMax+=1000;
    cout<<xMax<<" "<<xMin<<" "<<yMax<<" "<<yMin<<endl;
    mField.assign((xMax-xMin)*(yMax-yMin),vector<Direction>());
	for (int i=0; i<xMax-xMin; i++)
	{
		for (int j=0; j<yMax-yMin; j++)
		{
			in.read((char*)&size, 4);
			mField[(yMax-yMin)*i+j].assign(size,Direction());
			for (int k=0; k<size; k++)
			{
				in.read((char*)&tmp, sizeof(double));
				mField[(yMax-yMin)*i+j][k].mD.push_back(tmp);
				in.read((char*)&tmp, sizeof(double));
				mField[(yMax-yMin)*i+j][k].mD.push_back(tmp);
				mField[(yMax-yMin)*i+j][k].mWeight=1.0/size;
			}
		}
	}*/

	in.close();
}

vector<Direction> VectorField::get(const State& state)
{
    vector<Direction> directions;
    /*
	shared_ptr<CWD2D> si=dynamic_pointer_cast<CWD2D>(state.getCoordinate());
	int i=si->coords.mX-xMin;
	int j=si->coords.mY-yMin;
	double vx=si->coords.mVx;
	double vy=si->coords.mVy;



	if (i>=xMax-xMin||j>=yMax-yMin||j<0||i<0)
	{
		directions.clear();
		return directions;
	}
	else
	{
		directions=mField[(yMax-yMin)*i+j];
		//double newNorm=sqrt(pow(si->mDirection,2.0)+pow(sj->mDirection,2.0));
		double newNorm=sqrt(pow(vx,2.0)+pow(vy,2.0));
		//newNorm=500;
		for (unsigned int k=0; k<directions.size(); k++)
		{
			double oldNorm=sqrt(pow(directions[k].mD[0],2.0)+pow(directions[k].mD[1],2.0));
			if (oldNorm!=0.0)
				for (int l=0; l<2; l++)
					directions[k].mD[l]*=newNorm/oldNorm;
		}
    }*/
	return directions;
}


