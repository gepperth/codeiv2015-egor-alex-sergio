#include "target.h"

#define MODELFORCE 0.05

Target::Target()
{
	mHistoryLength=2;
	mID=-1;
	mValidity.push_back(0.0);
	mModelForce=MODELFORCE;
	mProbability=0.0;
}
Target::Target(int id, int historyLength)
{
	mID=id;
	mHistoryLength=historyLength;
	mValidity.push_back(0.0);
	mModelForce=MODELFORCE;
	mProbability=0.0;
}
Target::~Target()
{

}
void Target::set(int id, int historyLength)
{
	mID=id;
	mHistoryLength=historyLength;
	mValidity.push_back(0.0);
	mModelForce=MODELFORCE;
}
void Target::setID(int id)
{
	mID=id;
}
int Target::getID() const
{
	return mID;
}
void Target::setProbability(double probability)
{
	mProbability=probability;
}
double Target::getProbability() const
{
	return mProbability;
}
void Target::setValidity(double validity)
{
	while ((int)mValidity.size()>mHistoryLength)
	    mValidity.pop_front();

	double lambda=0.5;
	mValidity.push_back(lambda*validity+(1-lambda)*mValidity.back());
	//mValidity.push_back(validity);

	/*double add;
	if (mValidity.back()-1.0>=0)
		add=0.01;
	else
		add=-0.01;

	mModelForce=min(0.5,max(0.01,mModelForce+add));//mValidity.back();
	 *
	 */
}
double Target::getValidity() const
{
	return *mValidity.rbegin();
}
Target Target::operator=(const Target i)
{
	this->mHistory=i.mHistory;
	this->mHistoryLength=i.mHistoryLength;
	this->mProbability=i.mProbability;
	this->mID=i.mID;
	this->mValidity=i.mValidity;
	return *this;
}
void Target::setState(const shared_ptr<State> state)
{
	while ((int)mHistory.size()>mHistoryLength)
	    mHistory.pop_front();
    mHistory.push_back(state->copy());
}
shared_ptr<State> Target::getState() const
{
    return (*mHistory.rbegin())->copy();
}
list<shared_ptr<State>> Target::getHistory() const
{
	return mHistory;
}
list<double> Target::getHistoryValidity() const
{
	return mValidity;
}
void Target::setHistory(const list<shared_ptr<State>>& history)
{
	mHistory=history;
}

double Target::intersection(const Target& i, const vector<double>& sigmas) const
{
    return this->getState()->getGaussian(i.getState(),sigmas);
}
Target Target::merge(const Target& i) const
{
	Target merged;
	if (this->mID<i.mID)
		merged=*this;
	else
		merged=i;

	return merged;
}

std::ostream& operator<< (std::ostream& out, Target& target)
{
    shared_ptr<State> state=target.getState();
    list<shared_ptr<State>> history=target.getHistory();
    list<shared_ptr<State>>::reverse_iterator it=history.rbegin();
	if (history.size()>3)
	{
        (*it)->print(out);
        out<<":";
        it++;
        (*it)->print(out);
        out<<":";
        it++;
        (*it)->print(out);
        out<<":";
        it++;
        (*it)->print(out);
        out<<":"<<target.getID()<<" "<<target.getValidity()<<endl;

	}
	return out;
}
