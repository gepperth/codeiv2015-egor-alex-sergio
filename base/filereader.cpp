#include "filereader.h"

namespace filereader
{
	void trackletsToPointsWithTransform(vector<vector<Point> > *points, int& nF, string trackletsFileName, string oxtsFolderName)
	{
		Tracklets* tracklets = new Tracklets();
		Tracklets::tPose *pose;
		CarData cardata;
		string folder=oxtsFolderName;
		cardata.read(folder);
		tracklets->loadFromFile(trackletsFileName);
		points->clear();
		nF=-1;
		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			nF=std::max(nF,tracklets->getTracklet (i)->lastFrame());
		}
		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			points->push_back(vector<Point>());
			double oldx=0;
			double oldy=0;
			for (int j=0; j<nF; j++)
			{
				if (tracklets->getPose (i, j, pose))
				{
					Point point;
					point.mX=pose->tx;
					point.mY=-pose->ty;
					point.mW=tracklets->getTracklet(i)->h;
					point.mH=tracklets->getTracklet(i)->w;
					point.nFrame=j;


					double alpha=-cardata.positions[j].yaw;
					double xx[] = {point.mX, point.mY};
					double T[] = {cos(alpha), -sin(alpha),
									sin(alpha), cos(alpha)};
					Matrix Tm(T,2,2);
					Vector X(xx,2);
					Vector Y;

					Y=Tm*X;

					point.mX=Y[0]+cardata.positions[j].e;
					point.mY=Y[1]-cardata.positions[j].n;

					if (oldx==0||oldy==0)
					{
					    oldx=point.mX;
					    oldy=point.mY;
					}

					point.mVx=point.mX-oldx;
					point.mVy=point.mY-oldy;
					oldx=point.mX;
					oldy=point.mY;
					point.nFrame=j;

					(*points)[i].push_back(point);
				}
			}
			cout<<"Track "<<i<<" "<<(*points)[i].size()<<endl;
		}
		delete tracklets;
	}
	void fieldToPoints(vector<vector<Point> > *points, string fieldFileName)
	{
		ifstream in (fieldFileName);
		int Size, size;
		points->clear();
		in>>Size;
		points->assign(Size,vector<Point>());

		for (unsigned int i=0; i<points->size(); i++)
		{
			in>>size;
			(*points)[i].assign(size,Point());
			for (unsigned int j=0; j<(*points)[i].size(); j++)
				in>>(*points)[i][j].mX>>(*points)[i][j].mY>>(*points)[i][j].mVx>>(*points)[i][j].mVy;
		}
		in.close();
	}
	void pointsToField(vector<vector<Point> > *points, string fieldFileName)
	{
		ofstream out (fieldFileName);

		int Size,size;
		Size=points->size();
	    out<<Size<<" ";
	    for (unsigned int i=0; i<points->size(); i++)
	    {
	    	size=(int)(*points)[i].size();
	    	out<<size<<" ";
	    	for (unsigned int j=0; j<(*points)[i].size(); j++)
	    		out<<(*points)[i][j].mX<<" "<<(*points)[i][j].mY<<" "<<(*points)[i][j].mVx<<" "<<(*points)[i][j].mVy<<" ";
	    }
	    out.close();
	}
    void trackletsToDetectionsWithTransform(vector<Detection> &detections, Tracklets *tracklets, CarData &cardata, int nFrame, bool transform)
	{
		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
        double x_noised,y_noised,h,w;//,l;
        //State current_state;
		Tracklets::tPose *pose;
		normal_distribution<double> distributionX(0.0,0.5);
		normal_distribution<double> distributionY(0.0,0.5);
		default_random_engine generator(seed);

        double R[] = {9.999239e-01, 9.837760e-03, -7.445048e-03, 0.0,
                      -9.869795e-03, 9.999421e-01, -4.278459e-03, 0.0,
                      7.402527e-03, 4.351614e-03, 9.999631e-01, 0.0,
                      0.0,              0.0,            0.0,     1.0};
        double P[] = {7.215377e+02, 0.000000e+00, 6.095593e+02, 0.000000e+00,
                      0.000000e+00, 7.215377e+02, 1.728540e+02, 0.000000e+00,
                      0.000000e+00, 0.000000e+00, 1.000000e+00, 0.000000e+00};
        double Tcv[] = {7.533745e-03, -9.999714e-01, -6.166020e-04, -4.069766e-03,
                        1.480249e-02, 7.280733e-04, -9.998902e-01, -7.631618e-02,
                        9.998621e-01, 7.523790e-03, 1.480755e-02, -2.717806e-01,
                        0.0,            0.0,             0.0,            1.0};

        Matrix Rm(R,4,4);
        Matrix Pm(P,3,4);
        Matrix Tcvm(Tcv,4,4);

		detections.clear();
		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			if (tracklets->getPose (i, nFrame, pose))
			{
				x_noised=pose->tx;//+distributionX(generator);
                y_noised=pose->ty;//+distributionY(generator);
				h=tracklets->getTracklet(i)->h;
				w=tracklets->getTracklet(i)->w;
                //l=tracklets->getTracklet(i)->l;


                if (transform)
                {
                    double xx[] = {x_noised, y_noised, pose->tz, 1.0};

                    Vector X(xx,4);
                    Vector Y;
                    Y=Pm*Rm*Tcvm*X;

                    x_noised=Y[0]/Y[2];
                    y_noised=Y[1]/Y[2];
                }
                if (cardata.positions.size()!=0)
                {
                    /*
                    double alpha=-cardata.positions[nFrame].yaw;
                    double xx[] = {x_noised, y_noised};
                    double T[] = {cos(alpha), -sin(alpha),
							   sin(alpha), cos(alpha)};

                    Matrix Tm(T,2,2);
                    Vector X(xx,2);
                    Vector Y;

                    Y=Tm*X;

                    x_noised=Y[0]+cardata.positions[nFrame].e;
                    y_noised=Y[1]-cardata.positions[nFrame].n;
                    */
                }

                shared_ptr<State> state=make_shared<State2Ddim>();

                vector<double> data;
                data.push_back(x_noised);
                data.push_back(y_noised);
                data.push_back(0);
                data.push_back(0);
                data.push_back(w);
                data.push_back(h);
                state->set(data);

                Detection detection(state,1.0);


                detections.push_back(detection);

			}
		}
	}
    void linesToDetections(vector<Detection> &detections, ifstream &data, string &line)
	{
		detections.clear();
        double x1,y1,x2,y2;//,z;
		double score,xl,yl,zl;
		int id;
		double x_noised,y_noised,h,w;
        //State current_state;

		while (getline(data,line)&&line[0]!='#')
		{
			stringstream ss(line);
			ss>>x1>>y1>>x2>>y2>>score>>xl>>yl>>zl>>id;

			xl/=1000.0;
			yl/=1000.0;
			zl/=-1000.0;

			w=(x2-x1)/2.0;
			h=(y2-y1)/2.0;
			x_noised=(x1+x2)/2.0;
			y_noised=(y1+y2)/2.0;
            shared_ptr<State> state=make_shared<State2Ddim>();

            vector<double> data;
            data.push_back(x_noised);
            data.push_back(y_noised);
            data.push_back(0);
            data.push_back(0);
            data.push_back(w);
            data.push_back(h);
            state->set(data);

            Detection detection(state,1.0);

            detection.setID(id);
            detections.push_back(detection);
		}
	}

    void laserToClusterPoints(vector<ClusterPoint> &points, string &dataFolder, string &line, int nFrame)
	{
        points.clear();
		ifstream currentFile;
		string oneLine;
        //State current_state;
		string num=std::to_string(nFrame);
		while (num.size()<5)
			num="0"+num;
		double x,y,z;
        int sourceId;//, layerId;
        int i=0;
        line=dataFolder+"cam-coord-ibeoscan-"+num+".txt";
        //cout<<line<<endl;
        currentFile.open(line);
		if (!currentFile.good())
		{
            cout<<"Can't load "<<line<<endl;
		}
		else
		{
			//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);
			while (getline(currentFile,oneLine))
			{
				stringstream ss(oneLine);

                //ss>>sourceId>>layerId>>x>>y>>z;
                ss>>x>>y>>z>>sourceId;

                //if (sourceId==2)//layerId==0||layerId==1)
                {
                    ClusterPoint point;
                    point.mX=x;
                    point.mY=y;
                    point.mZ=z;
                    point.mLink=i+1;

                    points.push_back(point);
                    i++;
                }
			}
            points.back().mLink=0;
		}
        //cout<<"Laser points "<<points.size()<<endl;
		currentFile.close();
        sort(points.begin(),points.end());
	}
	void pointsToBinField(vector<vector<Point> > *points, string fieldFileName, double xmin, double ymin, double xmax, double ymax, double distLimit)
	{
		 ofstream out(fieldFileName, ios::binary);

		 out.write((const char*)&xmin, sizeof(int));
		 out.write((const char*)&ymin, sizeof(int));
		 out.write((const char*)&xmax, sizeof(int));
		 out.write((const char*)&ymax, sizeof(int));

		 vector<Point> tmp;
		 double dist;
		 for (int i=xmin; i<xmax; i++)
		 {
			 for (int j=ymin; j<ymax; j++)
			 {
				 tmp.clear();
				 for (unsigned int k=0; k<points->size(); k++)
				 {
		    		double mindist=DBL_MAX, imin;
		    		for (int l=0; l<(int)(*points)[k].size()-1; l++)
		    		{
		    			dist=(*points)[k][l].distanceToLine(i,j,(*points)[k][l+1]);

		    			if (dist<=mindist)
		    			{
		    				mindist=dist;
		    				imin=l;
		    			}
		    		}
		    		if (mindist<distLimit)
		    			tmp.push_back((*points)[k][imin]);
				 }
				 int s=tmp.size();
				 out.write((const char*)&s, sizeof(int));
				 double d;
				 for (unsigned int k=0; k<tmp.size(); k++)
				 {
					 d=tmp[k].mVx;
					 out.write((const char*)&d, sizeof(double));
					 d=tmp[k].mVy;
					 out.write((const char*)&d, sizeof(double));
				 }
			 }
		  }
		  out.close();
	}
    void trackletsToSom(string trackletsFolderName, vector<vector<SomPoint>>& data)
    {
        DIR *dir;
        struct dirent *ent;
        data.clear();
        if ((dir = opendir (trackletsFolderName.c_str())) != NULL)
        {
          while ((ent = readdir (dir)) != NULL)
          {
              Tracklets* tracklets = new Tracklets();
              Tracklets::tPose *pose;
              if (tracklets->loadFromFile(trackletsFolderName+"/"+string(ent->d_name)+"/tracklet_labels.xml"))
              {
                  double R[] = {9.999239e-01, 9.837760e-03, -7.445048e-03, 0.0,
                                -9.869795e-03, 9.999421e-01, -4.278459e-03, 0.0,
                                7.402527e-03, 4.351614e-03, 9.999631e-01, 0.0,
                                0.0,              0.0,            0.0,     1.0};
                  double P[] = {7.215377e+02, 0.000000e+00, 6.095593e+02, 0.000000e+00,
                                0.000000e+00, 7.215377e+02, 1.728540e+02, 0.000000e+00,
                                0.000000e+00, 0.000000e+00, 1.000000e+00, 0.000000e+00};
                  double Tcv[] = {7.533745e-03, -9.999714e-01, -6.166020e-04, -4.069766e-03,
                                  1.480249e-02, 7.280733e-04, -9.998902e-01, -7.631618e-02,
                                  9.998621e-01, 7.523790e-03, 1.480755e-02, -2.717806e-01,
                                  0.0,            0.0,             0.0,            1.0};

                  Matrix Rm(R,4,4);
                  Matrix Pm(P,3,4);
                  Matrix Tcvm(Tcv,4,4);

                  cout<<ent->d_name<<endl;
                  int nF=-1;
                  for (int i=0; i<tracklets->numberOfTracklets(); i++)
                  {
                      nF=std::max(nF,tracklets->getTracklet (i)->lastFrame());
                  }
                  for (int j=0; j<nF; j++)
                  {
                        data.push_back(vector<SomPoint>());
                        for (int i=0; i<tracklets->numberOfTracklets(); i++)
                        {

                            if (tracklets->getPose (i, j, pose))
                            {
                                SomPoint point;
                                point.lasX=pose->tx;
                                point.lasY=pose->ty;
                                point.camW=tracklets->getTracklet(i)->w;
                                point.camH=tracklets->getTracklet(i)->h;
                                double xx[] = {point.lasX, point.lasY, pose->tz, 1.0};

                                Vector X(xx,4);
                                Vector Y;
                                Y=Pm*Rm*Tcvm*X;

                                point.camX=Y[0]/Y[2];
                                point.camY=Y[1]/Y[2];
                                if (point.camX<=1242 && point.camX>=0 && point.camY<=375 && point.camY>=0)
                                    data.back().push_back(point);
                            }
                        }
                        if (data.back().empty())
                            data.pop_back();
                  }
              }
              delete tracklets;
          }

          int total=0;
          for (int i=0; i<(int)data.size(); i++)
          {
              total+=data[i].size();
          }
          cout<<"data "<<data.size()<<" "<<total<<endl;
          closedir (dir);
        }
        else
        {
            cout<<"Can't open dir"<<endl;
        }
    }
    void lasToSom(string folderName, vector<vector<SomPoint>>& data)
    {
        cout<<"Laser data downloading..."<<endl;
        data.clear();
        ifstream currentFile;
        string num,line;
        int n=1;//1
        int nmax=10;

        double x,y,z;
        for (;n<nmax; n++)
        {
            if (n!=10)
            {
            num=to_string(n);
            line=folderName+"outLas"+num+".txt";
            int nFrame=0;
            currentFile.open(line);
            if (!currentFile.good())
            {
                cout<<"Can't load "<<line<<endl;
            }
            else
            {
                bool notend=getline(currentFile,line);
                while (notend)
                {
                    vector<SomPoint> frame;
                    notend=getline(currentFile,line);
                    while (notend&&(line[0]!='/'))
                    {
                        stringstream ss(line);
                        //ss>>x>>y>>z;
                        ss>>y>>z>>x;

                        SomPoint point;
                        point.lasX=x;// /1000;
                        point.lasY=y;// /1000;
                        point.lasZ=z;// /1000;

                        frame.push_back(point);
                        notend=getline(currentFile,line);
                    }
                    data.push_back(frame);
                    nFrame++;
                }
            }
            currentFile.close();
            cout<<line<<" "<<nFrame<<endl;
            }
        }
        cout<<"done"<<endl;
    }
    void camToSom(string folderName, vector<vector<SomPoint>>& data)
    {
        cout<<"Camera data downloading..."<<endl;
        data.clear();
        ifstream currentFile;
        string num,line;
        int n=1;//1
        int nmax=10;

        double xmin,ymin,xmax,ymax;
        for (;n<nmax; n++)
        {
            if (n!=10)
            {
            num=to_string(n);
            line=folderName+"out"+num+".txt";
            int nFrame=0;
            currentFile.open(line);
            if (!currentFile.good())
            {
                cout<<"Can't load "<<line<<endl;
            }
            else
            {
                bool notend=getline(currentFile,line);
                while (notend)
                {
                    vector<SomPoint> frame;
                    notend=getline(currentFile,line);
                    while (notend&&(line[0]!='/'))
                    {
                        stringstream ss(line);
                        ss>>xmin>>xmin>>ymin>>xmax>>ymax;

                        SomPoint point;
                        point.camX=(xmax+xmin)/2.0;
                        point.camY=(ymax+ymin)/2.0;
                        point.camW=(xmax-xmin)/2.0;
                        point.camH=(ymax-ymin)/2.0;


                        frame.push_back(point);
                        notend=getline(currentFile,line);
                    }
                    data.push_back(frame);
                    nFrame++;
                }
            }
            currentFile.close();

            cout<<line<<" "<<nFrame<<endl;
            }
        }
        cout<<"done"<<endl;
    }
    void camLasToSom(string folderName, vector<vector<SomPoint>>& data)
    {
        cout<<"Camera and Laser data downloading..."<<endl;
        data.clear();
        ifstream currentFile;
        string num,line;
        int n=1;
        int nmax=10;

        double xmin,ymin,xmax,ymax,xlas,ylas,zlas;
        for (;n<nmax; n++)
        {
            num=to_string(n);
            line=folderName+"out"+num+".txt";
            int nFrame=0;
            currentFile.open(line);
            if (!currentFile.good())
            {
                cout<<"Can't load "<<line<<endl;
            }
            else
            {
                bool notend=getline(currentFile,line);
                while (notend)
                {
                    vector<SomPoint> frame;
                    notend=getline(currentFile,line);
                    while (notend&&(line[0]!='/'))
                    {
                        stringstream ss(line);
                        ss>>xmin>>ymin>>xmax>>ymax>>ylas>>zlas>>xlas;

                        SomPoint point;
                        point.camX=(xmax+xmin)/2.0;
                        point.camY=(ymax+ymin)/2.0;
                        point.camW=(xmax-xmin)/2.0;
                        point.camH=(ymax-ymin)/2.0;
                        point.lasX=xlas;// /1000;
                        point.lasY=ylas;// /1000;
                        point.lasZ=zlas;// /1000;

                        frame.push_back(point);
                        notend=getline(currentFile,line);
                    }
                    data.push_back(frame);
                    nFrame++;
                }
            }
            currentFile.close();

            cout<<line<<" "<<nFrame<<endl;
        }
        cout<<"done"<<endl;
    }
    void camToSomOne(string folderName, int n, vector<vector<SomPoint>>& data)
    {
        cout<<"Camera data downloading..."<<endl;
        data.clear();
        ifstream currentFile;
        string num,line;

        double xmin,ymin,xmax,ymax;

        num=to_string(n);
        line=folderName+"out"+num+".txt";
        int nFrame=0;
        currentFile.open(line);
        if (!currentFile.good())
        {
            cout<<"Can't load "<<line<<endl;
        }
        else
        {
            bool notend=getline(currentFile,line);
            while (notend)
            {
                vector<SomPoint> frame;
                notend=getline(currentFile,line);
                while (notend&&(line[0]!='/'))
                {
                    stringstream ss(line);
                    ss>>xmin>>xmin>>ymin>>xmax>>ymax;

                    SomPoint point;
                    point.camX=(xmax+xmin)/2.0;
                    point.camY=(ymax+ymin)/2.0;
                    point.camW=(xmax-xmin)/2.0;
                    point.camH=(ymax-ymin)/2.0;

                    frame.push_back(point);
                    notend=getline(currentFile,line);
                }
                data.push_back(frame);
                nFrame++;
            }
        }
        currentFile.close();

        cout<<line<<" "<<nFrame<<endl;
        cout<<"done"<<endl;
    }
}
void filereader::lidar2img(double lx, double ly,double lz, double & x, double & y)
{
    /*
    //left01
    double f1 = 8.2826601e+02;  // K11
    double f2 = 8.2781930e+02;  // K22
    double c1 = 8.0080822e+02;  // K13
    double c2 = 6.0706376e+02;  // K23
    double alpha = 0.0; // K12
    double kc [] = {
      -2.4183500e-02,
       4.6189326e-02,
      -6.3767040e-04,
      -5.7531256e-05,
       0.0000000e+00
       } ;
    */

    double f1 = 2454.7;  // K11
    double f2 = 2.4458e+03;  // K22
    double c1 = 8.688e+02;  // K13
    double c2 = 5.889e+02;  // K23
    double alpha = 0.0; // K12
    double kc [] = {
      0.0337,
      0.1002,
      0.0012,
      0.0069,
       0.0000000e+00
       } ;

    //yc=ptcloud(2,:);
    //zc=ptcloud(3,:);

    //normalise over Z (in this frame);
    // z is distance, y is height, x is left/right

    //double a=lx/ly;
    //double b=lz/ly;
    // x is distance, z is height, y is left/right
    double a=ly/lx; //lz<->ly
    double b=lz/lx;

    // add distortion
    double r = sqrt(a*a + b*b);

    double ad = a*(1 + kc[0]*r*r + kc[1]*r*r*r*r + kc[4]*r*r*r*r*r*r) +  2.*kc[2]*a*b + kc[3]*(r*r + 2.*a*a);
    double bd = b*(1 + kc[0]*r*r + kc[1]*r*r*r*r + kc[4]*r*r*r*r*r*r) +  kc[2]*(r*r + 2.*b*b) + 2.*kc[3]*a*b;
    //ad = a; bd = b ;

    // image coordinates
    x = f1*(ad + alpha*bd) + c1;
    y = f2*bd + c2 ;
}

void filereader::planeTransform(double lx, double ly,double lz, double & x, double & y, double& z,bool inv)
{

    double MM[] = {0.059494, 	 0.998160, 	 -0.011698,
                   0.123683, 	 -0.018999, 	 -0.992140,
                   -0.990537, 	 0.057580, 	 -0.124586};

/*
    double MM[] = {-0.081973, 	 0.996289, 	 -0.026229,
                   0.128646, 	 -0.015520, 	 -0.991569,
                   -0.988297, 	 -0.084656, 	 -0.126896};
*/
    /*
    double MM[] = {-0.088086, 	 0.996023, 	 -0.013372,
                   0.127911, 	 -0.002003, 	 -0.991784,
                   -0.987866, 	 -0.089073, 	 -0.127225};
                   */
    /*
    double MM[] = {-0.163292, 	 0.986571, 	 -0.003694,
                   0.125547, 	 0.017066, 	 -0.991941,
                   -0.978557, 	 -0.162440, 	 -0.126648};
                   */
/*
    double MM[] = {0.156649, 	 0.987499, 	 -0.017530,
                   0.117735, 	 -0.036294, 	 -0.992382,
                   -0.980612, 	 0.153392, 	 -0.121949};
*/
    double xx[]={lx, ly, lz};
    Matrix M(MM,3,3);
    Vector X(xx,3);
    Vector Y;
    if (inv)
    {
        Y=M.invert()*X;
    }
    else
    {
        Y=M*X;
    }

    x=Y[0];
    y=Y[1];
    z=Y[2];

}


