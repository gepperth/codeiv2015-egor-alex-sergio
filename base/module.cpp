/*
 * module.cpp
 *
 *  Created on: Nov 21, 2014
 *      Author: egor
 */

#include "module.h"

Module::Module():feeder()
{

}
Module::~Module()
{
	unions.close();
	filtered.close();
	particles.close();
	associations.close();
}
int Module::configurate(Configuration::ModuleConfig& config, vector<shared_ptr<Module> >& modules)
{
	unions.close();

	filtered.close();
	particles.close();
	associations.close();

	unions.open (config.mergedFile);
	filtered.open (config.filteredFile);
	particles.open (config.particleFile);
	associations.open(config.associatedFile);

	if (!unions.good())
	{
		cout<<"Can't open "<<config.mergedFile<<endl;
		//return -1;
	}
	if (!associations.good())
	{
		cout<<"Can't open "<<config.associatedFile<<endl;
		//return -1;
	}
	if (!filtered.good())
	{
		cout<<"Can't open "<<config.filteredFile<<endl;
		//return -1;
	}
	if (!particles.good())
	{
		cout<<"Can't open "<<config.particleFile<<endl;
		//return -1;
	}


	filter.initialize( 	config.particlesPerTarget,
						config.sigmas,
						config.Pfn,
                        config.associationCoef,
                        config.resampleCoef,
                        config.influenceCoef,
						config.historyLength,
						config.fieldFile);
    return feeder.connect(config.dataFile,config.framesIn,"",modules);
}
int Module::execute()
{
	vector<vector<Particle> > oldparticles;
    int out=feeder.get(detections,imageName,line);
	filtered<<line<<endl;
	particles<<line<<endl;
	unions<<line<<endl;
	associations<<line<<endl;


	targets=filter.evaluate(detections,
							oldparticles,
							associations,
							unions);

	for (unsigned int i=0; i<targets.size(); i++)
	{
		filtered << targets[i];
	}

	filter.printParticles(particles);
	return out;
}

