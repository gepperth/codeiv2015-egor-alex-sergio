/*
 * feeder.cpp
 *
 *  Created on: Nov 21, 2014
 *      Author: egor
 */
#include "feeder.h"
#include "module.h"

vector<Detection> convertTargToDet(const vector<Target>& targets)
{

	vector<Detection> detections(targets.size(),Detection());
	for (int i=0; i<(int)targets.size(); i++)
	{
		detections[i].setState(targets[i].getState());
		detections[i].setID(targets[i].getID());
	}
	return detections;
}

Feeder::Feeder()
{
	numIterMax=-1;
	numIter=-1;
	tracklets=NULL;
	module=NULL;
}
Feeder::~Feeder()
{
    if (tracklets!=NULL)
	  delete tracklets ;
    data.close();
}
int Feeder::connect(string detectionsFileName,string imageFolderName,string cardataName, vector<shared_ptr<Module> >& modules)
{
	stringstream dataFileStream(detectionsFileName);
	string name;
	int j;
	dataFileStream >> type;
	int max=0;
    imageFolder=imageFolderName;
    string imageName;
    string extension="png";
    imageNames.clear();
    DIR *dir;
    struct dirent *ent;
    dir = opendir (imageFolder.c_str());
    if (dir != NULL)
    {
        ent = readdir (dir);
        while (ent!=NULL)
        {
            if (ent->d_type == DT_REG)
            {
                string fname = ent->d_name;
                if (fname.find(extension, (fname.length() - extension.length())) != std::string::npos)
                {
                    imageName=imageFolder+string(ent->d_name);
                    imageNames.push_back(imageName);
                }
            }
            ent = readdir (dir);
        }
        closedir (dir);
        sort (imageNames.begin(), imageNames.end());
    }

    if (type=="XML"||type=="XMLT")
	{
		if (tracklets!=NULL)
			delete tracklets ;
		tracklets = new Tracklets();
		dataFileStream >> name;
		if (!tracklets->loadFromFile(name))
		{
			cout<<"Can't load "<<name<<endl;
			return -1;
		}

		cardata.read(cardataName);
		cout<<"cardata "<<cardata.positions.size()<<endl;

		for (int i=0; i<tracklets->numberOfTracklets(); i++)
		{
			max=std::max(max,tracklets->getTracklet (i)->lastFrame());
		}
		line="# "+ std::to_string(numIter+1);
	}
	else if (type=="TXT"||type=="TXTV")
	{
		data.close();
		dataFileStream >> name;
		data.open(name);
		if (!data.good())
		{
			cout<<"Can't load "<<name<<endl;
			return -1;
		}
		while( getline ( data, line ) )
		{
			if(line[0]=='#')
		    	max++;
		}
		data.close();
		data.open(name);
		getline(data,line);
	}
	else if (type=="MOD")
	{
		data.close();
		cout<<"here MOD"<<endl;
		max=1;
		dataFileStream >> j;
		module=modules[j];
	}
	else if (type=="LAS")
	{
		cout<<"here LAS"<<endl;
		data.close();
		dataFileStream >> dataFolder;
		max=1;
	}

	numIterMax=max;

	return numIterMax;
}

int Feeder::get(vector<Detection>& detections,string& imageName,string& outName)
{
	numIter++;
    outName=line;

    if (numIter<(int)imageNames.size())
    {
        imageName=imageNames[numIter];
        //cout<<imageNames[numIter]<<endl;
    }
    else imageName="";

    if ((type=="MOD")&&module!=NULL)
    {
    	detections=convertTargToDet(module->targets);
    	return 1;
    }
    else if (type=="XML")
	{
        filereader::trackletsToDetectionsWithTransform(detections,tracklets,cardata,numIter,false);
		line="# "+ std::to_string(numIter+1);
	}
    else if (type=="XMLT")
    {
        filereader::trackletsToDetectionsWithTransform(detections,tracklets,cardata,numIter,true);
        line="# "+ std::to_string(numIter+1);
    }
	else if (type=="TXT")
	{
        filereader::linesToDetections(detections,data,line);
	}
	else if (type=="LAS")
	{
        //filereader::laserToDetections(detections, dataFolder, line, numIter);
        //return 1;
        return -1;
	}
	else
	{
		cout<<"Error data feed of type "<<type<<endl;
		return -1;
	}

	if (numIter<numIterMax-1)
		return numIter;
	return -1;
}



