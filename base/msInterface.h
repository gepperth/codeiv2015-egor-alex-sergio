/*
 * msInterface.h
 *
 *  Created on: Sep 4, 2014
 *      Author: egor
 */

#ifndef MSINTERFACE_H_
#define MSINTERFACE_H_

#include "configuration.h"
#include "tracklets.h"
#include "phdFilter.h"
#include "matrix.h"
#include "filereader.h"
#include "feeder.h"
#include "module.h"

class MSInterface
{
public:
	vector<shared_ptr<Module> > modules;
	Configuration config;
public:
	MSInterface();
	~MSInterface();
	int initialize(vector<string> mArgs);
	vector<Detection> mergeDetections(const vector<Detection>& one, const vector<Detection>& two);
	int execute();
};


#endif /* MSINTERFACE_H_ */
