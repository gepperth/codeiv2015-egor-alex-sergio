#include "som.h"

Som::Som()
{
}

Som::Som(int w, int h, int nodeDim, const vector<pair<int, int> >& sizes)
{
    init(w,h,nodeDim,sizes);
}
void Som::init(int w, int h, int nodeDim, const vector<pair<int,int> >& sizes)
{
    mNodes.clear();
    default_random_engine generator;
    uniform_real_distribution<> distr(0.0,1000.0);
    mW=w;
    mH=h;
    mNodeDim=nodeDim;
    for (int i=0; i<w*h; i++)
    {
        vector<double> coords;
        for (int j=0; j<nodeDim; j++)
            coords.push_back(distr(generator));
        mNodes.push_back(Node(coords,sizes));
    }
}

void Som::draw(QPainter& p,double radius1, double radius2)
{

    for (int i=0; i<(int)mNodes.size(); i++)
    {
        p.drawEllipse ( QPointF(mNodes[i].coords[0], mNodes[i].coords[1]), radius1, radius2 );
    }


    for (int i=0; i<mW-1; i++)
    {
        for (int j=0; j<mH; j++)
        {
            p.drawLine ( QPointF(mNodes[i*mH+j].coords[0], mNodes[i*mH+j].coords[1]), QPointF(mNodes[(i+1)*mH+j].coords[0], mNodes[(i+1)*mH+j].coords[1]) );
        }
    }
    for (int j=0; j<mH-1; j++)
    {
        for (int i=0; i<mW; i++)
        {
            p.drawLine ( QPointF(mNodes[i*mH+j].coords[0], mNodes[i*mH+j].coords[1]), QPointF(mNodes[i*mH+j+1].coords[0], mNodes[i*mH+j+1].coords[1]) );
        }
    }

}
void Som::drawChosen(QPainter& p,int I, int J, double radius1, double radius2)
{
    p.setBrush(Qt::red);
    p.setPen(Qt::red);

    p.drawEllipse ( QPointF(mNodes[mH*I+J].coords[0],mNodes[mH*I+J].coords[1]), (qreal)radius1, (qreal)radius2 );
    //p.drawEllipse(QRectF(mNodes[mH*I+J].coords[0] - radius1, mNodes[mH*I+J].coords[1] - radius2, 2.0*radius1, 2.0*radius2));
}
void Som::drawChoice(QPainter& p,int realInd, const vector<int>& indices,double radius1,double radius2)
{
    p.setBrush(Qt::green);
    p.setPen(Qt::green);

    for (unsigned int i=0; i<indices.size(); i++)
        p.drawEllipse ( QPointF(mNodes[indices[i]].coords[0],mNodes[indices[i]].coords[1]), (qreal)radius1, (qreal)radius2 );

    p.setBrush(Qt::blue);
    p.setPen(Qt::blue);

    p.drawEllipse ( QPointF(mNodes[indices[realInd]].coords[0],mNodes[indices[realInd]].coords[1]), (qreal)radius1, (qreal)radius2 );
}

void Som::drawWeights(QPainter& p,int nSom,Node &node, double radius1, double radius2)
{
    QBrush brush(Qt::red);
    QColor color(Qt::red);
    QPen pen(Qt::red);

    for (int i=0; i<mW-1; i++)
    {
        p.drawLine ( QPointF(mNodes[i*mH+mH-1].coords[0], mNodes[i*mH+mH-1].coords[1]), QPointF(mNodes[(i+1)*mH+mH-1].coords[0], mNodes[(i+1)*mH+mH-1].coords[1]) );
        p.drawLine ( QPointF(mNodes[i*mH].coords[0], mNodes[i*mH].coords[1]), QPointF(mNodes[(i+1)*mH].coords[0], mNodes[(i+1)*mH].coords[1]) );
    }
    for (int j=0; j<mH-1; j++)
    {
        p.drawLine ( QPointF(mNodes[(mW-1)*mH+j].coords[0], mNodes[(mW-1)*mH+j].coords[1]), QPointF(mNodes[(mW-1)*mH+j+1].coords[0], mNodes[(mW-1)*mH+j+1].coords[1]) );
        p.drawLine ( QPointF(mNodes[j].coords[0], mNodes[j].coords[1]), QPointF(mNodes[j+1].coords[0], mNodes[j+1].coords[1]) );
    }


    vector<double>::const_iterator it;
    it = max_element(node.weights[nSom].begin(), node.weights[nSom].end());
    double maxW=*it;
    for (int i=0; i<(int)mNodes.size(); i++)
    {
        if (maxW!=0)
            color.setAlphaF (node.weights[nSom][i]/maxW);
        brush.setColor(color);
        pen.setColor(color);
        p.setBrush(brush);
        p.setPen(pen);
        p.drawEllipse ( QPointF(mNodes[i].coords[0], mNodes[i].coords[1]), radius1, radius2 );
    }
}


void Som::attract(int nI, int nJ,double sigma, double rate, const vector<double>& coords)
{
    for (int i=0; i<mW; i++)
    {
        for (int j=0; j<mH; j++)
        {
            double gauss=exp((-pow(double(nI-i)/max(1.0,mW-1.0),2.0)
                             -pow(double(nJ-j)/max(1.0,mH-1.0),2.0))
                             /pow(sigma,2.0));

            for (unsigned int k=0; k<coords.size(); k++)
            {
                mNodes[mH*i+j].coords[k]-=rate*gauss*(mNodes[mH*i+j].coords[k]-coords[k]);
            }
        }
    }
}
void Som::learn(vector<vector<SomPoint> >& data, int identifier)
{
    default_random_engine generator;
    normal_distribution<double> distr(400.0,100.0);
    int dataSize=calcDataSize(data);
    cout<<"Learn data size "<<dataSize<<endl;
    if (dataSize==0)
        return;
    uniform_int_distribution<> distrN(0,dataSize-1);

    int iMin, jMin;
    double sigma_f=0.001;
    double sigma_i=10;
    double rate_f=0.005;
    double rate_i=0.5;
    double sigma,rate;
    double t;
    int maxK=10000;
    vector<double> coordinates;

    for (int k=0; k<maxK; k++)
    {
        int randint=distrN(generator);
        int numFrame=0;
        int total=data[numFrame].size();

        while (total<=randint)
        {
            numFrame++;
            total+=data[numFrame].size();
        }
        coordinates.clear();

        if (identifier==0)
        {
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].camX);
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].camY);
        }
        else if (identifier==1)
        {
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].lasX);
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].lasY);
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].lasZ);
        }
        else if (identifier==2)
        {
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].camW);
            coordinates.push_back(data[numFrame][randint-total+data[numFrame].size()].camH);
        }
        else cout<<"Error of identifier"<<endl;


        findTheNearest(coordinates,iMin,jMin);
        t=double(k)/double(maxK);
        sigma=sigma_i*pow((sigma_f/sigma_i),t);
        rate=rate_i*pow((rate_f/rate_i),t);
        attract(iMin,jMin,sigma,rate, coordinates);
    }

}
double Som::distance(const vector<double>& coords1, const vector<double>& coords2)
{
    double candidate=0;
    for (unsigned int k=0; k<coords1.size(); k++)
        candidate+=pow(coords1[k]-coords2[k],2);
    return candidate;
}

void Som::findTheNearest(const vector<double>& coords, int &I, int &J)
{
    double min=DBL_MAX;
    for (int i=0; i<mW; i++)
    {
        for (int j=0; j<mH; j++)
        {
            double candidate=distance(coords,mNodes[mH*i+j].coords);
            if (candidate<min)
            {
                min=candidate;
                I=i;
                J=j;
            }
        }
    }
}

/*
double Som::getWeight(int Isource, int Ireal)
{
    return mNodes[Isource].weights[Ireal];
}*/
double Som::getWeight2(int Isource, int Ireal, int nSom)
{
    int I,J;

    int W,H;
    W=mNodes[0].sizes[nSom].first;
    H=mNodes[0].sizes[nSom].second;
    I=(int)Ireal/H;
    J=Ireal-I*H;

    double sigma=0.1;//0.07;
    double out=0.0;
    for (int i=0; i<W; i++)
    {
        for (int j=0; j<H; j++)
        {
            double gauss=exp((-pow(double(I-i)/max(1.0,W-1.0),2.0)//size of other sensor's som
                             -pow(double(J-j)/max(1.0,H-1.0),2.0))
                             /pow(sigma,2.0));
            out+=gauss*mNodes[Isource].weights[nSom][i*H+j];
        }
    }

    return out;
}

double Som::getWeight3(int Isource, int Ireal, int nSom)
{
    int I,J;

    int W,H;
    W=mNodes[0].sizes[nSom].first;
    H=mNodes[0].sizes[nSom].second;
    I=(int)Ireal/H;
    J=Ireal-I*H;

    double out=0.0;
    int radius=10;

    for (int i=max(0,I-radius); i<min(W, I+radius+1); i++)
    {
        for (int j=max(0,J-radius); j<min(H, J+radius+1); j++)
        {
            out=max(out,mNodes[Isource].weights[nSom][i*H+j]);
        }
    }

    return out;
}


double Som::getWeight4(int Isource, int Ireal, int nSom, int& IrealDescented)
{
    int I,J;

    int W,H;
    W=mNodes[0].sizes[nSom].first;
    H=mNodes[0].sizes[nSom].second;
    I=(int)Ireal/H;
    J=Ireal-I*H;

    double sigma=0.1;//0.07;
    double out=0.0;
    bool descent=1;
    int maxI,maxJ;
    double maxV;
    maxV=-1;
    while (descent)
    {

        for (int ii=-1; ii<=1; ii++)
        {

            for (int jj=-1; jj<=1; jj++)
            {
                out=0;
                //cout<<"ii "<<ii<<" "<<jj<<" "<<I<<" "<<J<<endl;
                for (int i=0; i<W; i++)
                {
                    for (int j=0; j<H; j++)
                    {

                        if (i+ii>=0&&i+ii<W&&j+jj>=0&&j+jj<H)
                        {
                            double gauss=exp((-pow(double(I+ii-i)/max(1.0,W-1.0),2.0)//size of other sensor's som
                                             -pow(double(J+jj-j)/max(1.0,H-1.0),2.0))
                                             /pow(sigma,2.0));

                            out+=gauss*mNodes[Isource].weights[nSom][(i+ii)*H+j+jj];
                            if (out>maxV)
                            {
                                maxV=out;
                                maxI=i+ii;
                                maxJ=j+jj;
                            }
                        }
                    }
                }
            }
        }
        if (maxI==I&&maxJ==J)
            descent=false;
        I=maxI;
        J=maxJ;
        descent=false;
    }

    IrealDescented=maxI*H+maxJ;

    return out;
}

int Som::getMaxIndex(int Isource, int nSom)
{
    int indmax;
    double max=0;
    double candidate;
    for (unsigned int i=0; i<mNodes[Isource].weights[nSom].size(); i++)
    {
        candidate=mNodes[Isource].weights[nSom][i];
        if (candidate>max)
        {
            max=candidate;
            indmax=i;
        }
    }
    return indmax;
}

void Som::normalize()
{
    for (int i=0; i<(int)mNodes.size(); i++)
    {
        mNodes[i].normalize();
    }
}
int Som::calcDataSize(vector<vector<SomPoint> >& data)
{
    int num=0;
    for (unsigned int i=0; i<data.size(); i++)
    {
        for (unsigned int j=0; j<data[i].size(); j++)
        {
            num++;
        }
    }
    return num;
}


