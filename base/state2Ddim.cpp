#include "state2Ddim.h"

using namespace std;

State2Ddim::State2Ddim()
{

}

void State2Ddim::add(const shared_ptr<State> i)
{
    const shared_ptr<State2Ddim> s=dynamic_pointer_cast<State2Ddim>(i);
    mX+=s->mX;
    mY+=s->mY;
    mVx+=s->mVx;
    mVy+=s->mVy;
    mDx+=s->mDx;
    mDy+=s->mDy;
}

void State2Ddim::sub(const shared_ptr<State> i)
{
    const shared_ptr<State2Ddim> s=dynamic_pointer_cast<State2Ddim>(i);
    mX-=s->mX;
    mY-=s->mY;
    mVx-=s->mVx;
    mVy-=s->mVy;
    mDx-=s->mDx;
    mDy-=s->mDy;

}

void State2Ddim::divBy(double d)
{
    mX/=d;
    mY/=d;
    mVx/=d;
    mVy/=d;
    mDx/=d;
    mDy/=d;
}

shared_ptr<State> State2Ddim::copy()
{
    shared_ptr<State2Ddim> copy=make_shared<State2Ddim>();

    copy->mX=mX;
    copy->mY=mY;
    copy->mVx=mVx;
    copy->mVy=mVy;
    copy->mDx=mDx;
    copy->mDy=mDy;

    return copy;
}

void State2Ddim::set(const vector<double> & data)
{
    if (data.size()!=6)
        cout<<"error data size"<<endl;
    mX=data[0];
    mY=data[1];
    mVx=data[2];
    mVy=data[3];
    mDx=data[4];
    mDy=data[5];

}

void State2Ddim::get(vector <double> & data) const
{
    data.clear();
    data.push_back(mX);
    data.push_back(mY);
    data.push_back(mVx);
    data.push_back(mVy);
    data.push_back(mDx);
    data.push_back(mDy);
}

double State2Ddim::getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const
{
    const shared_ptr<State2Ddim> s=dynamic_pointer_cast<State2Ddim>(i);
    double ex=(pow(mX-s->mX,2)+pow(mY-s->mY,2))/(2.0*sigmas[0]*sigmas[0]);
    ex+=(pow(mVx-s->mVx,2)+pow(mVy-s->mVy,2))/(2.0*sigmas[1]*sigmas[1]);
    ex+=(pow(mDx-s->mDx,2)+pow(mDy-s->mDy,2))/(2.0*sigmas[2]*sigmas[2]);

    return exp(-ex)*1000000;
}
void State2Ddim::setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end)
{
    const shared_ptr<State2Ddim> b=dynamic_pointer_cast<State2Ddim>(begin);
    const shared_ptr<State2Ddim> e=dynamic_pointer_cast<State2Ddim>(end);
    mVx=e->mX-b->mX;
    mVy=e->mY-b->mY;
}

void State2Ddim::applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator)
{
    normal_distribution<double> distributionCoord(0.0,sigmas[0]);
    normal_distribution<double> distributionSpeed(0.0,sigmas[1]);
    normal_distribution<double> distributionSize(0.0,sigmas[2]);

    mX+=distributionCoord(generator);
    mY+=distributionCoord(generator);
    mVx+=distributionSpeed(generator);
    mVy+=distributionSpeed(generator);
    mDx+=distributionSize(generator);
    mDy+=distributionSize(generator);
}

void State2Ddim::predict ()
{
    mX+=mVx;
    mY+=mVy;
}

ostream& State2Ddim::print(ostream& out)
{
    out<<mX<<" "<<mY<<" "<<mVx<<" "<<mVy<<" "<<mDx<<" "<<mDy<<" ";
    return out;
}

void State2Ddim::draw(QPainter& painter,double& dim)
{
    if (dim<=0)
        painter.drawRect ( QRectF (mX-mDx, mY-mDy, 2*mDx, 2*mDy));
    else
        painter.drawRect ( QRectF (mX-dim, mY-dim, 2*dim, 2*dim));
}

void State2Ddim::drawConnection(QPainter& painter,const shared_ptr<State> i)
{
    double xold=mX;
    double yold=mY;
    double xnew=dynamic_pointer_cast<const State2Ddim>(i)->mX;
    double ynew=dynamic_pointer_cast<const State2Ddim>(i)->mY;
    painter.drawLine ( QLineF(xold,yold,xnew,ynew) );
}

void State2Ddim::drawPoint(QPainter& painter)
{
    painter.drawPoint (QPointF(mX, mY));
}

void State2Ddim::drawText(QPainter& painter,QString text)
{
    painter.drawText( QPoint (mX, mY), text);
}





