#include "cluster.h"

Cluster::Cluster()
{
    selected=false;
}
Cluster::Cluster(ClusterPoint& p):Cluster()
{
    mPoints.clear();
    mPoints.push_back(p);
}
double Cluster::distance(ClusterPoint& p)
{
    double dist=DBL_MAX;
    for (unsigned int i=0; i<mPoints.size(); i++)
        dist=min(p.distance(mPoints[i]),dist);
    return dist;
}
void Cluster::add(ClusterPoint& p)
{
    mPoints.push_back(p);
}
double Cluster::distMax()
{
    ClusterPoint mean=getMean();

    double dist=0;
    for (unsigned int i=0; i<mPoints.size(); i++)
        dist=max(mean.distance(mPoints[i]),dist);
    return dist;

    return distance(mean);
}
ClusterPoint Cluster::getMean()
{
    ClusterPoint mean;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        mean=mean+mPoints[i];
    }
    mean/=(double)mPoints.size();


    return mean;
}

ClusterPoint Cluster::getMin()
{
    ClusterPoint min;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        min.mX=std::min(min.mX,mPoints[i].mX);
        min.mY=std::min(min.mY,mPoints[i].mY);
        min.mZ=std::min(min.mZ,mPoints[i].mZ);
    }

    return min;
}

ClusterPoint Cluster::getMax()
{
    ClusterPoint max;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        max.mX=std::max(max.mX,mPoints[i].mX);
        max.mY=std::max(max.mY,mPoints[i].mY);
        max.mZ=std::max(max.mZ,mPoints[i].mZ);
    }

    return max;
}

ClusterPoint Cluster::getMeanTransform(bool inv)
{
    ClusterPoint mean;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        double x,y,z;
        filereader::planeTransform(mPoints[i].mX,mPoints[i].mY,mPoints[i].mZ,x,y,z,inv);
        mean.mX=mean.mX+x;
        mean.mY=mean.mY+y;
        mean.mZ=mean.mZ+z;
    }
    mean/=(double)mPoints.size();


    return mean;
}

ClusterPoint Cluster::getMinTransform(bool inv)
{
    ClusterPoint min;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        double x,y,z;
        filereader::planeTransform(mPoints[i].mX,mPoints[i].mY,mPoints[i].mZ,x,y,z,inv);
        min.mX=std::min(min.mX,x);
        min.mY=std::min(min.mY,y);
        min.mZ=std::min(min.mZ,z);
    }

    return min;
}

ClusterPoint Cluster::getMaxTransform(bool inv)
{
    ClusterPoint max;

    for (unsigned int i=0; i<mPoints.size(); i++)
    {
        double x,y,z;
        filereader::planeTransform(mPoints[i].mX,mPoints[i].mY,mPoints[i].mZ,x,y,z,inv);
        max.mX=std::max(max.mX,x);
        max.mY=std::max(max.mY,y);
        max.mZ=std::max(max.mZ,z);
    }

    return max;
}

double Cluster::dispersion()
{
    ClusterPoint mean=getMean();

    double dist=0;
    for (unsigned int i=0; i<mPoints.size(); i++)
        dist+=mean.distance(mPoints[i]);
    dist/=mPoints.size();
    dist/=mean.distanceRad();
    return dist;
}
vector<Cluster> clustering::clusterisationPCL(vector<ClusterPoint> &points)
{

    vector<Cluster> clusters;
/*
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>), cloud_f (new pcl::PointCloud<pcl::PointXYZ>);
    for (unsigned int i=0; i<points.size(); i++)
    {
        cloud->push_back(pcl::PointXYZ(points[i].mX, points[i].mY, points[i].mZ));
    }
    pcl::SACSegmentation<pcl::PointXYZ> seg(false);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_plane (new pcl::PointCloud<pcl::PointXYZ> ());



    pcl::VoxelGrid<pcl::PointXYZ> vg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZ>);
    vg.setInputCloud (cloud);
    vg.setLeafSize (0.01f, 0.01f, 0.01f);
    vg.filter (*cloud_filtered);
    std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl;


    //pcl::PCDWriter writer;
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (100);
    seg.setDistanceThreshold (0.02);



    int i=0, nr_points = (int) cloud_filtered->points.size ();
    while (cloud_filtered->points.size () > 0.3 * nr_points)
    {
        // Segment the largest planar component from the remaining cloud
        seg.setInputCloud (cloud_filtered);
        seg.segment (*inliers, *coefficients);
        if (inliers->indices.size () == 0)
        {
          std::cout << "Could not estimate a planar model for the given dataset." << std::endl;
          break;
        }

        // Extract the planar inliers from the input cloud
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud (cloud_filtered);
        extract.setIndices (inliers);
        extract.setNegative (false);

        // Get the points associated with the planar surface
        extract.filter (*cloud_plane);
        std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;

        // Remove the planar inliers, extract the rest
        extract.setNegative (true);
        extract.filter (*cloud_f);
        *cloud_filtered = *cloud_f;
    }

        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
        tree->setInputCloud (cloud_filtered);

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
        ec.setClusterTolerance (0.02); // 2cm
        ec.setMinClusterSize (100);
        ec.setMaxClusterSize (25000);
        ec.setSearchMethod (tree);
        ec.setInputCloud (cloud_filtered);
        ec.extract (cluster_indices);

        int j = 0;

        for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
        {
          pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
          clusters.push_back(Cluster());
          for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
          {
            cloud_cluster->points.push_back (cloud_filtered->points[*pit]);
            ClusterPoint point;
            point.mX=cloud_filtered->points[*pit].x;
            point.mY=cloud_filtered->points[*pit].y;
            point.mZ=cloud_filtered->points[*pit].z;
            clusters.back().add(point);
          }
          cloud_cluster->width = cloud_cluster->points.size ();
          cloud_cluster->height = 1;
          cloud_cluster->is_dense = true;
          j++;
        }
*/
    return clusters;
}
vector<Cluster> clustering::clusterisation(vector<ClusterPoint> &points)
{
    double minV;
    int minK;
    double dist;
    double threshold=1000;//1.5;//1.0*0.02;
    vector<Cluster> clusters;

    while(!points.empty())
    {
        minV=DBL_MAX;
        minK=-1;
        for (unsigned int k=0; k< clusters.size(); k++)
        {
            dist=clusters[k].distance(*points.begin());// /points.begin()->distanceRad();
            if (dist<minV)
            {
                minV=dist;
                minK=k;
            }
        }
        if (minV<threshold)
        {
            clusters[minK].add(*points.begin());
        }
        else
        {
            clusters.push_back(Cluster(*points.begin()));
        }
        points.erase (points.begin());
    }
    return clusters;
}
vector<Cluster> clustering::clusterMerging(vector<Cluster>& clusters)
{
    /*vector<vector<int> > indexes;
    double coef=1.0;
    for (unsigned int i=0; i<clusters.size(); i++)
    {
        indexes.push_back(vector<int>());
        for (unsigned int j=i+1; j<clusters.size(); j++)
        {
            if (clusters[i].mean()-clusters[j].mean()<coef*(clusters[i].dispertion()+clusters[j].dispersion()))
            {
                indexes.back().push_back(j);
            }
        }
    }
    vector<int> erased;
    for (unsigned int i=clusters.size()-1; i>1; i--)
    {
        for (unsigned int j=indexes[i].size()-1; j>0; j--)
        {
            clusters[i].absorb(clusters[j]);
            erased
        }
    }*/
    return clusters;
}
vector<ClusterPoint> clustering::transform(vector<ClusterPoint>& points)
{

    vector<ClusterPoint> newpoints;
    double todeg=57.2957795;
    for (unsigned int i=0; i<points.size(); i++)
    {
        ClusterPoint point;
        point.mZ=atan(points[i].mX/points[i].mZ)*todeg;
        point.mX=points[i].distanceRad();
        point.mY=0;
        newpoints.push_back(point);
    }
    return newpoints;

}

vector<ClusterPoint> clustering::transformInv(vector<ClusterPoint>& points)
{
    vector<ClusterPoint> newpoints;
    double todeg=57.2957795;
    for (unsigned int i=0; i<points.size(); i++)
    {
        ClusterPoint point;
        point.mZ=points[i].mX*cos(points[i].mZ/todeg);
        point.mX=points[i].mX*sin(points[i].mZ/todeg);
        point.mY=0;
        newpoints.push_back(point);
    }
    return newpoints;

}
vector<ClusterPoint> clustering::filterPolygon(vector<ClusterPoint>& points)
{
    vector<ClusterPoint> polygon;
    if (points.size()>0)
    {
        Kalman filter;
        double x=points[0].mZ;
        double y=points[0].mX;
        shared_ptr<State> state, out;
        state=make_shared<State2D>();
        vector<double> data;

        data.push_back(x);
        data.push_back(y);
        data.push_back(0);
        data.push_back(0);
        state->set(data);
        filter.setVector(state);

        for (unsigned int i=0; i<points.size(); i++)
        {

            state=make_shared<State2D>();
            data.clear();

            data.push_back(points[i].mZ);
            data.push_back(points[i].mX);
            data.push_back(0);
            data.push_back(0);
            state->set(data);

            out=filter.evaluate(state);

            data.clear();
            out->get(data);

            x=data[0];
            y=data[1];
            ClusterPoint point;
            point.mZ=x;
            point.mX=y;
            polygon.push_back(point);
        }
    }
    return polygon;
}
