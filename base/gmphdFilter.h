#ifndef PHDFILTER_H
#define PHDFILTER_H

#include <vector>
#include <random>
#include <chrono>
#include <cfloat>
#include <algorithm>
#include "state.h"
#include "detection.h"
#include "particle.h"
#include "gaussian.h"

using namespace std;

class GMPHDFilter
{
private:
	vector<Detection> mObservations;
	vector<Target*> mTargets;
	vector<Gaussian> mGaussians;
	int targetHistoryLength;

	vector<bool> mUsed;
	default_random_engine mGenerator;
	double mPfn;
	double mPb;
	double mPd;
	double mPs;
	int idNum;

public:
	double mAssociationCoef;
	double mResampleCoef;
	double mInfluenceCoef;
	vector<double> mSigmas;
public:
	GMPHDFilter();
	~GMPHDFilter();
	void resample2();
	void initialize(const vector<double>& sigmas, const vector<bool>& used, double pfn, int historyLength);
	void prediction();
	void association(ostream& out);
	void merge(ostream& out);
	void observation(vector<Detection> detections);
	vector<Target> correction();
	vector<Target> evaluate(vector<Detection> state,ostream& associations, ostream& unions);
	int getTargetsNumber();
	void printParticles(ostream& out);
};






#endif /* PHDFILTER_H_ */
