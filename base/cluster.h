#ifndef CLUSTER_H
#define CLUSTER_H


#include <math.h>
#include <vector>
#include <cfloat>
#include <iostream>
#include "filereader.h"
#include "kalman.h"


//#include <pcl/ModelCoefficients.h>
//#include <pcl/point_types.h>
//#include <pcl/common/projection_matrix.h>
//#include <pcl/io/pcd_io.h>
//#include <pcl/filters/extract_indices.h>
//#include <pcl/filters/voxel_grid.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/kdtree/kdtree.h>
//#include <pcl/sample_consensus/method_types.h>
//#include <pcl/sample_consensus/model_types.h>
//#include <pcl/segmentation/sac_segmentation.h>
//#include <pcl/segmentation/extract_clusters.h>

using namespace std;



class Cluster
{
public:
    vector<ClusterPoint> mPoints;
    bool selected;
public:
    Cluster();
    Cluster(ClusterPoint& p);
    double distance(ClusterPoint& p);
    void add(ClusterPoint& p);
    double distMax();
    ClusterPoint getMean();
    ClusterPoint getMin();
    ClusterPoint getMax();
    ClusterPoint getMeanTransform(bool inv);
    ClusterPoint getMinTransform(bool inv);
    ClusterPoint getMaxTransform(bool inv);
    double dispersion();

};
namespace clustering
{
    vector<Cluster> clusterisationPCL(vector<ClusterPoint> &points);
    vector<Cluster> clusterisation(vector<ClusterPoint> &points);
    vector<Cluster> clusterMerging(vector<Cluster>& clusters);
    vector<ClusterPoint> transform(vector<ClusterPoint>& points);
    vector<ClusterPoint> transformInv(vector<ClusterPoint>& points);
    vector<ClusterPoint> filterPolygon(vector<ClusterPoint>& points);
}

#endif // CLUSTER_H
