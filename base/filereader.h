#ifndef FILEREADER_H_
#define FILEREADER_H_

#include <string>
#include "tracklets.h"
#include "detection.h"
#include "point.h"
#include "matrix.h"
#include "som.h"
#include <dirent.h>
#include "state2D.h"
#include "state2Ddim.h"

class ClusterPoint
{
public:
    double mX;
    double mY;
    double mZ;
    int    mLink;
    ClusterPoint()
    {
        mX=0;
        mY=0;
        mZ=0;
    };
    ~ClusterPoint(){};
    ClusterPoint operator+ (ClusterPoint& p)
    {
        ClusterPoint out;
        out.mX=this->mX+p.mX;
        out.mY=this->mY+p.mY;
        out.mZ=this->mZ+p.mZ;
        return out;
    }
    void operator/= (double d)
    {
        mX/=d;
        mY/=d;
        mZ/=d;
    }
    double distanceRad()
    {
        return sqrt(pow(this->mX,2)+pow(this->mY,2)+pow(this->mZ,2));
    }

    double distance(ClusterPoint& p)
    {
        return sqrt(pow(this->mX-p.mX,2)+pow(this->mY-p.mY,2)+pow(this->mZ-p.mZ,2));
    }
    bool operator< (const ClusterPoint& j) const
    {
        return (atan(this->mX/this->mZ)<atan(j.mX/j.mZ));
    }
};

namespace filereader
{
	void trackletsToPointsWithTransform(vector<vector<Point> > *points, int& nF, string trackletsFileName, string oxtsFolderName);
	void fieldToPoints(vector<vector<Point> > *points, string fieldFileName);
	void pointsToField(vector<vector<Point> > *points, string fieldFileName);
    void trackletsToDetectionsWithTransform(vector<Detection> &detections, Tracklets *tracklets, CarData &cardata, int nFrame, bool transform);
    void linesToDetections(vector<Detection> &detections, ifstream &data, string &line);
    void laserToClusterPoints(vector<ClusterPoint> &points, string &dataFolder, string &line, int nFrame);
	void pointsToBinField(vector<vector<Point> > *points, string fieldFileName, double xmin, double ymin, double xmax, double ymax, double distLimit);
    void trackletsToSom(string trackletsFolderName, vector<vector<SomPoint>>& data);
    void lasToSom(string folderName, vector<vector<SomPoint>>& data);
    void camToSom(string folderName, vector<vector<SomPoint>>& data);
    void camLasToSom(string folderName, vector<vector<SomPoint>>& data);
    void camToSomOne(string folderName, int n, vector<vector<SomPoint>>& data);
    void lidar2img(double lx, double ly, double lz, double& x, double& y);
    void planeTransform(double lx, double ly,double lz, double & x, double & y, double& z,bool inv);
}

#endif /* FILEREADER_H_ */

