/*
 * module.h
 *
 *  Created on: Nov 21, 2014
 *      Author: egor
 */

#ifndef SRC_MODULE_H_
#define SRC_MODULE_H_


#include "feeder.h"
#include "configuration.h"


class Module
{
public:
	Feeder feeder;
	PHDFilter filter;
	ofstream unions, filtered, particles, associations;
	string line;
    string imageName;
	vector<Target> targets;
	vector<Detection> detections;
public:
	Module();
	int configurate(Configuration::ModuleConfig& config, vector<shared_ptr<Module> >& modules);
	int execute();
	~Module();
};



#endif /* SRC_MODULE_H_ */
