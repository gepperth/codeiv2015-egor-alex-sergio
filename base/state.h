
#ifndef STATE_H
#define STATE_H

#include <math.h>
#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <memory>
#include <QPainter>

using namespace std;


class Direction
{
public:
    vector<double> mD;
    double mWeight;
};

class State
{

public:

    virtual void set(const vector<double> & data)=0;
    virtual void get(vector <double> & data) const=0;
    virtual void add( const shared_ptr<State> i)=0 ;
    virtual void sub( const shared_ptr<State> i)=0 ;
    virtual void divBy(double d)=0 ;
    virtual shared_ptr<State> copy() =0;

    virtual void setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end)=0;
    virtual void predict ()=0;
    virtual double getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const=0;
    virtual void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator)=0;

    virtual std::ostream& print(std::ostream& out)=0 ;
    virtual void draw(QPainter& ,double& )=0;
    virtual void drawConnection(QPainter& ,const shared_ptr<State> )=0;
    virtual void drawPoint(QPainter& )=0;
    virtual void drawText(QPainter& ,QString )=0;
};

#endif
