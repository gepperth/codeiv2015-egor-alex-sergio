#ifndef PARTICLE_FILTER_H
#define PARTICLE_FILTER_H


#include <stdio.h>
#include <vector>
#include <list>
#include <random>
#include <chrono>
#include <algorithm>
#include "state.h"
#include "particle.h"
#include "target.h"


using namespace std;

class ParticleFilter
{
public:
	vector<Particle> mParticles;
	int nParticles;
	Target mTarget;
	default_random_engine mGenerator;
	vector<double> mSigmas;
	double mResampleCoef;
	double mInfluenceCoef;

public:
	ParticleFilter();
	~ParticleFilter();
	void resample();
    void initialize(const shared_ptr<State> state, int num_particles, int history_length, vector<double>& sigmas);
	void prediction();
    //void modifyWithDirection();
    void observation(const shared_ptr<State> state);
    shared_ptr<State> correction();
    shared_ptr<State> evaluate(const shared_ptr<State> state);
	int getParticlesNumber();
	Particle getParticle(int number);
	vector<Particle> getParticles();
	void setValidity(int numDirections);
	void printParticles(ostream& out);
};

#endif
