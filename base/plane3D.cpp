#include "plane3D.h"

/*
Plane3D::Plane3D(double a, double b, double c, double d) : Coordinate()// : Coordinate(center, dimension, direction)
{
	mA=a;
	mB=b;
	mC=c;
	mD=d;
}
Plane3D::Plane3D(const shared_ptr<Coordinate> i) : Coordinate(i)
{
	const shared_ptr<Plane3D> si=dynamic_pointer_cast<Plane3D>(i);
	mA=si->mA;
	mB=si->mB;
	mC=si->mC;
	mD=si->mD;
}
Plane3D::~Plane3D() {};
shared_ptr<Coordinate> Plane3D::operator+(const shared_ptr<Coordinate> i) const
{
	const shared_ptr<Plane3D> s=dynamic_pointer_cast<Plane3D>(i);
	shared_ptr<Plane3D> coordinate=make_shared<Plane3D>(this->mA+s->mA,
											this->mB+s->mB,
											this->mC+s->mC,
											this->mD+s->mD);
	return coordinate;
}
shared_ptr<Coordinate> Plane3D::operator-(const shared_ptr<Coordinate> i) const
{
	const shared_ptr<Plane3D> s=dynamic_pointer_cast<Plane3D>(i);
	shared_ptr<Plane3D> coordinate=make_shared<Plane3D>(this->mA-s->mA,
											this->mB-s->mB,
											this->mC-s->mC,
											this->mD-s->mD);
	return coordinate;
}
shared_ptr<Coordinate> Plane3D::operator/=(double d)
{
	this->mA/=d;
	this->mB/=d;
	this->mC/=d;
	this->mD/=d;
	return make_shared<Plane3D>(*this);
}
shared_ptr<Coordinate> Plane3D::operator-()
{
	shared_ptr<Coordinate> coordinate=make_shared<Plane3D>(-this->mA,
											-this->mB,
											-this->mC,
											-this->mD);
	return coordinate;
}
shared_ptr<Coordinate> Plane3D::copy()
{
	return make_shared<Plane3D>(*this);
}
double Plane3D::getGaussian(const vector<double>& sigmas,bool sigmaCorr) const
{
	double mult=1.0;


	double ex=(mA*mA+mB*mB+mC*mC*+mD*mD)/(2.0*sigmas[0]*sigmas[0]*mult*mult);

	return exp(-ex)*1000000;
}
double Plane3D::getSquare() const
{
	return mA*mA+mB*mB+mC*mC+mD*mD;
}
void Plane3D::shift(double shift)
{
	//mX+=shift;
	//mY+=shift;
}
void Plane3D::setDynamics(const shared_ptr<Coordinate> begin, const shared_ptr<Coordinate> end)
{
	//const shared_ptr<Plane3D> b=dynamic_pointer_cast<Plane3D>(begin);
	//const shared_ptr<Plane3D> e=dynamic_pointer_cast<Plane3D>(end);
	//mVx=e->mX-b->mX;
	//mVy=e->mY-b->mY;
}
void Plane3D::keepRatio(const shared_ptr<Coordinate> i, double ratio)
{

}

void Plane3D::randomFluctuate(default_random_engine &generator,const vector<double>& sigmas)
{

	normal_distribution<double> distributionCoord(0.0,sigmas[0]);

	mA+=distributionCoord(generator);
	mB+=distributionCoord(generator);
	mC+=distributionCoord(generator);

}
void Plane3D::randomFluctuateDir(default_random_engine &generator,const vector<double>& sigmas, const Direction& dir)
{
	//mVx=dir.mD[0];
	//mVy=dir.mD[1];

	randomFluctuate(generator,sigmas);
}
void Plane3D::modifyWithDirection(const Direction& dir)
{
	//mVx=dir.mD[0];
	//mVy=dir.mD[1];
	//mX+=dir.mD[0];
	//mY+=dir.mD[1];
	//cout<<mVx<<" "<<mVy<<endl;
}

void Plane3D::mean(const shared_ptr<Coordinate> i, const shared_ptr<Coordinate> j)
{
	*this=*dynamic_pointer_cast<Plane3D>(i)+dynamic_pointer_cast<Plane3D>(j);
	*this/=2.0;
}
shared_ptr<Coordinate> Plane3D::predict()
{
	//mX+=mVx;
	//mY+=mVy;
	return make_shared<Plane3D>(*this);
}
ostream& Plane3D::print(ostream& out)
{
	out<<mA<<" "<<mB<<" "<<mC<<" "<<mD<<" ";
	return out;
}
*/
/*
void Cartesian2D::draw(QPainter& painter, double& dim)
{
	painter.drawRect ( QRectF (mX-dim, mY-dim, 2*dim, 2*dim));
}
void Cartesian2D::drawConnection(QPainter& painter,const shared_ptr<Coordinate> i)
{
	double xold=mX;
	double yold=mY;
	double xnew=dynamic_pointer_cast<const Cartesian2D>(i)->mX;
	double ynew=dynamic_pointer_cast<const Cartesian2D>(i)->mY;
	painter.drawLine ( QLineF(xold,yold,xnew,ynew) );
}
void Cartesian2D::drawPoint(QPainter& painter)
{
	painter.drawPoint (QPointF(mX, mY));
}
void Cartesian2D::drawText(QPainter& painter,QString text)
{
	painter.drawText( QPoint (mX, mY), text);
}
*/





