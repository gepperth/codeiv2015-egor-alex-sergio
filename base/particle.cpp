#include "particle.h"

Particle::Particle()
{
	mWeight=0.0;
	mTarget=NULL;
}
Particle::~Particle()
{

}
void Particle::setState(shared_ptr<State> state)
{
    mState=state->copy();
}
shared_ptr<State> Particle::getState() const
{
    return mState->copy();
}
void Particle::setWeight(double weight)
{
	mWeight=weight;
}
double Particle::getWeight() const
{
	return mWeight;
}
void Particle::setTarget(Target* target)
{
	mTarget=target;
}
Target* Particle::getTarget() const
{
	return mTarget;
}

void Particle::operator=(Particle in)
{
    this->mState=in.mState->copy();
	this->mWeight=in.mWeight;
	this->mTarget=in.mTarget;
}

bool Particle::operator() (Particle i, Particle j)
{
    return ( i.mWeight > j.mWeight );
}

bool Particle::operator< (Particle i)
{
    return ( this->mWeight < i.mWeight );
}
std::ostream& operator<< (std::ostream& out, Particle& particle)
{
    //return out<<particle.mState;
    particle.mState->print(out);
    return out;
}


