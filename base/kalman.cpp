#include "kalman.h"


Kalman::Kalman() : H(4,4),Q(4,4),F(4,4),R(4,4),P(4,4)
{
	reset();
}
Kalman::~Kalman()
{

}
void Kalman::reset()
{
    int N=4;
	Matrix eye(N,N);
    for (int i=0; i<N; i++)
    	eye.M[N*i+i]=1;
	H=eye;
	F.M[N*2+2]=0;
	F.M[N*3+3]=0;
	F=eye;


    for (int i=0; i<N; i++)
        eye.M[N*i+i]=2000;//200;//15;

	R=eye;
	P=eye;

    for (int i=0; i<N; i++)
    	eye.M[N*i+i]=1;
	Q=eye;

	vector<double> v;
    for (int i=0; i<N; i++)
    	v.push_back(0);
	x.set(v);
}
void Kalman::setVector(shared_ptr<State> z)
{
    x=state2vector(z);
}

Vector Kalman::state2vector(shared_ptr<State> z)
{
    shared_ptr<State2D> s1=dynamic_pointer_cast<State2D>(z);

	Vector v(4);
    v[0]=s1->mX;
    v[1]=s1->mY;
    v[2]=s1->mVx;
    v[4]=s1->mVy;

	return v;
}
shared_ptr<State> Kalman::vector2state(Vector v)
{
    shared_ptr<State2D> state=make_shared<State2D>();
    vector<double> data;
    for (int i=0; i<4; i++)
        data.push_back(v[i]);
    state->set(data);
	return state;
}
shared_ptr<State> Kalman::evaluate(shared_ptr<State> z)
{
	predict();
	return update(z);
}
shared_ptr<State> Kalman::predict()
{
	x=F*x;
	P=F*P*F.transpose()+Q;
	return vector2state(x);
}
shared_ptr<State> Kalman::update(shared_ptr<State> z)
{
	int N=P.Ncol;
	Matrix I(N,N);
    for (int i=0; i<N; i++)
    	I.M[N*i+i]=1;
	Vector y=state2vector(z)-H*x;
	Matrix S=H*P*H.transpose()+R;
	Matrix K=P*H.transpose()*S.invert();

	x=x+K*y;
	P=(I-K*H)*P;

	return vector2state(x);
}




