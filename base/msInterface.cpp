#include "msInterface.h"



MSInterface::MSInterface()
{

}
MSInterface::~MSInterface()
{

}


int MSInterface::initialize(vector<string> args)
{
	if(!config.loadFromFile (args))
		cout<<"BAD CONFIG"<<endl;

	modules.clear();
	int min=0;
	for (unsigned int i=0; i<config.windows.size(); i++)
	{
		modules.push_back(make_shared<Module>());
		min=std::min(modules[i]->configurate(config.windows[i],modules),min);
	}

	return min;
}


int MSInterface::execute()
{
    int limit=-1;
	for (unsigned int i=0; i<config.windows.size(); i++)
        limit=std::max(limit,modules[i]->execute());
	return limit;
}




