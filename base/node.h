#ifndef NODE_H
#define NODE_H

#include "stdio.h"
#include <random>
#include <iostream>
#include <vector>
#include <cfloat>
//#include <chrono>

using namespace std;


class SomPoint
{
public:
    double camX;
    double camY;
    double camW;
    double camH;

    double lasX;
    double lasY;
    double lasZ;
    double lasSize;
    double lasDisp;

    SomPoint(){};
    ~SomPoint(){};
};


class Node
{
public:

    vector<double> coords;
    vector<vector<double> > weights;
    vector<pair<int,int> > sizes;

    Node()
    {
    }
    Node(const vector<double>& coordinates,const vector<pair<int,int> >& ssizes)
    {
        coords=coordinates;
        sizes=ssizes;
        weights.assign(sizes.size(),vector<double>());
        for (unsigned int i=0; i<sizes.size(); i++)
            weights[i].assign(sizes[i].first*sizes[i].second,0.01);

    }

    void addWeights(int nSom, int I, int J)
    {
        double sigma=0.07;
        //double lambda=0.01; //0.001
        int radius=2;

        int W=sizes[nSom].first;
        int H=sizes[nSom].second;

        for (int i=max(0,I-radius); i<min(W, I+radius+1); i++)
        {
            for (int j=max(0,J-radius); j<min(H, J+radius+1); j++)
            {
                double gauss=exp((-pow(double(I-i)/max(1.0,W-1.0),2.0)
                                 -pow(double(J-j)/max(1.0,H-1.0),2.0))
                                 /pow(sigma,2.0));
                //cout<<"gauss "<<gauss<<endl;
                weights[nSom][H*i+j]+=gauss;
                //weights[H*i+j]=weights[H*i+j]*(1.0-lambda)+gauss*lambda;
            }
        }
    }
    void normalize()
    {
        for (unsigned int j=0; j<weights.size(); j++)
        {
            double sum=0;
            for (int i=0; i<(int)weights[j].size(); i++)
            {
                sum+=weights[j][i];
            }
            for (int i=0; i<(int)weights[j].size(); i++)
            {
                weights[j][i]/=sum;
            }
        }
    }
};





#endif // NODE_H
