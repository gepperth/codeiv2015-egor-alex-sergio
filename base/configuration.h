#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include <QDomDocument>
#include <QDomElement>
#include <QFile>

using namespace std;

class Configuration
{
public:
	class ModuleConfig
	{
	public:
		bool present;
		string mergedFile;
		string associatedFile;
		string filteredFile;
		string particleFile;
		string dataFile;
		string fieldFile;
		string cardata;
		vector<double> sigmas;
		double Pfn;
		double Pfb;
		double Pfd;
		int particlesPerTarget;
		int historyLength;
		bool mergeDetections;
		bool useTracklets;
		string framesIn;
		string framesOut;
		vector<int> sizes;
        int numSensor;
		double drawWidth;
        double associationCoef;
        double resampleCoef;
        double influenceCoef;
		bool useDimensions;
		void load(QDomElement e)
		{
			mergedFile=e.namedItem("mergedFile").toElement().text().toStdString();
			associatedFile=e.namedItem("associatedFile").toElement().text().toStdString();
			filteredFile=e.namedItem("filteredFile").toElement().text().toStdString();
			particleFile=e.namedItem("particleFile").toElement().text().toStdString();
			dataFile=e.namedItem("dataFile").toElement().text().toStdString();
			fieldFile=e.namedItem("fieldFile").toElement().text().toStdString();
		   	cardata=e.namedItem("carData").toElement().text().toStdString();
	    	string strtmp;
	    	strtmp=e.namedItem("sigmas").toElement().text().toStdString();
	    	sigmas.clear();
	    	stringstream insigmas(strtmp);
	    	double i;
	    	while (insigmas >> i) sigmas.push_back(i);
		   	Pfn=e.namedItem("Pfn").toElement().text().toDouble();
		   	Pfb=e.namedItem("Pfb").toElement().text().toDouble();
	    	Pfd=e.namedItem("Pfd").toElement().text().toDouble();
            associationCoef=e.namedItem("associationCoef").toElement().text().toDouble();
            resampleCoef=e.namedItem("resampleCoef").toElement().text().toDouble();
            influenceCoef=e.namedItem("influenceCoef").toElement().text().toDouble();
	    	particlesPerTarget=e.namedItem("particlesPerTarget").toElement().text().toInt();
	    	historyLength=e.namedItem("historyLength").toElement().text().toInt();
		   	mergeDetections=e.namedItem("mergeDetections").toElement().text().toInt();
	    	stringstream dataFileStream(dataFile);
	    	string ext;
	    	while (getline(dataFileStream, ext, '.'));
	    	if (ext=="xml")
	    		useTracklets=true;
	    	else
		   		useTracklets=false;

			framesIn=e.namedItem("framesIn").toElement().text().toStdString();
			framesOut=e.namedItem("framesOut").toElement().text().toStdString();

			strtmp=e.namedItem("sizes").toElement().text().toStdString();
			sizes.clear();
			stringstream in(strtmp);
			int j;
			while (in >> j) sizes.push_back(j);
            numSensor=e.namedItem("sensor").toElement().text().toInt();
			drawWidth=e.namedItem("drawWidth").toElement().text().toDouble();
			present=e.namedItem("present").toElement().text().toInt();
			useDimensions=e.namedItem("useDimensions").toElement().text().toInt();
		};
		void show()
		{
		      cout<<"Window:"<<endl;
			  cout<<"	present: "<<present<<endl;
			  cout<<"	mergedFile "<<mergedFile<<endl;
			  cout<<"	associatedFile "<<associatedFile<<endl;
			  cout<<"	filteredFile "<<filteredFile<<endl;
			  cout<<"	particleFile "<<particleFile<<endl;
			  cout<<"	dataFile "<<dataFile<<endl;
			  cout<<"	fieldFile "<<fieldFile<<endl;
	 		  cout<<"	carData "<<cardata<<endl;
	 		  cout<<"	sigmas ";
			  for (int i=0; i<(int)sigmas.size(); i++)
				  cout<<sigmas[i]<<" ";
			  cout<<endl;
	 		  cout<<"	Pfn "<<Pfn<<endl;
			  cout<<"	Pfb "<<Pfb<<endl;
	 		  cout<<"	Pfd "<<Pfd<<endl;
              cout<<"	associationCoef "<<associationCoef<<endl;
              cout<<"	resampleCoef "<<resampleCoef<<endl;
              cout<<"	influenceCoef "<<influenceCoef<<endl;
			  cout<<"	particlesPerTarget "<<particlesPerTarget<<endl;
	 		  cout<<"	historyLength "<<historyLength<<endl;
	 		  cout<<"	mergeDetections "<<mergeDetections<<endl;
			  cout<<"	useTracklets "<<useTracklets<<endl;
		      cout<<"	Size: "<<sizes[0]<<" "<<sizes[1]<<" "<<sizes[2]<<" "<<sizes[3]<<endl;
              cout<<"	Sensor#: "<<numSensor<<endl;
		      cout<<"	framesIn "<<framesIn<<endl;
		      cout<<"	framesOut "<<framesOut<<endl;
		      cout<<"	useDimensions "<<useDimensions<<endl;
		};

	};

	vector<ModuleConfig> windows;
    void parsing(vector<string> args)
    {
    	for (int i=1; i<(int)args.size()-1; i++)
    	{
    		if (args[i]== "mergedFile") {windows[i+1].mergedFile=args[i+2];}
    		else if (args[i]== "associatedFile") {windows[i+1].associatedFile=args[i+2];}
    		else if (args[i]== "filteredFile") {windows[i+1].filteredFile=args[i+2];}
    		else if (args[i]== "particleFile") {windows[i+1].particleFile=args[i+2];}
    		else if (args[i]== "dataFile") {windows[i+1].dataFile=args[i+2];}
    		else if (args[i]== "fieldFile") {windows[i+1].fieldFile=args[i+2];}
    		else if (args[i]== "carData") {windows[i+1].cardata=args[i+2];}
    		else if (args[i]== "Pfn") {windows[i+1].Pfn=std::stof(args[i+2]);}
    		else if (args[i]== "Pfb") {windows[i+1].Pfb=std::stof(args[i+2]);}
    		else if (args[i]== "Pfd") {windows[i+1].Pfd=std::stof(args[i+2]);}
            else if (args[i]== "associationCoef") {windows[i+1].associationCoef=std::stof(args[i+2]);}
            else if (args[i]== "resampleCoef") {windows[i+1].resampleCoef=std::stof(args[i+2]);}
            else if (args[i]== "influenceCoef") {windows[i+1].influenceCoef=std::stof(args[i+2]);}
    		else if (args[i]== "particlesPerTarget") {windows[i+1].particlesPerTarget=std::stoi(args[i+2]);}
    		else if (args[i]== "historyLength") {windows[i+1].historyLength=std::stoi(args[i+2]);}
    		else if (args[i]== "mergeDetections") {windows[i+1].mergeDetections=std::stoi(args[i+2]);}
    		else cout<<"Unknown parameter "<<args[i]<<endl;
    		i+=2;
    	}
    }
	bool loadFromFile (vector<string> args)
	{
	    QDomElement docElem,root;
	    QDomDocument xmldoc;
	    QString errorMsg;
	    int col,row;
	    string filename=args[0];
	    QFile file(QString::fromStdString(filename));
	    if (file.open(QIODevice::ReadOnly))
	    {
	    	xmldoc.setContent(&file,false,&errorMsg,&row,&col);
	    	root=xmldoc.documentElement();

	    	QDomElement e = root.firstChildElement( "window" );
	    	for ( ; !e.isNull(); e = e.nextSiblingElement( "window" ) )
	    	{
	    		ModuleConfig w;
	    		w.load(e);
	    		windows.push_back(w);
	    	}

	    	parsing(args);

	    	show();
	    }
	    return true;

	  }
	void show()
	{
		  for (int i=0; i<(int)windows.size(); i++)
			  windows[i].show();
	}
};



#endif /* CONFIGURATION_H_ */
