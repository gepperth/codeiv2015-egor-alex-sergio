#include "state3D.h"

using namespace std;

State3D::State3D()
{

}

void State3D::add(const shared_ptr<State> i)
{
    const shared_ptr<State3D> s=dynamic_pointer_cast<State3D>(i);
    mX+=s->mX;
    mY+=s->mY;
    mZ+=s->mZ;
    mVx+=s->mVx;
    mVy+=s->mVy;
    mVz+=s->mVz;
}

void State3D::sub(const shared_ptr<State> i)
{
    const shared_ptr<State3D> s=dynamic_pointer_cast<State3D>(i);
    mX-=s->mX;
    mY-=s->mY;
    mZ-=s->mZ;
    mVx-=s->mVx;
    mVy-=s->mVy;
    mVz-=s->mVz;
}

void State3D::divBy(double d)
{
    mX/=d;
    mY/=d;
    mZ/=d;
    mVx/=d;
    mVy/=d;
    mVz/=d;
}

shared_ptr<State> State3D::copy()
{
    shared_ptr<State3D> copy=make_shared<State3D>();

    copy->mX=mX;
    copy->mY=mY;
    copy->mZ=mZ;
    copy->mVx=mVx;
    copy->mVy=mVy;
    copy->mVz=mVz;

    return copy;
}

void State3D::set(const vector<double> & data)
{
    if (data.size()!=6)
        cout<<"error data size"<<endl;
    mX=data[0];
    mY=data[1];
    mZ=data[2];
    mVx=data[3];
    mVy=data[4];
    mVz=data[5];

}

void State3D::get(vector <double> & data) const
{
    data.clear();
    data.push_back(mX);
    data.push_back(mY);
    data.push_back(mZ);
    data.push_back(mVx);
    data.push_back(mVy);
    data.push_back(mVz);
}

double State3D::getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const
{
    const shared_ptr<State3D> s=dynamic_pointer_cast<State3D>(i);
    double ex=(pow(mX-s->mX,2)+pow(mY-s->mY,2)+pow(mZ-s->mZ,2))/(2.0*sigmas[0]*sigmas[0]);
    ex+=(pow(mVx-s->mVx,2)+pow(mVy-s->mVy,2)+pow(mVz-s->mVz,2))/(2.0*sigmas[1]*sigmas[1]);

    return exp(-ex)*1000000;
}
void State3D::setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end)
{
    const shared_ptr<State3D> b=dynamic_pointer_cast<State3D>(begin);
    const shared_ptr<State3D> e=dynamic_pointer_cast<State3D>(end);
    mVx=e->mX-b->mX;
    mVy=e->mY-b->mY;
    mVz=e->mZ-b->mZ;
}

void State3D::applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator)
{
    normal_distribution<double> distributionCoord(0.0,sigmas[0]);
    normal_distribution<double> distributionSpeed(0.0,sigmas[1]);

    mX+=distributionCoord(generator);
    mY+=distributionCoord(generator);
    mZ+=distributionCoord(generator);
    mVx+=distributionSpeed(generator);
    mVy+=distributionSpeed(generator);
    mVz+=distributionSpeed(generator);
}

void State3D::predict ()
{
    mX+=mVx;
    mY+=mVy;
    mZ+=mVz;
}

ostream& State3D::print(ostream& out)
{
    out<<mX<<" "<<mY<<" "<<mZ<<" "<<mVx<<" "<<mVy<<" "<<mVz<<" ";
    return out;
}

void State3D::draw(QPainter& painter,double& dim)
{
    if (dim<=0)
        painter.drawPoint (QPointF(mX, mY));
    else
        painter.drawRect ( QRectF (mX-dim, mY-dim, 2*dim, 2*dim));
}

void State3D::drawConnection(QPainter& painter,const shared_ptr<State> i)
{
    double xold=mX;
    double yold=mY;
    double xnew=dynamic_pointer_cast<const State3D>(i)->mX;
    double ynew=dynamic_pointer_cast<const State3D>(i)->mY;
    painter.drawLine ( QLineF(xold,yold,xnew,ynew) );
}

void State3D::drawPoint(QPainter& painter)
{
    painter.drawPoint (QPointF(mX, mY));
}

void State3D::drawText(QPainter& painter,QString text)
{
    painter.drawText( QPoint (mX, mY), text);
}





