#ifndef KALMAN_H_
#define KALMAN_H_

#include "state2D.h"

#include "matrix.h"

class Kalman
{
	Vector x;

	Matrix H;
	Matrix Q;
	Matrix F;
	Matrix R;
	Matrix P;
public:
	Kalman();
	~Kalman();
    void reset();
    void setVector(shared_ptr<State> z);
    Vector state2vector(shared_ptr<State> z);
    shared_ptr<State> vector2state(Vector v);
    shared_ptr<State> evaluate(shared_ptr<State> z);
    shared_ptr<State> predict();
    shared_ptr<State> update(shared_ptr<State> z);
};


#endif /* KALMAN_H_ */
