#ifndef PLANE3D_H_
#define PLANE3D_H_

#include "state.h"

class Plane3D : virtual public State
{

public:
	double mA;
	double mB;
	double mC;
	double mD;
public:
    /*
	Plane3D(double a, double b, double c, double d);
	Plane3D(const shared_ptr<Coordinate> i);
	~Plane3D();
	shared_ptr<Coordinate> operator+(const shared_ptr<Coordinate> i) const;
	shared_ptr<Coordinate> operator-(const shared_ptr<Coordinate> i) const;
	shared_ptr<Coordinate> operator/=(double d);
	shared_ptr<Coordinate> operator-();
	shared_ptr<Coordinate> copy();
	double getGaussian(const vector<double>& sigmas,bool sigmaCorr) const;
	double getSquare() const;
	void shift(double shift);
	void setDynamics(const shared_ptr<Coordinate> begin, const shared_ptr<Coordinate> end);
	void keepRatio(const shared_ptr<Coordinate> i, double ratio);
	void randomFluctuate(default_random_engine &generator,const vector<double>& sigmas);
	void randomFluctuateDir(default_random_engine &generator,const vector<double>& sigmas, const Direction& dir);
	void modifyWithDirection(const Direction& dir);
	void mean(const shared_ptr<Coordinate> i, const shared_ptr<Coordinate> j);
	shared_ptr<Coordinate> predict();
	ostream& print(ostream& out);
	void draw(QPainter& painter,double& dim);
	void drawConnection(QPainter& painter,const shared_ptr<Coordinate> i);
	void drawPoint(QPainter& painter);
	void drawText(QPainter& painter,QString text);
    */
};




#endif /* PLANE3D_H_ */
