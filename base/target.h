#ifndef TARGET_H
#define TARGET_H


#include <list>
#include "state.h"

using namespace std;

class Target
{
private:
    list<shared_ptr<State>> mHistory;
	int mHistoryLength;
	int mID;
	double mProbability;
	list<double> mValidity;
public:
	double mModelForce;
 public:

    Target(int idNumber, int historyLength);
    Target ();
    void set(int idNumber, int historyLength);
    ~Target();
    Target operator=(const Target i);
    void setState(const shared_ptr<State> state);
    shared_ptr<State> getState() const;
    void setValidity(double validity);
    double getValidity() const;

    list<shared_ptr<State>> getHistory() const;
    list<double> getHistoryValidity() const;
    void setHistory(const list<shared_ptr<State>>& history);

    double intersection(const Target& i, const vector<double>& sigmas) const;
    Target merge(const Target& i) const;
    void setID(int id);
    int getID() const;
    void setProbability(double probability);
    double getProbability() const;

    friend std::ostream& operator<< (std::ostream& out, Target& target);
};


#endif /* TARGET_H_ */
