#ifndef STATE2D_H_
#define STATE2D_H_

#include "state.h"


class State2D : virtual public State
{

public:
    double mX;
    double mY;
    double mVx;
    double mVy;
public:
    State2D();
    void set(const vector<double> & data);
    void get(vector <double> & data) const;
    void add(const shared_ptr<State> i);
    void sub(const shared_ptr<State> i);
    void divBy(double d);
    shared_ptr<State> copy();

    void setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end);
    void predict ();
    double getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const;
    void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator);

    ostream& print(ostream& out);
    void draw(QPainter& painter,double& dim);
    void drawConnection(QPainter& painter,const shared_ptr<State> i);
    void drawPoint(QPainter& painter);
    void drawText(QPainter& painter,QString text);
};


#endif /* CARTESIAN2D_H_ */
