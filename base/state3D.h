#ifndef STATE3D_H
#define STATE3D_H

#include "state.h"


class State3D : virtual public State
{

public:
    double mX;
    double mY;
    double mZ;
    double mVx;
    double mVy;
    double mVz;
public:
    State3D();
    void set(const vector<double> & data);
    void get(vector <double> & data) const;
    void add(const shared_ptr<State> i);
    void sub(const shared_ptr<State> i);
    void divBy(double d);
    shared_ptr<State> copy();

    void setDynamics( const shared_ptr<State> begin,  const shared_ptr<State> end);
    void predict ();
    double getGaussian ( const shared_ptr<State> i,const vector<double>& sigmas) const;
    void applyRandomFluctuation(const vector<double> & sigmas, default_random_engine& generator);

    ostream& print(ostream& out);
    void draw(QPainter& painter,double& dim);
    void drawConnection(QPainter& painter,const shared_ptr<State> i);
    void drawPoint(QPainter& painter);
    void drawText(QPainter& painter,QString text);
};


#endif // STATE3D_H
